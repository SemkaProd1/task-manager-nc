package com.tmnc.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.users.CreateNewUserRequestPayload;
import com.tmnc.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.math.BigInteger;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
public class RegistrationControllerTest extends GeneralMvcControllerTest {

    public static final String USERNAME = "UserName";
    public static final String PASSWORD = "12345678";
    public static final String EMAIL = "some.email@gmail.com";

    @Autowired
    private UserService userService;

    @Test
    public void register() throws Exception {
        CreateNewUserRequestPayload payload = createNewUserRequestPayload();

        UserAccount user = registerAndGet(payload);
        Assert.assertNotNull(user);
        assertUserAccountWithUserRequestPayload(user, payload);

        deleteUser(user.getObjectId());
        Assert.assertNull(userService.getUser(user.getObjectId()));
    }

    public UserAccount registerAndGet(CreateNewUserRequestPayload payload) throws Exception {
        MockHttpServletResponse response = register(payload);

        String objectId = new ObjectMapper()
                .readTree(response.getContentAsString())
                .findPath("objectId")
                .asText();

        return userService.getUser(new BigInteger(objectId));
    }

    /**
     * The method registers a new user account with default parameters.
     *
     * @return - the new user account
     */
    public UserAccount registerAndGet() throws Exception {
        return registerAndGet(createNewUserRequestPayload());
    }

    public boolean deleteUser(BigInteger objectId) {
        return userService.delete(objectId);
    }

    private MockHttpServletResponse register(CreateNewUserRequestPayload payload) throws Exception {
        RequestBuilder postRequest = buildPost("/register/pass", payload);
        return perform(postRequest)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
    }

    private void assertUserAccountWithUserRequestPayload(UserAccount userAccount, CreateNewUserRequestPayload payload) {
        Assert.assertEquals(userAccount.getUserName(), payload.getUsername());
        Assert.assertEquals(userAccount.getEmail(), payload.getEmail());
        Assert.assertEquals(userAccount.getFirstName(), payload.getFirstName());
        Assert.assertEquals(userAccount.getLastName(), payload.getLastName());
    }

    private CreateNewUserRequestPayload createNewUserRequestPayload() {
        CreateNewUserRequestPayload potentialUser = new CreateNewUserRequestPayload();
        potentialUser.setEmail(EMAIL);
        potentialUser.setConfirmPassword(PASSWORD);
        potentialUser.setPassword(PASSWORD);
        potentialUser.setFirstName(USERNAME);
        potentialUser.setLastName(USERNAME);
        potentialUser.setUsername(USERNAME);
        return potentialUser;
    }
}