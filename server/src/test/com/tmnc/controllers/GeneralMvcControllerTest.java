package com.tmnc.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmnc.configs.WebMvcConfig;
import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.function.BiFunction;

@WebAppConfiguration
@ContextConfiguration(classes = {WebMvcConfig.class})
public class GeneralMvcControllerTest {

    @Autowired
    private WebApplicationContext context;

    private static MockMvc mvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    MockHttpServletRequestBuilder buildPost(String url, Object var) {
        return buildPost(url)
                .content(parseJson(var))
                .contentType(MediaType.APPLICATION_JSON);
    }

    MockHttpServletRequestBuilder buildPost(String url) {
        return MockMvcRequestBuilders.post(url);
    }

    MockHttpServletRequestBuilder buildGet(String url) {
        return MockMvcRequestBuilders.get(url);
    }

    MockHttpServletRequestBuilder buildPut(String url) {
        return MockMvcRequestBuilders.put(url);
    }

    MockHttpServletRequestBuilder buildPut(String url, Object var) {
        return buildPut(url)
                .content(parseJson(var))
                .contentType(MediaType.APPLICATION_JSON);
    }

    ResultActions perform(RequestBuilder requestBuilder) throws Exception {
        return mvc.perform(requestBuilder);
    }

    ResultActions perform(String url, Object var, BiFunction<String, Object, MockHttpServletRequestBuilder> function) throws Exception {
        RequestBuilder request = function.apply(url, var);
        return perform(request);
    }

    private String parseJson(Object var) {
        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(var);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }
}