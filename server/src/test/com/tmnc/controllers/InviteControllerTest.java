package com.tmnc.controllers;

import com.tmnc.data.entities.Invite;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.invite.CreateNewInviteRequestPayload;
import com.tmnc.data.utils.ParticipantRole;
import com.tmnc.services.InviteService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.Cookie;
import java.math.BigInteger;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
public class InviteControllerTest extends GeneralMvcControllerTest {

    @Autowired
    private InviteService inviteService;

    @Autowired
    private RegistrationControllerTest registrationControllerTest;

    private UserAccount user;

    @Before
    public void createUser() throws Exception {
        user = registrationControllerTest.registerAndGet();
    }

    @After
    public void deleteUser() {
        registrationControllerTest.deleteUser(user.getObjectId());
    }

    @Test
    public void sendAndAccept_nonRegistered() throws Exception {
        CreateNewInviteRequestPayload potentialInvite = createInviteNonRegisteredUser();

        String inviteUrl = send(potentialInvite);

        Invite invite = inviteService.get(inviteUrl);
        Assert.assertNotNull(invite);
        assertInviteEqualsInviteRequestPayload(invite, potentialInvite);

        MockHttpServletResponse response = accept(inviteUrl);
        Assert.assertEquals("/api/register/pass", response.getForwardedUrl());

        Cookie cookie = response.getCookie("Invite");
        Assert.assertNotNull(cookie);
        Assert.assertEquals(cookie.getValue(), inviteUrl);

        inviteService.delete(invite.getObjectId());
        Assert.assertNull(inviteService.get(inviteUrl));
    }

    @Test
    public void sendAndAccept_nonAuthorized() throws Exception {
        CreateNewInviteRequestPayload potentialInvite = createInviteNonAuthorizedUser();

        String inviteUrl = send(potentialInvite);

        Invite invite = inviteService.get(inviteUrl);
        assertInviteEqualsInviteRequestPayload(invite, potentialInvite);
        Assert.assertEquals(user.getEmail(), invite.getEmail());

        MockHttpServletResponse response = accept(inviteUrl);
        Assert.assertEquals("/login", response.getForwardedUrl());

        Cookie cookie = response.getCookie("Invite");
        Assert.assertNotNull(cookie);
        Assert.assertEquals(cookie.getValue(), inviteUrl);

        inviteService.delete(invite.getObjectId());
        Assert.assertNull(inviteService.get(inviteUrl));
    }

    public String send(CreateNewInviteRequestPayload potentialInvite) throws Exception {
        RequestBuilder postRequest = buildPost("/api/invite/send", potentialInvite);
        String url = perform(postRequest)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString()
                .replace("\"", "");

        Assert.assertNotNull(url);
        return url;
    }

    public MockHttpServletResponse accept(String inviteUrl) throws Exception {
        return perform(buildGet(inviteUrl))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
    }

    private void assertInviteEqualsInviteRequestPayload(Invite target, CreateNewInviteRequestPayload request) {
        Assert.assertEquals(target.getProjectId(), request.getProjectId());
        Assert.assertEquals(target.getEmail(), request.getEmail());
        Assert.assertEquals(target.getRole(), request.getRole());
    }

    private CreateNewInviteRequestPayload createInviteNonRegisteredUser() {
        CreateNewInviteRequestPayload potentialInvite = new CreateNewInviteRequestPayload();
        potentialInvite.setEmail("non.registered@gmail.com");
        potentialInvite.setProjectId(BigInteger.valueOf(1));
        potentialInvite.setRole(ParticipantRole.USER);
        return potentialInvite;
    }

    private CreateNewInviteRequestPayload createInviteNonAuthorizedUser() {
        CreateNewInviteRequestPayload potentialInvite = new CreateNewInviteRequestPayload();
        potentialInvite.setEmail(RegistrationControllerTest.EMAIL);
        potentialInvite.setProjectId(BigInteger.valueOf(1));
        potentialInvite.setRole(ParticipantRole.USER);
        return potentialInvite;
    }
}