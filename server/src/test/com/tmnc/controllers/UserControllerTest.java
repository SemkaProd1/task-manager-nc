package com.tmnc.controllers;

import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.users.UpdateUserEmailRequestPayload;
import com.tmnc.data.exchange.users.UpdateUserPasswordRequestPayload;
import com.tmnc.services.UserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigInteger;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest extends GeneralMvcControllerTest {

    @Autowired
    private UserService userService;

    @Autowired
    private RegistrationControllerTest registrationControllerTest;

    private UserAccount user;

    @Before
    public void createUser() throws Exception {
        user = registrationControllerTest.registerAndGet();
    }

    @After
    public void deleteUser() {
        registrationControllerTest.deleteUser(user.getObjectId());
    }

    @Test
    public void updateEmail_incorrectEmail() throws Exception {
        UpdateUserEmailRequestPayload incorrectEmail = createIncorrectUpdateEmailUserRequestPayload(user.getObjectId());

        perform("/api/usr/update/email", incorrectEmail, super::buildPost);
        String actualEmail = userService.getUser(user.getObjectId()).getEmail();
        String unexpectedEmail = incorrectEmail.getEmail();
        Assert.assertNotEquals(unexpectedEmail, actualEmail);
    }

    @Test
    public void updateEmail_emptyEmail() throws Exception {
        UpdateUserEmailRequestPayload emptyEmail = createEmptyUpdateEmailUserRequestPayload(user.getObjectId());

        perform("/api/usr/update/email", emptyEmail, super::buildPost);
        String actualEmail = userService.getUser(user.getObjectId()).getEmail();
        String unexpectedEmail = emptyEmail.getEmail();
        Assert.assertNotEquals(unexpectedEmail, actualEmail);
    }

    @Test
    public void updateEmail_correctEmail() throws Exception {
        UpdateUserEmailRequestPayload correctEmail = createCorrectUpdateEmailUserRequestPayload(user.getObjectId());

        perform("/api/usr/update/email", correctEmail, super::buildPost);
        String expectedEmail = correctEmail.getEmail();
        String actualEmail = userService.getUser(user.getObjectId()).getEmail();
        Assert.assertEquals(expectedEmail, actualEmail);
    }

    @Test
    public void updateEmail_emptyPassword() throws Exception {
        UpdateUserPasswordRequestPayload emptyPassword = createEmptyPasswords(user.getObjectId());

        perform("/api/usr/update/password", emptyPassword, super::buildPost);
        String actualPassword = userService.getUser(user.getObjectId()).getPassword();
        Assert.assertTrue(userService.passwordNotMatch(emptyPassword.getPassword(), actualPassword));
    }

    @Test
    public void updatePassword_notMatchPasswords() throws Exception {
        UpdateUserPasswordRequestPayload notMatches = createNotMatchPasswords(user.getObjectId());

        perform("/api/usr/update/password", notMatches, super::buildPost);
        String actualPassword = userService.getUser(user.getObjectId()).getPassword();
        Assert.assertTrue(userService.passwordNotMatch(notMatches.getPassword(), actualPassword));
    }

    @Test
    public void updatePassword_incorrectOldPassword() throws Exception {
        UpdateUserPasswordRequestPayload incorrectOldPassword = createIncorrectOldPassword(user.getObjectId());

        perform("/api/usr/update/password", incorrectOldPassword, super::buildPost);
        String actualPassword = userService.getUser(user.getObjectId()).getPassword();
        Assert.assertTrue(userService.passwordNotMatch(incorrectOldPassword.getPassword(), actualPassword));
    }

    @Test
    public void updatePassword_nullablePassword() throws Exception {
        UpdateUserPasswordRequestPayload nullablePassword = createNullablePassword(user.getObjectId());

        perform("/api/usr/update/password", nullablePassword, super::buildPost);
        String actualPassword = userService.getUser(user.getObjectId()).getPassword();
        Assert.assertTrue(userService.passwordNotMatch(nullablePassword.getPassword(), actualPassword));
    }

    @Test
    public void updatePassword_correctPassword() throws Exception {
        UpdateUserPasswordRequestPayload correctPassword = createCorrectPassword(user.getObjectId());

        perform("/api/usr/update/password", correctPassword, super::buildPost);
        UserAccount actualUser = userService.getUser(user.getObjectId());
        String expectedPassword = correctPassword.getPassword();
        String actualEncodedPassword = actualUser.getPassword();
        Assert.assertTrue(userService.isPasswordMatch(expectedPassword, actualEncodedPassword));
    }

    @Test
    public void isEmailExists_correctEmail() throws Exception {
        Integer response = new Integer(
                perform(
                        buildGet("/api/usr/isMailExists")
                                .param("email", RegistrationControllerTest.EMAIL))
                        .andReturn()
                        .getResponse()
                        .getContentAsString()
        );
        Assert.assertEquals(Integer.valueOf(1), response);
    }

    @Test
    public void isEmailExists_incorrectEmail() throws Exception {
        String incorrectEmail = "Incorrect.Email.test@email.easy";
        Integer response = new Integer(
                perform(
                        buildGet("/api/usr/isMailExists")
                                .param("email",incorrectEmail))
                        .andReturn()
                        .getResponse()
                        .getContentAsString()
        );
        Assert.assertEquals(Integer.valueOf(0), response);
    }

    private UpdateUserEmailRequestPayload createIncorrectUpdateEmailUserRequestPayload(BigInteger objectId) {
        UpdateUserEmailRequestPayload incorrectEmail = new UpdateUserEmailRequestPayload();
        incorrectEmail.setObjectId(objectId);
        incorrectEmail.setEmail("some.incorrect.email.ua");
        return incorrectEmail;
    }

    private UpdateUserEmailRequestPayload createEmptyUpdateEmailUserRequestPayload(BigInteger objectId) {
        UpdateUserEmailRequestPayload incorrectEmail = new UpdateUserEmailRequestPayload();
        incorrectEmail.setObjectId(objectId);
        incorrectEmail.setEmail("");
        return incorrectEmail;
    }

    private UpdateUserEmailRequestPayload createCorrectUpdateEmailUserRequestPayload(BigInteger objectId) {
        UpdateUserEmailRequestPayload correctEmail = new UpdateUserEmailRequestPayload();
        correctEmail.setObjectId(objectId);
        correctEmail.setEmail("some.correct@email.ua");
        return correctEmail;
    }

    private UpdateUserPasswordRequestPayload createIncorrectOldPassword(BigInteger objectId) {
        UpdateUserPasswordRequestPayload incorrectOldPassword = new UpdateUserPasswordRequestPayload();
        incorrectOldPassword.setObjectId(objectId);
        incorrectOldPassword.setOldPassword("incorrect");
        incorrectOldPassword.setPassword("11111111");
        incorrectOldPassword.setConfirmPassword("11111111");
        return incorrectOldPassword;
    }

    private UpdateUserPasswordRequestPayload createNotMatchPasswords(BigInteger objectId) {
        UpdateUserPasswordRequestPayload notMatches = new UpdateUserPasswordRequestPayload();
        notMatches.setObjectId(objectId);
        notMatches.setOldPassword(RegistrationControllerTest.PASSWORD);
        notMatches.setPassword("11111111");
        notMatches.setConfirmPassword("notMatches");
        return notMatches;
    }

    private UpdateUserPasswordRequestPayload createEmptyPasswords(BigInteger objectId) {
        UpdateUserPasswordRequestPayload notMatches = new UpdateUserPasswordRequestPayload();
        notMatches.setObjectId(objectId);
        notMatches.setOldPassword(RegistrationControllerTest.PASSWORD);
        notMatches.setPassword("");
        notMatches.setConfirmPassword("");
        return notMatches;
    }

    private UpdateUserPasswordRequestPayload createNullablePassword(BigInteger objectId) {
        UpdateUserPasswordRequestPayload nullablePassword = new UpdateUserPasswordRequestPayload();
        nullablePassword.setObjectId(objectId);
        nullablePassword.setOldPassword("incorrect");
        nullablePassword.setPassword("5235235235");
        nullablePassword.setConfirmPassword(null);
        return nullablePassword;
    }

    private UpdateUserPasswordRequestPayload createCorrectPassword(BigInteger objectId) {
        UpdateUserPasswordRequestPayload nullablePassword = new UpdateUserPasswordRequestPayload();
        nullablePassword.setObjectId(objectId);
        nullablePassword.setOldPassword(RegistrationControllerTest.PASSWORD);
        nullablePassword.setPassword("11111111");
        nullablePassword.setConfirmPassword("11111111");
        return nullablePassword;
    }
}