package com.tmnc.advices;

import com.tmnc.core.exceptions.NotValidInviteException;
import com.tmnc.core.exceptions.TMNCApplicationException;
import com.tmnc.core.exceptions.file.FileDownloadException;
import com.tmnc.core.exceptions.file.FileStorageDeleteException;
import com.tmnc.core.exceptions.file.FileStorageInitException;
import com.tmnc.core.exceptions.file.FileStorageStoreException;
import com.tmnc.utils.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestControllerAdvice(basePackages = {"com.tmnc.controllers"})
public class ExceptionHandlingControllerAdvice {

	@ExceptionHandler({
			FileStorageDeleteException.class,
			FileStorageInitException.class,
			FileStorageStoreException.class
	})
	public final ResponseEntity<ApiError> fileExceptions(TMNCApplicationException e) {
		ApiError apiError = new ApiError();
		apiError.setMessage(e.getMessage());

		return ResponseEntity.badRequest()
				.body(apiError);
	}

	@ExceptionHandler({
			NotValidInviteException.class
	})
	public final ResponseEntity<ApiError> notValidInvite(NotValidInviteException e) {
		ApiError apiError = new ApiError();
		apiError.setMessage("Invalid invite");

		return ResponseEntity.badRequest()
				.body(apiError);
	}

	@ExceptionHandler({
			InsufficientAuthenticationException.class
	})
	public final ResponseEntity<ApiError> accessDenied(InsufficientAuthenticationException e) {
		ApiError apiError = new ApiError();
		apiError.setMessage("Please login");

		return ResponseEntity.status(HttpStatus.FORBIDDEN)
				.body(apiError);
	}

	@ExceptionHandler({
			FileDownloadException.class
	})
	public final ResponseEntity<ApiError> fileDownloadError(FileDownloadException e) {
		ApiError apiError = new ApiError();
		apiError.setMessage("Some problems occur while preparing for download");

		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(apiError);
	}
}
