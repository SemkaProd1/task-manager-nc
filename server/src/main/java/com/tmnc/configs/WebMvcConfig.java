package com.tmnc.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.tmnc.core.hash.PBKDF2PasswordEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.ServletContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@EnableWebMvc
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "com.tmnc")
public class WebMvcConfig implements WebMvcConfigurer {

    private final SqlDateConfig config;
    private final ServletContext context;
    private final Logger logger = LogManager.getRootLogger();

    @Value("${user.dir}")
    private Path userDir;

    @Autowired
    public WebMvcConfig(SqlDateConfig config, ServletContext context) {
        this.config = config;

        this.context = context;
    }

    @Bean
    public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();

        builder.indentOutput(true)
                .modules(
                        new JavaTimeModule(),
                        new Jdk8Module(),
                        new ParameterNamesModule())
                .deserializers(config.sqlDateDeserializer())
                .serializers(config.sqlDateSerializer());

        return builder;
    }

    @Bean
    public MappingJackson2HttpMessageConverter jackson2HttpMessageConverter() {
        return new MappingJackson2HttpMessageConverter(jackson2ObjectMapperBuilder().build());
    }

    @Bean
    public ObjectMapper mapper() {
        return jackson2ObjectMapperBuilder().build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PBKDF2PasswordEncoder(
                PBKDF2PasswordEncoder.Algorithm.HMAC_SHA1, "justsalt", 4, 64);
    }

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(this.jackson2HttpMessageConverter());
	}

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        try {
            String s = Paths.get(context.getRealPath("/WEB-INF/client")).toUri().toURL().toExternalForm() + "/";
            registry.addResourceHandler("/**")
                    .addResourceLocations(s);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
        registry.addViewController("/**/{[path:[^\\.]*}").setViewName("forward:/index.html");
    }

    @Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(config.sqlDateSpringFormatter());
	}
}
