package com.tmnc.configs;


import oracle.jdbc.driver.OracleDriver;
import org.apache.commons.dbcp2.*;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages="com.tmnc")
@PropertySource("classpath:application.properties")
public class JdbcConfig {

	@Value("${tmnc.database.url}")
	private String url;

	@Value("${tmnc.database.login}")
	private String login;

	@Value("${tmnc.database.password}")
	private String password;

	private final Logger logger = LogManager.getLogger(JdbcConfig.class);


	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(OracleDriver.class.getCanonicalName());
		dataSource.setUrl(this.url);
		dataSource.setUsername(this.login);
		dataSource.setPassword(this.password);

		ConnectionFactory connectionFactory = new DataSourceConnectionFactory(dataSource);

		PoolableConnectionFactory poolableConnectionFactory =
				new PoolableConnectionFactory(connectionFactory, null);

		ObjectPool<PoolableConnection> connectionPool =
				new GenericObjectPool<>(poolableConnectionFactory);
		poolableConnectionFactory.setPool(connectionPool);

		return new PoolingDataSource<>(connectionPool);
	}

	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(this.dataSource());
	}

	@Bean
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
		return new NamedParameterJdbcTemplate(this.dataSource());
	}
}
