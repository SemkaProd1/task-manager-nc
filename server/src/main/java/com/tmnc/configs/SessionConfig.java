package com.tmnc.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages="com.tmnc")
@PropertySource("classpath:application.properties")
public class SessionConfig {
	@Value("${tmnc.session.maxInactiveInterval}")
	private int maxInactiveInterval;

	public int getMaxInactiveInterval() {
		return maxInactiveInterval;
	}
}
