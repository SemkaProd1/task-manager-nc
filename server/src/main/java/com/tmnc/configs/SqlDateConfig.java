package com.tmnc.configs;

import com.tmnc.utils.date.SqlDateDeserializer;
import com.tmnc.utils.date.SqlDateSerializer;
import com.tmnc.utils.date.SqlDateSpringFormatter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Configuration
@ComponentScan(basePackages="com.tmnc")
@PropertySource("classpath:application.properties")
public class SqlDateConfig {

	@Bean
	public DateTimeFormatter dateTimeFormatter() {
		return DateTimeFormatter.ISO_DATE_TIME
				.withZone(ZoneId.of("UTC"))
				.withLocale(
						defaultLocale()
				);
	}

	@Bean
	public Locale defaultLocale() {
		Locale.setDefault(Locale.ENGLISH);
		return Locale.ENGLISH;
	}

	@Bean
	public SqlDateSpringFormatter sqlDateSpringFormatter() {
		return new SqlDateSpringFormatter(dateTimeFormatter());
	}

	@Bean
	public SqlDateDeserializer sqlDateDeserializer() {
		return new SqlDateDeserializer(dateTimeFormatter());
	}

	@Bean
	public SqlDateSerializer sqlDateSerializer() {
		return new SqlDateSerializer(dateTimeFormatter());
	}

}
