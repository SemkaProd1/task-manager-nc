package com.tmnc.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@ComponentScan(basePackages="com.tmnc")
@PropertySource("classpath:application.properties")
public class FileUploadConfig {

    @Value("${tmnc.fileupload.storage.maxFileSize}")
    private int maxFileSize;

    private Path storageAbsolutePath;
    private Path projectsPath;
    private Path usersPath;

    public FileUploadConfig(@Value("${tmnc.fileupload.storage.location}") String storageRelativePath,
                            @Value("${user.home}") String userHome) {
        this.storageAbsolutePath = Paths.get(userHome, storageRelativePath);
        this.projectsPath = this.storageAbsolutePath.resolve("projects");
        this.usersPath = this.storageAbsolutePath.resolve("users");
    }

    public Path getStorageAbsolutePath() {
        return storageAbsolutePath;
    }

    public Path getProjectsPath() {
        return projectsPath;
    }

    public Path getUsersPath() {
        return usersPath;
    }

    public int getMaxFileSize() {
        return maxFileSize;
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(this.maxFileSize);

        return multipartResolver;
    }
}
