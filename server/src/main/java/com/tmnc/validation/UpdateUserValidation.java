package com.tmnc.validation;

import com.tmnc.configs.FileUploadConfig;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.users.UpdateUserRequestPayload;
import com.tmnc.data.repositories.UserRepository;
import com.tmnc.services.UserService;
import com.tmnc.utils.PathSanitizeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

import static com.tmnc.validation.utils.UtilsValidation.emailValidation;
import static com.tmnc.validation.utils.UtilsValidation.passwordValidation;

@Component
public class UpdateUserValidation implements Validator {

    private final UserService service;
    private final UserRepository repository;
    private final FileUploadConfig config;

    @Autowired
    public UpdateUserValidation(UserService service, UserRepository repository, FileUploadConfig config) {
        this.service = service;
        this.repository = repository;
        this.config = config;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UpdateUserRequestPayload.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object user, Errors errors) {

        UpdateUserRequestPayload payload = (UpdateUserRequestPayload) user;
        UserAccount currentUserAccount = repository.getUserAccountById(payload.getObjectId());

        if (payload.getUsername() != null) {
            if ((payload.getUsername().trim().length() > 0 && (payload.getUsername().length() < 6
                    || payload.getUsername().length() > 32))) {
                errors.rejectValue("username", "InvalidValue",
                        "Please use between 6 and 32 characters.");
                if (service.isUserExist(payload.getUsername())) {
                    errors.rejectValue("username", "Duplicate.userForm.username",
                            "Username already exists.");
                }
            }
        }

        if (payload.getEmail() != null) {
            emailValidation(errors, payload.getEmail(), currentUserAccount.getEmail(), service);
        }

        if (payload.getOldPassword() != null && payload.getPassword() != null
                && payload.getConfirmPassword() != null) {
            if (payload.getOldPassword().trim().length() > 0) {
                if (!currentUserAccount.getPassword().equals(payload.getOldPassword())) {
                    errors.rejectValue("oldPassword", "Diff.userForm.passwordConfirm", "Password mismatch");
                }
                passwordValidation(errors, payload.getPassword(), payload.getConfirmPassword());
            }
        }


        MultipartFile avatar = payload.getAvatar();

        if (avatar == null || avatar.isEmpty()) {
            return;
        }

        Path projectPath = config.getProjectsPath()
                .resolve(payload.getObjectId().toString())
                .resolve("avatar");

        Path actualLogoPath = projectPath.resolve(avatar.getOriginalFilename());
        if (PathSanitizeUtils.isSanitizedPath(projectPath, actualLogoPath)) {
            errors.rejectValue("avatar", "avatar.name.not.resolved",
                    "Could not resolve avatar name");
        }
    }
}
