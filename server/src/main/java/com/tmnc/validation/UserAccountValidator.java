package com.tmnc.validation;

import com.tmnc.data.exchange.users.CreateNewUserRequestPayload;
import com.tmnc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.tmnc.validation.utils.UtilsValidation.emailExistsValidation;
import static com.tmnc.validation.utils.UtilsValidation.passwordValidation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UserAccountValidator implements Validator {
    private final UserService service;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String REQUIRED_MESSAGE = "This field is required.";
    private static final String NOT_EMPTY = "NotEmpty";
    private static final String USERNAME_PATTERN = "^(?=.{6,32}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$";

    @Autowired
    public UserAccountValidator(UserService service) {
        this.service = service;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return (CreateNewUserRequestPayload.class.isAssignableFrom(aClass));
    }

    @Override
    public void validate(Object user, Errors errors) {
        CreateNewUserRequestPayload userAccount = (CreateNewUserRequestPayload) user;


        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(userAccount.getEmail());

        Pattern usernamePattern = Pattern.compile(USERNAME_PATTERN);
        Matcher usrnameMatcher = usernamePattern.matcher(userAccount.getUsername());

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", NOT_EMPTY,
                REQUIRED_MESSAGE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName",
                "NotEmpty", REQUIRED_MESSAGE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", NOT_EMPTY,
                REQUIRED_MESSAGE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", NOT_EMPTY,
                REQUIRED_MESSAGE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", NOT_EMPTY,
                REQUIRED_MESSAGE);

        if ((userAccount.getUsername().length() < 6 || userAccount.getUsername().length() > 32)) {
            errors.rejectValue("username", "InvalidValue",
                    "Username must be between 6 and 32 characters.");
        }
        if (service.isUserExist(userAccount.getUsername())) {
            errors.rejectValue("username", "Duplicate.userForm.username",
                    "Username already exists.");
        }

        emailExistsValidation(errors, service, userAccount.getEmail());

        passwordValidation(errors, userAccount.getPassword(), userAccount.getConfirmPassword());

        if (!matcher.matches()) {
            errors.rejectValue("email", "Invalid.userForm.email",
                    "Invalid email.");
        }

        if (!usrnameMatcher.matches()) {
            errors.rejectValue("username", "Invalid.userForm.username", "Invalid username format");
        }
    }
}