package com.tmnc.validation;

import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.users.UpdateUserEmailRequestPayload;
import com.tmnc.data.repositories.UserRepository;
import com.tmnc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Objects;

import static com.tmnc.validation.utils.UtilsValidation.emailValidation;

@Component
public class UpdateUserEmailValidation implements Validator {

    private final UserService service;
    private final UserRepository repository;

    @Autowired
    public UpdateUserEmailValidation(UserService service, UserRepository repository) {
        this.service = service;
        this.repository = repository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UpdateUserEmailRequestPayload.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object user, Errors errors) {

        UpdateUserEmailRequestPayload payload = (UpdateUserEmailRequestPayload) user;
        UserAccount currentUserAccount = repository.getUserAccountById(payload.getObjectId());
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email",
                "NotEmpty", "Please, specify email field");


        if (Objects.isNull(payload.getEmail())) {
            errors.rejectValue("email", "Nullable.userForm.email", "Email is empty");
            return;
        }

        emailValidation(errors, payload.getEmail(), currentUserAccount.getEmail(), service);
    }
}