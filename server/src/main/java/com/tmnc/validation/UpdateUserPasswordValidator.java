package com.tmnc.validation;

import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.users.UpdateUserPasswordRequestPayload;
import com.tmnc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.tmnc.validation.utils.UtilsValidation.passwordValidation;

@Component
public class UpdateUserPasswordValidator implements Validator {

    private final UserService service;

    @Autowired
    public UpdateUserPasswordValidator(UserService service) {
        this.service = service;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return UpdateUserPasswordRequestPayload.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        UpdateUserPasswordRequestPayload payload = (UpdateUserPasswordRequestPayload) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPassword", "NotEmpty",
                "Password field is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty",
                "New Password field is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty",
                "confirm password field is required");

        UserAccount userAccount = service.getUser(payload.getObjectId());
        if (service.passwordNotMatch(payload.getOldPassword(), userAccount.getPassword())) {
            errors.rejectValue("password", "Incorrect.userForm.password",
                    "This password don't match with the old one");
        }
        passwordValidation(errors, payload.getPassword(), payload.getConfirmPassword());
    }
}