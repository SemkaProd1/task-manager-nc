package com.tmnc.validation;

import com.tmnc.data.exchange.project.CreateNewProjectRequestPayload;
import com.tmnc.validation.utils.UtilsValidation;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.math.BigInteger;

@Component
public class CreateNewProjectInputValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return CreateNewProjectRequestPayload.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CreateNewProjectRequestPayload payload = (CreateNewProjectRequestPayload) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"projectName",
				"NotEmpty",
				"Project name must be specified");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"projectDescription",
				"NotEmpty",
				"Project description must be specified");

		if (payload.getCreatorId() == null || payload.getCreatorId().equals(BigInteger.ZERO)) {
			errors.rejectValue("creatorId", "InvalidValue", "Creator must be specified");
		}

		UtilsValidation.rejectOnInvalidDate("startDate", payload.getStartDate(),
				"Start date must be specified", errors);
		UtilsValidation.rejectOnInvalidDate("endDate", payload.getEndDate(),
				"End date must be specified", errors);
	}
}
