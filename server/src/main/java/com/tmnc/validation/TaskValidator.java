package com.tmnc.validation;

import com.tmnc.data.exchange.task.CreateNewTaskRequestPayload;
import com.tmnc.data.exchange.task.UpdateTaskRequestPayload;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TaskValidator implements Validator {
	@Override
	public boolean supports(Class<?> aClass) {
		return CreateNewTaskRequestPayload.class.isAssignableFrom(aClass)
				|| UpdateTaskRequestPayload.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(Object task, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"name",
				"NotEmpty",
				"Task name must be specified");

	}
}
