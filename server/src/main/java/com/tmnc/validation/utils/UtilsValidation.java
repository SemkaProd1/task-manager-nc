package com.tmnc.validation.utils;

import com.tmnc.services.UserService;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import java.sql.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilsValidation {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    public static void rejectOnInvalidDate(String field, Date value, String defaultMessage, Errors errors) {

        if (value == null) {
            errors.rejectValue(field, "NonNull", defaultMessage);
        }
    }

    public static void passwordValidation(Errors errors, String password, String confirmPassword) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty",
                "This field is required.");
        if (password.length() < 8 || password.length() > 32 ) {
            errors.rejectValue("password", "Size.userForm.password",
                    "Password must be at least 8 characters.");
        }
        if (!Objects.equals(password, confirmPassword)) {
            errors.rejectValue("confirmPassword", "Diff.userForm.passwordConfirm",
                    "These passwords don't match");
        }
    }

    public static void emailExistsValidation(Errors errors, UserService service, String email) {
        if(service.isEmailExists(email)) {
            errors.rejectValue("email", "Duplicate.userForm.email",
                    "Email already exists");
        }
    }

    public static void emailValidation(Errors errors, String email, String currentUserEmail, UserService service){
        if (email.trim().length() > 0) {
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(email);
            if (!currentUserEmail.equals(email)) {
                emailExistsValidation(errors, service, email);
            }
            if (!matcher.matches()) {
                errors.rejectValue("email", "Invalid.userForm.email",
                        "Invalid email.");
            }
        }
    }
}