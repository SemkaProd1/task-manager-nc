package com.tmnc.validation;

import com.tmnc.configs.FileUploadConfig;
import com.tmnc.data.exchange.project.UpdateProjectRequestPayload;
import com.tmnc.utils.PathSanitizeUtils;
import com.tmnc.validation.utils.UtilsValidation;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

@Component
public class UpdateProjectValidator implements Validator {

    private final FileUploadConfig config;

    public UpdateProjectValidator(FileUploadConfig config) {
        this.config = config;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UpdateProjectRequestPayload.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UpdateProjectRequestPayload payload = (UpdateProjectRequestPayload) o;

        if (payload.getName() == null || payload.getName().isEmpty()) {
            errors.rejectValue("name", "name.isEmpty", "Project name must not be empty");
        }

        if (payload.getPhase() == null) {
            errors.rejectValue("phase", "phase.notSpecified", "Phase is invalid");
        }

        UtilsValidation.rejectOnInvalidDate("startDate", payload.getStartDate(),
                "Start date must be specified", errors);
        UtilsValidation.rejectOnInvalidDate("endDate", payload.getEndDate(),
                "End date must be specified", errors);

        MultipartFile logo = payload.getLogo();

        if (logo == null || logo.isEmpty()) {
            return;
        }

        Path projectPath = config.getProjectsPath()
                .resolve(payload.getObjectId().toString())
                .resolve("logo");

        Path actualLogoPath = projectPath.resolve(logo.getOriginalFilename());

        if (PathSanitizeUtils.isSanitizedPath(projectPath, actualLogoPath)) {
            errors.rejectValue("logo", "logo.name.not.resolved", "Could not resolve logo name");
        }
    }
}
