package com.tmnc.validation;

import com.tmnc.data.exchange.sprint.CreateNewSprintRequestPayload;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.tmnc.validation.utils.UtilsValidation.rejectOnInvalidDate;

@Component
public class SprintValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return CreateNewSprintRequestPayload.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object sprint, Errors errors) {
        CreateNewSprintRequestPayload payload = (CreateNewSprintRequestPayload) sprint;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty",
                "Sprint name mustn't be null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "NotEmpty",
                "Sprint description mustn't be null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate", "NotEmpty",
                "Sprint start date mustn't be null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endDate", "NotEmpty",
                "Sprint end date mustn't be null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "status", "NotEmpty",
                "Sprint status mustn't be null");

        rejectOnInvalidDate("startDate", payload.getStartDate(),
                "Start date must be specified", errors);
        rejectOnInvalidDate("endDate", payload.getEndDate(),
                "End date must be specified", errors);
    }
}
