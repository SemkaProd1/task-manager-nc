package com.tmnc.core.hash;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class PBKDF2InvalidKeyException extends TMNCApplicationException {
    public PBKDF2InvalidKeyException() {
    }

    public PBKDF2InvalidKeyException(String message) {
        super(message);
    }

    public PBKDF2InvalidKeyException(String message, Throwable cause) {
        super(message, cause);
    }

    public PBKDF2InvalidKeyException(Throwable cause) {
        super(cause);
    }
}
