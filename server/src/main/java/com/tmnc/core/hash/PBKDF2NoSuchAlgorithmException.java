package com.tmnc.core.hash;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class PBKDF2NoSuchAlgorithmException extends TMNCApplicationException {
    public PBKDF2NoSuchAlgorithmException() {
    }

    public PBKDF2NoSuchAlgorithmException(String message) {
        super(message);
    }

    public PBKDF2NoSuchAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }

    public PBKDF2NoSuchAlgorithmException(Throwable cause) {
        super(cause);
    }
}
