package com.tmnc.core.hash;

import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;


public class PBKDF2PasswordEncoder implements PasswordEncoder {
    private final String salt;
    private final int iterations;
    private final int keyLength;
    private final Mac mac;

    public PBKDF2PasswordEncoder(Algorithm algorithm,
                                 String salt,
                                 int iterations,
                                 int keyLength) {
        this.salt = salt;
        this.iterations = iterations;
        this.keyLength = keyLength;
        this.mac = this.initAlgorithm(algorithm);
    }

    @Override
    public String encode(CharSequence rawPassword) {
        try {
            int blockCount = (int) Math.ceil(keyLength / (20 * 1.0));
            byte[] last;
            byte[] result = new byte[this.keyLength];

            for (int blockIndex = 1; blockIndex <= blockCount; blockIndex++) {
                byte[] saltByteArray = salt.getBytes();
                byte[] blockIndexBigEndianByteArray = ByteBuffer.allocate(4).putInt(blockIndex).array();

                last = this.concat(saltByteArray, blockIndexBigEndianByteArray);

                byte[] xorSum = null;

                for (int iterationIndex = 1; iterationIndex <= iterations; iterationIndex++) {
                    last = this.encodeToHmac(last, rawPassword.toString().getBytes());
                    xorSum = xorSum == null ? last : bitwiseXor(xorSum, last);
                }

                result = this.concat(result, xorSum);
            }

            return this.encodeToHex(result).substring(keyLength * 2, keyLength * 4);
        } catch (InvalidKeyException e) {
            throw new PBKDF2InvalidKeyException(e);
        }
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        String encode = encode(rawPassword);
        return encodedPassword.equals(encode);
    }

    private byte[] encodeToHmac(byte[] toEncode, byte[] key) throws InvalidKeyException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, mac.getAlgorithm());
        mac.init(secretKeySpec);
        return mac.doFinal(toEncode);
    }

    private String encodeToHex(byte[] toEncode) {
        Formatter formatter = new Formatter();

        for (byte b : toEncode) {
            formatter.format("%02X", b);
        }

        return formatter.toString();
    }

    public byte[] concat(byte[]... arrays) {
        byte[] result = null;
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            for (byte[] array : arrays) {
                stream.write(array);
            }

            result = stream.toByteArray();
        } catch (IOException ignored) {
        }

        return result;
    }

    public byte[] bitwiseXor(byte[] first, byte[] second) {
        if (first.length != second.length) {
            throw new IllegalArgumentException("Arrays length are not equal!");
        }

        byte[] result = new byte[first.length];

        for (int index = 0; index < first.length; index++) {
            result[index] = (byte) (first[index] ^ second[index]);
        }

        return result;
    }

    private Mac initAlgorithm(Algorithm algorithm) {
        try {
            return Mac.getInstance(algorithm.getAlgorithm());
        } catch (NoSuchAlgorithmException e) {
            throw new PBKDF2NoSuchAlgorithmException("Could not resolve algorithm", e);
        }
    }

    public enum Algorithm {
        HMAC_SHA1("HmacSHA1"),
        HMAC_SHA256("HmacSHA256"),
        HMAC_SHA512("HmacSHA512");

        private final String algorithm;

        Algorithm(String algorithm) {
            this.algorithm = algorithm;
        }

        public String getAlgorithm() {
            return algorithm;
        }
    }

}
