package com.tmnc.core;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.NamedParameterUtils;
import org.springframework.jdbc.core.namedparam.ParsedSql;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.util.List;

public class TMNCPreparedStatementCreatorFactory {
    public static PreparedStatementCreator create(String sqlQuery, SqlParameterSource source) {

        ParsedSql parsedSql = NamedParameterUtils.parseSqlStatement(sqlQuery);
        String sqlToUse = NamedParameterUtils.substituteNamedParameters(parsedSql, source);
        List<SqlParameter> declaredParameters = NamedParameterUtils.buildSqlParameterList(parsedSql, source);

        PreparedStatementCreatorFactory factory = new PreparedStatementCreatorFactory(sqlToUse, declaredParameters);
        factory.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);

        Object[] params = NamedParameterUtils.buildValueArray(parsedSql, source, null);

        return factory.newPreparedStatementCreator(params);
    }
}
