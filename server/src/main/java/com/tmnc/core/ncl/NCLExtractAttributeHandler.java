package com.tmnc.core.ncl;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class NCLExtractAttributeHandler {

    public static final BigInteger OBJECT_TYPE = BigInteger.valueOf(-11);
    public static final BigInteger OBJECT_ID = BigInteger.valueOf(0);
    public static final BigInteger PARENT_ID = BigInteger.valueOf(-13);

    public NCLObject.Builder extractData(ResultSet resultSet, NCLObject.Builder object) throws SQLException {
        BigInteger attrId = resultSet.getBigDecimal("attribute_id").toBigInteger();
        String attrName = resultSet.getString("attribute_name");
        String attrType = resultSet.getString("attribute_type");
        String attrValue = resultSet.getString("attribute_value");
        BigDecimal id = resultSet.getBigDecimal("attribute_object_type_id");
        BigInteger attrObjectTypeId = id == null ? null : id.toBigInteger();

        if (attrId.equals(OBJECT_TYPE)) {
            object.objType(NCLType.fromId(new BigInteger(attrValue)));
            return object;
        }

        if (attrId.equals(OBJECT_ID)) {
            object.objectId(new BigInteger(attrValue));
            return object;
        }

        if (attrId.equals(PARENT_ID)) {
            object.parentId(attrValue == null ? null : new BigInteger(attrValue));
            return object;
        }

        NCLAttribute.Builder attr = NCLAttribute.builder()
                .id(attrId)
                .name(attrName)
                .type(attrType);

        if (attrObjectTypeId != null) {
            attr.referenceType(NCLType.fromId(attrObjectTypeId));
            attr.references(this.getRefsByType(resultSet, attrId));
        } else {
            attr.value(NCLAttributeValue.fromObject(attrValue, attrType));
        }

        object.attribute(attr.build());

        return object;
    }

    private List<BigInteger> getRefsByType(ResultSet resultSet,
                                           BigInteger attrId) throws SQLException {
        List<BigInteger> refs = new ArrayList<>();

        while (true) {
            if (!attrId.equals(new BigInteger(resultSet.getString("attribute_id")))) {
                resultSet.relative(-1);
                break;
            }

            refs.add(new BigInteger(resultSet.getObject("attribute_value").toString()));

            if (!resultSet.next()) {
                break;
            }
        }

        return refs;
    }
}
