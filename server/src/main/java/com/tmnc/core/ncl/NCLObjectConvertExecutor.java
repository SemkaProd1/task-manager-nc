package com.tmnc.core.ncl;


import org.springframework.stereotype.Component;

@Component
public class NCLObjectConvertExecutor {

	public <T> T executeConvert(NCLObject object, NCLToObjectConverter<T> converter) {
		if (object == null) {
			return null;
		}

		if (converter.accept() != object.getObjectType()) {
			throw new NCLObjectConvertException(
					"Object with id " + object.getObjectId() + " has type " + object.getObjectType() +
					" whereas expected type is " + converter.accept());
		}

		return converter.convert(object);
	}
}
