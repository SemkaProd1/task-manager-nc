package com.tmnc.core.ncl;

import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Component
public interface NCLToObjectConverter<T> {

    T convert(NCLObject object);

    NCLType accept();

    default Date getDate(NCLObject object, String attrName) {
        return getAttribute(object, attrName, NCLAttributeValue::getDateValue);
    }

    default NCLListType getListValue(NCLObject object, String attrName) {
        return getAttribute(object, attrName, NCLAttributeValue::getListValue);
    }

    default String getValue(NCLObject object, String attrName) {
        return getAttribute(object, attrName, NCLAttributeValue::getValue);
    }

    default BigInteger getReference(NCLObject object, String attrName) {
        NCLAttribute attribute = object.getAttributes().get(attrName);

        if (Objects.isNull(attribute)) {
            return null;
        }
        List<BigInteger> references = attribute.getReferences();
        return (Objects.isNull(references) || references.isEmpty()) ? null : references.get(0);
    }

    default List<BigInteger> getReferences(NCLObject object, String attrName) {
        NCLAttribute attribute = object.getAttributes().get(attrName);

        if (Objects.isNull(attribute)) {
            return new ArrayList<>();
        }
        List<BigInteger> references = attribute.getReferences();
        return Objects.nonNull(references) ? references : new ArrayList<>();
    }

    default <T> T getAttribute(NCLObject object, String attrName, Function<NCLAttributeValue, T> get) {
        NCLAttribute attribute = object.getAttributes().get(attrName);

        if (Objects.isNull(attribute)) {
            return null;
        }
        return get.apply(attribute.getAttributeValue());
    }
}