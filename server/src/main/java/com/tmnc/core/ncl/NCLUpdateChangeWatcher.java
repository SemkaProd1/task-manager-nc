package com.tmnc.core.ncl;

import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.core.ncl.update.NCLReferenceAction;
import com.tmnc.core.ncl.update.NCLReferenceRecord;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.sql.Date;
import java.util.*;
import java.util.stream.Stream;

@Component
public class NCLUpdateChangeWatcher {

    private final Logger logger = Logger.getLogger(NCLObjectRepository.class);

    public List<NCLAttributeRecord> compareAttributes(Object actualObject,
                                                      Object potentialObject,
                                                      Map<String, BigInteger> attributeTypes) {
        try {
            return compare(actualObject, potentialObject, attributeTypes, this::getAttributeRecord);
        } catch (IllegalAccessException e) {
            logger.trace(e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    public List<NCLReferenceRecord> compareReference(Object actualObject,
                                                     Object potentialObject,
                                                     Map<String, BigInteger> attributeTypes) {
        try {
            return compare(actualObject, potentialObject, attributeTypes, this::getReferenceRecord);
        } catch (IllegalAccessException e) {
            logger.trace(e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    private <T> List<T> compare(Object actualObject,
                                Object potentialObject,
                                Map<String, BigInteger> attributeTypes,
                                TripleFunction<T, Object, Object, BigInteger> function) throws IllegalAccessException {

        Field[] actualFields = actualObject.getClass().getDeclaredFields();
        Field[] potentialFields = potentialObject.getClass().getDeclaredFields();

        openAccessibleFields(actualFields);
        openAccessibleFields(potentialFields);

        List<T> list = new ArrayList<>();

        for (Map.Entry<String, BigInteger> attrType : attributeTypes.entrySet()) {
            String fieldName = attrType.getKey();
            BigInteger attrID = attrType.getValue();

            Field actualField = getFieldByName(fieldName, actualFields);
            Field potentialField = getFieldByName(fieldName, potentialFields);

            if (Objects.isNull(actualField) || Objects.isNull(potentialField)) {
                continue;
            }

            Object actualValue = actualField.get(actualObject);
            Object potentialValue = potentialField.get(potentialObject);

            if (!Objects.equals(actualValue, potentialValue))
                list.add(
                        function.get(actualValue, potentialValue, attrID)
                );
        }
        return list;
    }

    private NCLAttributeRecord getAttributeRecord(Object actualValue, Object potentialValue, BigInteger attrID) {
        if (actualValue instanceof Boolean)
            return new NCLAttributeRecord(attrID, convertToString((Boolean) potentialValue), null, null);

        if (actualValue instanceof String)
            return new NCLAttributeRecord(attrID, (String) potentialValue, null, null);

        if (actualValue instanceof Date)
            return new NCLAttributeRecord(attrID, null, (Date) potentialValue, null);

        if (actualValue instanceof NCLListType)
            return new NCLAttributeRecord(attrID, null, null, (NCLListType) potentialValue);

        return new NCLAttributeRecord(attrID, potentialValue == null ? null : potentialValue.toString(), null, null);
    }

    private NCLReferenceRecord getReferenceRecord(Object actualValue, Object potentialValue, BigInteger attrID) {
        if (Objects.isNull(actualValue)) {
            return new NCLReferenceRecord(NCLReferenceAction.INSERT, attrID, (BigInteger) potentialValue);
        }
        if (Objects.isNull(potentialValue)) {
            return new NCLReferenceRecord(NCLReferenceAction.DELETE, attrID, (BigInteger) actualValue);
        }
        return new NCLReferenceRecord(NCLReferenceAction.UPDATE, attrID, (BigInteger) actualValue, (BigInteger) potentialValue);
    }

    private String convertToString(Boolean condition) {
        return condition ? "1" : "0";
    }

    private void openAccessibleFields(Field[] fields) {
        for (Field field : fields) {
            field.setAccessible(true);
        }
    }

    private Field getFieldByName(String name, Field[] fields) {
        return Stream.of(fields).filter(field -> field.getName().equals(name)).findFirst().orElse(null);
    }

    /**
     * Function interface that contains method that receives three parameters, and returns result value.
     *
     * @param <T> - is result type
     * @param <B> - first parameter
     * @param <C> - second parameter
     * @param <R> - third parameter
     */
    @FunctionalInterface
    interface TripleFunction<T, B, C, R> {
        T get(B b, C c, R r);
    }
}