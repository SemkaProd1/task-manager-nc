package com.tmnc.core.ncl;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class NCLObjectConvertException extends TMNCApplicationException {
	public NCLObjectConvertException() {
	}

	public NCLObjectConvertException(String message) {
		super(message);
	}

	public NCLObjectConvertException(String message, Throwable cause) {
		super(message, cause);
	}

	public NCLObjectConvertException(Throwable cause) {
		super(cause);
	}
}
