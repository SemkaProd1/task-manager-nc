package com.tmnc.core.ncl;

import com.tmnc.core.TMNCPreparedStatementCreatorFactory;
import com.tmnc.core.exceptions.TMNCApplicationException;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.core.ncl.update.NCLReferenceRecord;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class NCLObjectRepository {

    private final JdbcTemplate template;
    private final NCLObjectResultSetExtractor extractor;
    private final NCLObjectListResultSetExtractor nclObjectListResultSetExtractor;
    private final NCLObjectConvertExecutor convertExecutor;

    @Autowired
    public NCLObjectRepository(JdbcTemplate template,
                               NCLObjectResultSetExtractor nclObjectResultSetExtractor,
                               NCLObjectListResultSetExtractor nclObjectListResultSetExtractor,
                               NCLObjectConvertExecutor convertExecutor) {
        this.template = template;
        this.extractor = nclObjectResultSetExtractor;
        this.nclObjectListResultSetExtractor = nclObjectListResultSetExtractor;
        this.convertExecutor = convertExecutor;
    }

    public NCLObject executeQuery(String sqlQuery, SqlParameterSource source) {
        PreparedStatementCreator preparedStatementCreator =
                TMNCPreparedStatementCreatorFactory.create(sqlQuery, source);
        return this.template.query(preparedStatementCreator, extractor);
    }

    public <T> T executeQuery(String sqlQuery, SqlParameterSource source, NCLToObjectConverter<T> converter) {
        NCLObject object = this.executeQuery(sqlQuery, source);
        return this.convertExecutor.executeConvert(object, converter);
    }

    public NCLObject selectObject(BigInteger objectId, List<BigInteger> attrIds) {
        CallableStatementCreator creator = connection -> {
            OracleConnection oracleConnection = getOracleConnection(connection);

            ArrayDescriptor attrTableDescriptor = ArrayDescriptor.createDescriptor("TMNC.NUMBERS", oracleConnection);
            ARRAY attrTable = new ARRAY(attrTableDescriptor, oracleConnection,
                    attrIds.stream().map(BigDecimal::new).toArray());

            OracleCallableStatement callableStatement = (OracleCallableStatement) oracleConnection.prepareCall(
                    "select * from table(tmnc_commons.get_obj_infos(?, ?))",
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);

            callableStatement.setBigDecimal(1, new BigDecimal(objectId));
            callableStatement.setARRAY(2, attrTable);

            return callableStatement;
        };

        return template.execute(creator, preparedStatement ->
                extractor.extractData(preparedStatement.executeQuery()));
    }

    public <T> T selectObject(BigInteger objectId, List<BigInteger> attrIds, NCLToObjectConverter<T> converter) {
        NCLObject object = this.selectObject(objectId, attrIds);
        return convertExecutor.executeConvert(object, converter);
    }

    public List<NCLObject> selectObjects(List<BigInteger> objectIds, List<BigInteger> attrIds) {
        CallableStatementCreator creator = connection -> {
            OracleConnection oracleConnection = getOracleConnection(connection);

            ArrayDescriptor attrIdsDescriptor = ArrayDescriptor.createDescriptor("TMNC.NUMBERS", oracleConnection);

            ARRAY attrArray = new ARRAY(attrIdsDescriptor, oracleConnection,
                    attrIds.stream().map(BigDecimal::new).toArray());
            ARRAY objectArray = new ARRAY(attrIdsDescriptor, oracleConnection,
                    objectIds.stream().map(BigDecimal::new).toArray());

            OracleCallableStatement callableStatement = (OracleCallableStatement) oracleConnection.prepareCall(
                    "select * from table(tmnc_commons.get_objects(?, ?))",
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            callableStatement.setARRAY(1, objectArray);
            callableStatement.setARRAY(2, attrArray);
            return callableStatement;
        };

        return template.execute(creator, callableStatement ->
                this.nclObjectListResultSetExtractor.extractData(callableStatement.executeQuery()));
    }

    public List<NCLObject> selectObjectsAllAttrs(List<BigInteger> objectIds) {
        CallableStatementCreator creator = connection -> {
            OracleConnection oracleConnection = getOracleConnection(connection);
            ArrayDescriptor attrIdsDescriptor = ArrayDescriptor.createDescriptor("TMNC.NUMBERS", oracleConnection);
            ARRAY objectArray = new ARRAY(attrIdsDescriptor, oracleConnection,
                    objectIds.stream().map(BigDecimal::new).toArray());

            OracleCallableStatement callableStatement = (OracleCallableStatement) oracleConnection.prepareCall(
                    "select * from table(tmnc_commons.get_objects_all_attrs(?))",
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            callableStatement.setARRAY(1, objectArray);
            return callableStatement;
        };

        return template.execute(creator, callableStatement ->
                this.nclObjectListResultSetExtractor.extractData(callableStatement.executeQuery()));
    }

    public BigInteger update(BigInteger objectId,
                             BigInteger parentId,
                             List<NCLAttributeRecord> attrs,
                             List<NCLReferenceRecord> refs) {

        List<NCLAttributeRecord> attributes = attrs == null ? Collections.emptyList() : attrs;
        List<NCLReferenceRecord> references = refs == null ? Collections.emptyList() : refs;

        CallableStatementCreator creator = connection -> {
            OracleConnection oracleConnection = getOracleConnection(connection);
            OracleCallableStatement callableStatement = (OracleCallableStatement) oracleConnection.prepareCall(
                    "{? = call tmnc_commons.update_object(?, ?, ?, ?)}");

            List<STRUCT> attrTableValue = new ArrayList<>();

            for (NCLAttributeRecord record : attributes) {
                attrTableValue.add(record.toStructValue(oracleConnection));
            }

            ArrayDescriptor attrTableDescriptor = ArrayDescriptor.createDescriptor("TMNC.ATTR_TABLE", oracleConnection);
            ARRAY attrTable = new ARRAY(attrTableDescriptor, oracleConnection, attrTableValue.toArray());

            List<STRUCT> refsTableValue = new ArrayList<>();

            for (NCLReferenceRecord record : references) {
                refsTableValue.add(record.toStructValue(oracleConnection));
            }

            ArrayDescriptor refsTableDescriptor =
                    ArrayDescriptor.createDescriptor("TMNC.REFERENCE_TABLE", oracleConnection);
            ARRAY refsTable = new ARRAY(refsTableDescriptor, oracleConnection, refsTableValue.toArray());

            callableStatement.registerOutParameter(1, Types.NUMERIC);
            callableStatement.setBigDecimal(2, new BigDecimal(objectId));
            callableStatement.setBigDecimal(3, parentId == null ? null : new BigDecimal(parentId));
            callableStatement.setARRAY(4, attrTable);
            callableStatement.setARRAY(5, refsTable);
            return callableStatement;
        };

        return template.execute(creator, callableStatement -> {
            callableStatement.execute();
            return callableStatement.getBigDecimal(1).toBigInteger();
        });
    }

    public BigInteger saveObject(String catalogName,
                                 String functionName,
                                 SqlParameter[] sqlParameters,
                                 MapSqlParameterSource sqlParameterSource) {
        SimpleJdbcCall call = new SimpleJdbcCall(template)
                .withCatalogName(catalogName)
                .withFunctionName(functionName)
                .declareParameters(sqlParameters)
                .withReturnValue();

        return call.executeFunction(BigDecimal.class, sqlParameterSource).toBigInteger();
    }

    public <T> T getObject(BigInteger objectId, NCLToObjectConverter<T> converter) {
        return executeQuery(
                "SELECT * from table (tmnc_commons.GET_OBJ_INFO(:id))",
                new MapSqlParameterSource("id", objectId),
                converter
        );
    }

    public BigInteger deleteObject(BigInteger id) {
        SimpleJdbcCall call = new SimpleJdbcCall(template)
                .withCatalogName("tmnc_commons")
                .withFunctionName("delete_object")
                .declareParameters(new SqlParameter("obj_id", Types.NUMERIC))
                .withReturnValue();
        return call.executeFunction(
                BigDecimal.class,
                new MapSqlParameterSource()
                        .addValue("obj_id", id))
                .toBigInteger();
    }

    public boolean updateReference(boolean bind, BigInteger attrId, BigInteger objectId, BigInteger referenceId) {
        SimpleJdbcCall call = new SimpleJdbcCall(template)
                .withCatalogName("tmnc_commons")
                .declareParameters(new SqlParameter("attribute_id", Types.NUMERIC),
                        new SqlParameter("object_id_in", Types.NUMERIC),
                        new SqlParameter("reference_id", Types.NUMERIC));
        if (bind) {
            call.withProcedureName("bind_ref");
        } else {
            call.withProcedureName("unbind_ref");
        }
        call.executeFunction(Void.class,
                new MapSqlParameterSource()
                        .addValue("attribute_id", attrId)
                        .addValue("object_id_in", objectId)
                        .addValue("reference_id", referenceId));
        return true;
    }


    public BigInteger getFirstParent(BigInteger id) {
        SimpleJdbcCall call = new SimpleJdbcCall(template)
                .withCatalogName("tmnc_commons")
                .withFunctionName("GET_PARENT")
                .declareParameters(new SqlParameter("object_id_in", Types.NUMERIC))
                .withReturnValue();
        return call.executeFunction(
                BigDecimal.class,
                new MapSqlParameterSource()
                        .addValue("object_id_in", id))
                .toBigInteger();
    }

    public <T> List<T> getListEntities(List<BigInteger> entityIDs, NCLToObjectConverter<T> converter) {
        List<NCLObject> objects = selectObjectsAllAttrs(entityIDs);
        if (objects.isEmpty() || objects.get(0).getObjectId() == null) {
            return new ArrayList<>();
        }
        List<T> entities = new ArrayList<>();
        for (NCLObject object : objects) {
            entities.add(convertExecutor.executeConvert(object, converter));
        }
        return entities;
    }

    public List<NCLObject> getObjectsByRefference(BigInteger attrIdIn, BigInteger referenceIdIn) {
        CallableStatementCreator creator = connection -> {
            OracleConnection oracleConnection = getOracleConnection(connection);
            OracleCallableStatement callableStatement = (OracleCallableStatement) oracleConnection.prepareCall(
                    "select * from table(tmnc_commons.get_obj_id_by_ref(?, ?))",
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY
            );

            callableStatement.setBigDecimal(1, new BigDecimal(attrIdIn));
            callableStatement.setBigDecimal(2, new BigDecimal(referenceIdIn));

            return callableStatement;
        };

        return template.execute(creator, callableStatement ->
                this.nclObjectListResultSetExtractor.extractData(callableStatement.executeQuery()));
    }

    public String getObjectName(BigInteger id) {
        SimpleJdbcCall call = new SimpleJdbcCall(template)
                .withCatalogName("tmnc_commons")
                .withFunctionName("get_object_name")
                .declareParameters(new SqlParameter("object_id_in", Types.NUMERIC))
                .withReturnValue();
        return call.executeFunction(
                String.class,
                new MapSqlParameterSource()
                        .addValue("object_id_in", id));
    }

    private OracleConnection getOracleConnection(Connection wrappedConnection) throws SQLException {
        if (!wrappedConnection.isWrapperFor(OracleConnection.class)) {
            throw new TMNCApplicationException("Could not get oracle connection");
        }

        return wrappedConnection.unwrap(OracleConnection.class);
    }
}