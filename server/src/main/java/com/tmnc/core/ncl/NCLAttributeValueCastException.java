package com.tmnc.core.ncl;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class NCLAttributeValueCastException extends TMNCApplicationException {
	public NCLAttributeValueCastException() {
	}

	public NCLAttributeValueCastException(String message) {
		super(message);
	}

	public NCLAttributeValueCastException(String message, Throwable cause) {
		super(message, cause);
	}

	public NCLAttributeValueCastException(Throwable cause) {
		super(cause);
	}
}
