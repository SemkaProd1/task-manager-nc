package com.tmnc.core.ncl.update;

import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;

public class NCLReferenceRecord implements Struct {

    private NCLReferenceAction action;
    private BigInteger attrId;
    private BigInteger refId;
    private BigInteger newRefId;

    public NCLReferenceRecord(NCLReferenceAction action,
                              BigInteger attrId,
                              BigInteger refId) {
        this(action, attrId, refId, null);
    }

    public NCLReferenceRecord(NCLReferenceAction action,
                              BigInteger attrId,
                              BigInteger refId,
                              BigInteger newRefId) {
        this.action = action;
        this.attrId = attrId;
        this.refId = refId;
        this.newRefId = newRefId;
    }

    public NCLReferenceAction getAction() {
        return action;
    }

    public void setAction(NCLReferenceAction action) {
        this.action = action;
    }

    public BigInteger getAttrId() {
        return attrId;
    }

    public void setAttrId(BigInteger attrId) {
        this.attrId = attrId;
    }

    public BigInteger getRefId() {
        return refId;
    }

    public void setRefId(BigInteger refId) {
        this.refId = refId;
    }

    public BigInteger getNewRefId() {
        return newRefId;
    }

    public void setNewRefId(BigInteger newRefId) {
        this.newRefId = newRefId;
    }

    @Override
    public STRUCT toStructValue(Connection connection) throws SQLException {
        StructDescriptor descriptor = StructDescriptor.createDescriptor("TMNC.REFERENCE_RECORD", connection);
        Object[] objects = new Object[]{
                this.action.toString(),
                new BigDecimal(this.attrId),
                new BigDecimal(this.refId),
                this.newRefId == null ? null : new BigDecimal(this.newRefId)
        };
        return new STRUCT(descriptor, connection, objects);
    }
}
