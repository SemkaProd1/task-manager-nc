package com.tmnc.core.ncl;


import java.math.BigInteger;
import java.util.Objects;

public enum NCLType {
	PROJECT(1),
	USER_ACCOUNT(2),
	PARTICIPANT(3),
	SPRINT(4),
	TASK(5),
	DOCUMENT(6),
	LABEL(7),
	COMMENT(8),
	LOG(9),
	INVITE(10);

	private final BigInteger objectTypeId;

	NCLType(int objectTypeId) {
		this.objectTypeId = BigInteger.valueOf(objectTypeId);
	}

	public BigInteger getObjectTypeId() {
		return objectTypeId;
	}

	public static NCLType fromId(int objectTypeId) {
		return fromId(BigInteger.valueOf(objectTypeId));
	}

	public static NCLType fromId(BigInteger objectTypeId) {
		for (NCLType type: NCLType.values()) {
			if (Objects.deepEquals(objectTypeId, type.getObjectTypeId())) {
				return type;
			}
		}

		throw new NCLTypeNotFoundException();
	}
}
