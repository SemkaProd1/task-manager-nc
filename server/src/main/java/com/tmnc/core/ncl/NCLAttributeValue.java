package com.tmnc.core.ncl;


import com.tmnc.data.utils.NCLListTypeUtils;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

public class NCLAttributeValue {
	private String value;
	private Date dateValue;
	private NCLListType listValue;

	private NCLAttributeValue() {
	}

	public String getValue() {
		return value;
	}

	public Date getDateValue() {
		return dateValue;
	}

	public NCLListType getListValue() {
		return listValue;
	}

	public static NCLAttributeValue fromObject(String rawValue, String attrType) {
		NCLAttributeValue nclAttributeValue = new NCLAttributeValue();

		if (Objects.isNull(attrType)) {
			return nclAttributeValue;
		}

		switch (attrType) {
			case "ATTR":
				nclAttributeValue.value = rawValue;
				break;
			case "LIST_VALUE":
				nclAttributeValue.listValue = NCLListTypeUtils.resolve(Integer.parseInt(rawValue));
				break;
			case "DATE":
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.getDefault());
				nclAttributeValue.dateValue = Date.valueOf(LocalDate.parse(rawValue, formatter));
				break;
			default:
				throw new NCLAttributeValueCastException("AttrType '" + attrType + "' is not supported!");
		}

		return nclAttributeValue;
	}

	public static NCLAttributeValue nullValue() {
		return new NCLAttributeValue();
	}
}
