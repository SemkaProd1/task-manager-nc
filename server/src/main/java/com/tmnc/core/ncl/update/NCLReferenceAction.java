package com.tmnc.core.ncl.update;

public enum NCLReferenceAction {
    INSERT,
    DELETE,
    UPDATE
}
