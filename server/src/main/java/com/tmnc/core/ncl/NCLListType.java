package com.tmnc.core.ncl;

import java.math.BigInteger;

public interface NCLListType {

    BigInteger getListTypeId();
    String getListTypeValue();
}
