package com.tmnc.core.ncl;

import java.math.BigInteger;
import java.util.List;

public class NCLAttribute {
	private BigInteger id;
	private String type;
	private String name;
	private NCLAttributeValue value;
	private NCLType referenceType;
	private List<BigInteger> references;

	private NCLAttribute(Builder builder) {
		this.id = builder.id;
		this.referenceType = builder.referenceType;
		this.name = builder.attributeName;
		this.value = builder.attributeValue;
		this.type = builder.attributeType;
		this.references = builder.references;
	}

	public NCLType getReferenceType() {
		return referenceType;
	}

	public List<BigInteger> getReferences() {
		return references;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NCLAttributeValue getAttributeValue() {
		return value;
	}

	public void setValue(NCLAttributeValue value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "NCLAttribute{" +
				"id=" + id +
				", type='" + type + '\'' +
				", name='" + name + '\'' +
				", value=" + value +
				", referenceType=" + referenceType +
				", references=" + references +
				'}';
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private NCLType referenceType;
		private BigInteger id;
		private String attributeName;
		private NCLAttributeValue attributeValue;
		private List<BigInteger> references;
		private String attributeType;

		public Builder referenceType(NCLType referenceType) {
			this.referenceType = referenceType;
			return this;
		}

		public Builder id(BigInteger id) {
			this.id = id;
			return this;
		}

		public Builder name(String attributeName) {
			this.attributeName = attributeName;
			return this;
		}

		public Builder value(NCLAttributeValue attributeValue) {
			this.attributeValue = attributeValue;
			return this;
		}

		public Builder references(List<BigInteger> references) {
			this.references = references;
			return this;
		}

		public Builder type(String attributeType) {
			this.attributeType = attributeType;
			return this;
		}

		public NCLAttribute build() {
			return new NCLAttribute(this);
		}

	}
}
