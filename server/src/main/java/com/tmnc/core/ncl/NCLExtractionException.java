package com.tmnc.core.ncl;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class NCLExtractionException extends TMNCApplicationException {
	public NCLExtractionException() {
	}

	public NCLExtractionException(String message) {
		super(message);
	}

	public NCLExtractionException(String message, Throwable cause) {
		super(message, cause);
	}

	public NCLExtractionException(Throwable cause) {
		super(cause);
	}
}
