package com.tmnc.core.ncl.update;

import oracle.sql.STRUCT;

import java.sql.Connection;
import java.sql.SQLException;

public interface Struct {
    STRUCT toStructValue(Connection connection) throws SQLException;
}
