package com.tmnc.core.ncl;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class NCLObject {
	private BigInteger objectId;
	private BigInteger parentId;
	private NCLType objectType;
	private Map<String, NCLAttribute> attributes;

	private NCLObject(Builder builder) {
		this.objectType = builder.objectType;
		this.attributes = builder.attributes;
		this.objectId = builder.objectId;
		this.parentId = builder.parentId;
	}

	public BigInteger getParentId() {
		return parentId;
	}

	public void setParentId(BigInteger parentId) {
		this.parentId = parentId;
	}

	public BigInteger getObjectId() {
		return objectId;
	}

	public void setObjectId(BigInteger objectId) {
		this.objectId = objectId;
	}

	public NCLType getObjectType() {
		return objectType;
	}

	public void setObjectType(NCLType objectTypeId) {
		this.objectType = objectTypeId;
	}

	public Map<String, NCLAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, NCLAttribute> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "NCLObject{" +
				"objectId=" + objectId +
				", objectType=" + objectType +
				", attributes=" + attributes +
				'}';
	}

	public static Builder forType(NCLType objectType) {
		return new Builder(objectType);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		BigInteger parentId;
		BigInteger objectId;
		NCLType objectType;
		Map<String, NCLAttribute> attributes;

		public Builder(NCLType objectType) {
			this.objectType = objectType;
			this.attributes = new HashMap<>();
		}

		public Builder() {
			this(null);
		}

		public Builder attribute(NCLAttribute attribute) {
			this.attributes.put(attribute.getName(), attribute);
			return this;
		}

		public Builder objType(NCLType type) {
			this.objectType = type;
			return this;
		}

		public Builder objectId(BigInteger objectId) {
			this.objectId = objectId;
			return this;
		}

		public Builder parentId(BigInteger parentId) {
			this.parentId = parentId;
			return this;
		}

		public NCLObject build() {
			return new NCLObject(this);
		}
	}
}
