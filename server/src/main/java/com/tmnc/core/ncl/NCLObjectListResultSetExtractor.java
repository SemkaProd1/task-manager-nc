package com.tmnc.core.ncl;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class NCLObjectListResultSetExtractor implements ResultSetExtractor<List<NCLObject>> {

    private final NCLExtractAttributeHandler handler;

    public NCLObjectListResultSetExtractor(NCLExtractAttributeHandler handler) {
        this.handler = handler;
    }

    @Override
    public List<NCLObject> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List<NCLObject> list = new ArrayList<>();

        NCLObject.Builder object = NCLObject.builder();

        while (resultSet.next()) {
            BigInteger attributeId = resultSet.getBigDecimal("attribute_id").toBigInteger();

            if (NCLExtractAttributeHandler.OBJECT_ID.equals(attributeId)) {
                if (resultSet.getRow() != 1) {
                    list.add(object.build());
                }

                object = NCLObject.builder();
            }

            object = this.handler.extractData(resultSet, object);
        }
        list.add(object.build());

        return list;
    }
}