package com.tmnc.core.ncl.update;

import com.tmnc.core.ncl.NCLListType;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

public class NCLAttributeRecord implements Struct {
    private BigInteger attrId;
    private String value;
    private Date dateValue;
    private NCLListType listValue;

    public NCLAttributeRecord(BigInteger attrId, String value, Date dateValue, NCLListType listValue) {
        this.attrId = attrId;
        this.value = value;
        this.dateValue = dateValue;
        this.listValue = listValue;
    }

    public BigInteger getAttrId() {
        return attrId;
    }

    public void setAttrId(BigInteger attrId) {
        this.attrId = attrId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public NCLListType getListValue() {
        return listValue;
    }

    public void setListValue(NCLListType listValue) {
        this.listValue = listValue;
    }

    @Override
    public STRUCT toStructValue(Connection connection) throws SQLException {
        StructDescriptor descriptor = StructDescriptor.createDescriptor("TMNC.ATTR_RECORD", connection);
        Object[] objects = new Object[]{
                new BigDecimal(this.attrId),
                this.value,
                this.dateValue,
                this.listValue == null ? null : new BigDecimal(this.listValue.getListTypeId())
        };
        return new STRUCT(descriptor, connection, objects);
    }
}
