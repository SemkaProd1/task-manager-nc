package com.tmnc.core.ncl;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class NCLObjectResultSetExtractor implements ResultSetExtractor<NCLObject> {

	private final NCLExtractAttributeHandler handler;

	public NCLObjectResultSetExtractor(NCLExtractAttributeHandler handler) {
		this.handler = handler;
	}

	@Override
	public NCLObject extractData(ResultSet rs) throws SQLException, DataAccessException {

		NCLObject.Builder object = NCLObject.builder();

		while (rs.next()) {
			object = handler.extractData(rs, object);
		}

		return object.objectId == null ? null : object.build();
	}
}