package com.tmnc.core.ncl;

import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

@Component
public class NCLFromMapConverter {
	public NCLObject convert(List<Map<String, Object>> rawAttrs) {
		NCLObject.Builder builder = NCLObject.builder();

		ListIterator<Map<String, Object>> mapListIterator = rawAttrs.listIterator();

		while (mapListIterator.hasNext()) {
			Map<String, Object> rawAttr = mapListIterator.next();

			BigInteger attributeId = new BigInteger(rawAttr.get("ATTRIBUTE_ID").toString());
			String attributeValue = rawAttr.get("ATTRIBUTE_VALUE").toString();
			String attrName = rawAttr.get("ATTRIBUTE_NAME").toString();
			String attrType = rawAttr.get("ATTRIBUTE_TYPE").toString();
			Object rawObjectTypeID = rawAttr.get("ATTRIBUTE_OBJECT_TYPE_ID");

			if (attributeId.equals(NCLExtractAttributeHandler.OBJECT_TYPE)) {
				builder.objType(NCLType.fromId(new BigInteger(attributeValue)));
				continue;
			}

			if (attributeId.equals(NCLExtractAttributeHandler.OBJECT_ID)) {
				builder.objectId(new BigInteger(attributeValue));
				continue;
			}

			if (attributeId.equals(NCLExtractAttributeHandler.PARENT_ID)) {
				builder.parentId(attributeValue == null ? null : new BigInteger(attributeValue));
				continue;
			}

			NCLAttribute.Builder attr = NCLAttribute.builder()
					.id(attributeId)
					.name(attrName)
					.type(attrType);

			if (rawObjectTypeID != null) {
				attr.referenceType(NCLType.fromId(new BigInteger(rawObjectTypeID.toString())));
				attr.references(this.getRefsByType(rawAttr, mapListIterator, attributeId));
			} else {
				attr.value(NCLAttributeValue.fromObject(attributeValue, attrType));
			}

			builder.attribute(attr.build());
		}

		return builder.build();
	}

	private List<BigInteger> getRefsByType(Map<String, Object> current,
	                                       ListIterator<Map<String, Object>> rawAttrs,
	                                       BigInteger attrId) {
		List<BigInteger> refs = new ArrayList<>();
		Map<String, Object> element = current;

		while (true) {
			if (!attrId.equals(new BigInteger(element.get("ATTRIBUTE_ID").toString()))) {
				rawAttrs.previous();
				break;
			}

			refs.add(new BigInteger(element.get("attribute_value").toString()));

			if (!rawAttrs.hasNext()) {
				break;
			} else {
				element = rawAttrs.next();
			}
		}

		return refs;
	}
}
