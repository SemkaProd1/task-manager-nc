package com.tmnc.core.exceptions.file;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class FileStorageDeleteException extends TMNCApplicationException {
    public FileStorageDeleteException() {
    }

    public FileStorageDeleteException(String message) {
        super(message);
    }

    public FileStorageDeleteException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileStorageDeleteException(Throwable cause) {
        super(cause);
    }
}
