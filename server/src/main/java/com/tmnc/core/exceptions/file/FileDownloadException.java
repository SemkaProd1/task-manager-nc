package com.tmnc.core.exceptions.file;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class FileDownloadException extends TMNCApplicationException {

    public FileDownloadException() {
    }

    public FileDownloadException(String message) {
        super(message);
    }

    public FileDownloadException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileDownloadException(Throwable cause) {
        super(cause);
    }


}
