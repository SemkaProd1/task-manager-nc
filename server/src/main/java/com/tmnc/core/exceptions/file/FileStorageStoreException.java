package com.tmnc.core.exceptions.file;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class FileStorageStoreException extends TMNCApplicationException {
    public FileStorageStoreException() {
    }

    public FileStorageStoreException(String message) {
        super(message);
    }

    public FileStorageStoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileStorageStoreException(Throwable cause) {
        super(cause);
    }
}
