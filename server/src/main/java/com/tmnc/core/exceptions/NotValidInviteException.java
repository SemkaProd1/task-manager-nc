package com.tmnc.core.exceptions;

public class NotValidInviteException extends RuntimeException {

    public NotValidInviteException(String url) {
        super("The invite [" + url + "] is not exist.");
    }
}
