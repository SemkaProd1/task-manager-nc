package com.tmnc.core.exceptions;

public class TMNCApplicationException extends RuntimeException {
	public TMNCApplicationException() {
	}

	public TMNCApplicationException(String message) {
		super(message);
	}

	public TMNCApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public TMNCApplicationException(Throwable cause) {
		super(cause);
	}
}
