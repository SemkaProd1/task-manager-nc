package com.tmnc.core.exceptions.file;

import com.tmnc.core.exceptions.TMNCApplicationException;

public class FileStorageInitException extends TMNCApplicationException {
    public FileStorageInitException() {
    }

    public FileStorageInitException(String message) {
        super(message);
    }

    public FileStorageInitException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileStorageInitException(Throwable cause) {
        super(cause);
    }
}
