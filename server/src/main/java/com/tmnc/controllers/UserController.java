package com.tmnc.controllers;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.data.converters.NCLToParticipantConverter;
import com.tmnc.data.entities.Participant;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.participant.ParticipantResponsePayload;
import com.tmnc.data.exchange.project.ProjectInfoForUserResponsePayload;
import com.tmnc.data.exchange.task.TaskToAssigneeResponsePayload;
import com.tmnc.data.exchange.users.*;
import com.tmnc.services.*;
import com.tmnc.utils.ResponseEntityFactory;
import com.tmnc.validation.UpdateUserEmailValidation;
import com.tmnc.validation.UpdateUserPasswordValidator;
import com.tmnc.validation.UpdateUserValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/usr")
public class UserController {

    private final UserService userService;
    private final UpdateUserValidation validator;
    private final FileUploadStorageService storageService;
    private final TaskService taskService;
    private final UpdateUserEmailValidation emailValidation;
    private final UpdateUserPasswordValidator passwordValidator;
    private final NCLObjectRepository repository;
    private final NCLToParticipantConverter converter;
    private final LoginService loginService;
    private final UserAuthenticationService userAuthenticationService;

    @Autowired
    public UserController(UserService userService, UpdateUserValidation validator,
                          FileUploadStorageService storageService, TaskService taskService,
                          UpdateUserEmailValidation emailValidation, UpdateUserPasswordValidator passwordValidator,
                          NCLObjectRepository repository,
                          NCLToParticipantConverter converter,
                          LoginService loginService,
                          UserAuthenticationService userAuthenticationService) {
        this.userService = userService;
        this.validator = validator;
        this.storageService = storageService;
        this.taskService = taskService;
        this.emailValidation = emailValidation;
        this.passwordValidator = passwordValidator;
        this.repository = repository;
        this.converter = converter;
        this.loginService = loginService;
        this.userAuthenticationService = userAuthenticationService;
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    public UserAccount greet(@PathVariable("id") BigInteger id) {
        return userService.getUser(id);
    }

    @InitBinder("updateUserEmailRequestPayload")
    public void initUpdateUserEmailBinder(WebDataBinder binder) {
        binder.setValidator(this.emailValidation);
    }

    @InitBinder("updateUserPasswordRequestPayload")
    public void initUpdateUserPasswordBinder(WebDataBinder binder) {
        binder.setValidator(this.passwordValidator);
    }

    @InitBinder("updateUserRequestPayload")
    public void initUpdateUserBinder(WebDataBinder binder) {
        binder.setValidator(this.validator);
    }

    @PostMapping(value = "/update", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> updateUser(@Validated UpdateUserRequestPayload payload,
                                        Errors errors,
                                        HttpServletRequest request) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }

        BigInteger bigInteger = this.userService.updateUserRequestPayload(payload);
        String userName = this.userService.getUser(bigInteger).getUserName();

        this.reloadUser(userName, request);

        return ResponseEntity.ok(bigInteger);
    }

    @GetMapping("/isMailExists")
    public int isEmailExists(@RequestParam("email") String email) {
        return userService.isEmailExists(email) ? 1 : 0;
    }


    @GetMapping("/isUsernameExists")
    public int isUserExists(@RequestParam("username") String username) {
        return userService.isUserExist(username) ? 1 : 0;
    }

    @PostMapping("/update/email")
    public ResponseEntity<?> updateEmail(@Validated @RequestBody UpdateUserEmailRequestPayload updateUserEmailRequestPayload,
                                         Errors errors,
                                         HttpServletRequest request) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }
        BigInteger bigInteger = this.userService.updateEmail(updateUserEmailRequestPayload);
        String userName = this.userService.getUser(bigInteger).getUserName();

        this.reloadUser(userName, request);

        return ResponseEntity.ok(bigInteger);
    }

    @PostMapping("/update/password")
    public ResponseEntity<?> updatePassword(@Validated @RequestBody UpdateUserPasswordRequestPayload updateUserPasswordRequestPayload,
                                            Errors errors) throws IOException {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }
        return ResponseEntity.ok(this.userService.updatePassword(updateUserPasswordRequestPayload));
    }

    @GetMapping(value = "/{userId}/avatar", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public void downloadAvatar(HttpServletResponse response, @PathVariable BigInteger userId) {
        this.userService.getUserAvatar(response, userId);
    }

    @GetMapping("/{userId}/userProjects")
    public List<ProjectInfoForUserResponsePayload> getProjectInfo(@PathVariable BigInteger userId) {
        return this.userService.getProjectInfo(userId);
    }

    @GetMapping("/getTasks/{assigneeId}")
    public List<TaskToAssigneeResponsePayload> getTasksByAssignee(@PathVariable BigInteger assigneeId) {
        return taskService.getTasksByAssignee(assigneeId);
    }

    @GetMapping("/getCurrentParticipant")
    public ParticipantResponsePayload getCurrentParticipant(@RequestParam BigInteger userId,
                                                            @RequestParam BigInteger projectId) {
        return userService.getCurrentParticipant(userId, projectId);
    }

    @DeleteMapping("/remove/{userId}")
    public boolean removeTask(@PathVariable BigInteger userId) {
        return this.userService.delete(userId);
    }

    private LoginUserResponsePayload reloadUser(String username, HttpServletRequest request) {
        UserDetails userDetails = this.userAuthenticationService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());

        return this.loginService.updatePrincipals(token, request, true);
    }
}