package com.tmnc.controllers;

import com.tmnc.data.entities.Task;
import com.tmnc.data.exchange.documentation.CreateNewDocumentationRequestPayload;
import com.tmnc.data.exchange.task.CreateNewTaskRequestPayload;
import com.tmnc.data.exchange.task.UpdateStatusTaskRequestPayload;
import com.tmnc.data.exchange.task.UpdateTaskRequestPayload;
import com.tmnc.services.TaskService;
import com.tmnc.utils.ResponseEntityFactory;
import com.tmnc.validation.TaskValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/project/task")
public class TaskController {

    private final TaskValidator validator;
    private final TaskService service;

    @Autowired
    public TaskController(TaskValidator validator, TaskService service) {
        this.validator = validator;
        this.service = service;
    }

    @InitBinder("createNewTaskRequestPayload")
    public void initCreateNewTaskBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @PostMapping(value = "/new", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> createNewTask(
            @Validated CreateNewTaskRequestPayload payload, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }
        return ResponseEntity.ok(this.service.create(payload));
    }

    @DeleteMapping("/removeTask/{taskId}")
    public boolean removeTask(@PathVariable BigInteger taskId) {
        return this.service.delete(taskId);
    }

    @PostMapping("/setStatus")
    public boolean setStatus(@RequestBody UpdateStatusTaskRequestPayload payload) {
        return service.setTaskStatus(payload.getObjectId(), payload.getStatus());
    }

    @GetMapping("/setAssignee")
    public boolean setAssignee(@RequestParam BigInteger taskId, @RequestParam BigInteger participantId) {
        return this.service.addAssigneeToTask(taskId, participantId);
    }

    @DeleteMapping("/removeAssignee")
    public boolean removeAssignee(@RequestParam BigInteger taskId, @RequestParam BigInteger participantId) {
        return this.service.unbindAssigneeFromTask(taskId, participantId);
    }

    @GetMapping("/setLabel")
    public boolean setLabel(@RequestParam BigInteger taskId, @RequestParam BigInteger labelId) {
        return this.service.addLabelToTask(taskId, labelId);
    }

    @DeleteMapping("/removeLabel")
    public boolean removeLabel(@RequestParam BigInteger taskId, @RequestParam BigInteger labelId) {
        return this.service.unbindLabelFromTask(taskId, labelId);
    }


    @PostMapping(value = "/addDocument", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public boolean addDocument(CreateNewDocumentationRequestPayload payload, @RequestParam BigInteger taskId) {
        return this.service.addDocumentToTask(payload, taskId);
    }

    @DeleteMapping("/removeDocument/{documentId}")
    public boolean removeDocument(@PathVariable BigInteger documentId) {
        return this.service.unbindDocumentFromTask(documentId);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateTask(
            @Validated @RequestBody UpdateTaskRequestPayload updateTaskRequestPayload, Errors errors) {
            if(errors.hasErrors()){
                return ResponseEntityFactory.errorAfterValidation(errors);
            }
        return ResponseEntity.ok(this.service.update(updateTaskRequestPayload));
    }

    @GetMapping("/get/{taskId}")
    public Task getTask(@PathVariable BigInteger taskId) {
        return this.service.get(taskId);
    }
}