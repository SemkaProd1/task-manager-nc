package com.tmnc.controllers;


import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.data.entities.Participant;
import com.tmnc.data.exchange.participant.GetParticipantsRequestPayload;
import com.tmnc.data.exchange.project.CreateNewProjectRequestPayload;
import com.tmnc.data.exchange.project.ProjectInfoResponsePayload;
import com.tmnc.data.exchange.project.ProjectRequestPayload;
import com.tmnc.data.exchange.project.UpdateProjectRequestPayload;
import com.tmnc.services.ParticipantService;
import com.tmnc.services.ProjectService;
import com.tmnc.utils.ResponseEntityFactory;
import com.tmnc.validation.CreateNewProjectInputValidator;
import com.tmnc.validation.UpdateProjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/api/project")
public class ProjectController {

    private final ProjectService projectService;
    private final CreateNewProjectInputValidator createNewProjectInputValidator;
    private final UpdateProjectValidator updateProjectValidator;
    private final ParticipantService participantService;
    private final NCLObjectRepository nclObjectRepository;

    @Autowired
    public ProjectController(ProjectService projectService,
                             CreateNewProjectInputValidator createNewProjectInputValidator,
                             UpdateProjectValidator updateProjectValidator,
                             ParticipantService participantService,
                             NCLObjectRepository nclObjectRepository) {
        this.projectService = projectService;
        this.createNewProjectInputValidator = createNewProjectInputValidator;
        this.updateProjectValidator = updateProjectValidator;
        this.participantService = participantService;
        this.nclObjectRepository = nclObjectRepository;
    }

    @InitBinder("createNewProjectRequestPayload")
    public void initCreateNewProjectBinder(WebDataBinder binder) {
        binder.setValidator(this.createNewProjectInputValidator);
    }

    @InitBinder("updateProjectRequestPayload")
    public void initUpdateNewProjectBinder(WebDataBinder binder) {
        binder.setValidator(this.updateProjectValidator);
    }

    @GetMapping("/get/{id}")
    public ProjectInfoResponsePayload getCurrentProject(@PathVariable BigInteger id) {
        return new ProjectInfoResponsePayload(this.projectService.getProject(id));
    }

    @PutMapping("/new")
    public ResponseEntity<?> createNewProject(
            @Validated @RequestBody CreateNewProjectRequestPayload createNewProjectRequestPayload, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }
        return ResponseEntity.ok(this.projectService.createNewProject(createNewProjectRequestPayload));
    }

    @PostMapping(value = "/update", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> updateProject(
            @Validated UpdateProjectRequestPayload updateProjectRequestPayload, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }
        return ResponseEntity.ok(this.projectService.updateProject(updateProjectRequestPayload));
    }

    @DeleteMapping(value = "/delete/{projectId}")
    public boolean deleteProject(@PathVariable BigInteger projectId) {
        return this.projectService.deleteProject(projectId);
    }

    @GetMapping(value = "/participants/{projectId}")
    public List<GetParticipantsRequestPayload> getParticipants(
            @PathVariable BigInteger projectId) {
        return this.participantService.getParticipants(projectId);
    }

    @GetMapping(value = "/{projectId}/getLogo", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public void getProjectLogo(HttpServletResponse http, @PathVariable BigInteger projectId) {
        this.projectService.getProjectLogo(http, projectId);
    }

    @GetMapping("/get/{projectId}/participantAccounts")
    public List<Participant> getParticipantUserAccountIds(@PathVariable BigInteger projectId) {
        List<Participant> result = new ArrayList<>();
        for (BigInteger bigInteger: getCurrentProject(projectId).getParticipants()) {
            result.add(participantService.get(bigInteger));
        }

        return result;
    }
}
