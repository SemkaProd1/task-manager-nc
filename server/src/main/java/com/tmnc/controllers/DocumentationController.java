package com.tmnc.controllers;

import com.tmnc.data.entities.Documentation;
import com.tmnc.data.entities.ProjectDocumentation;
import com.tmnc.data.exchange.documentation.CreateNewDocumentationRequestPayload;
import com.tmnc.data.exchange.documentation.DocumentationResponsePayload;
import com.tmnc.data.exchange.documentation.UpdateDocumentationRequestPayload;
import com.tmnc.services.DocumentationService;
import com.tmnc.utils.ResponseEntityFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping(value = "/api/documentation")
public class DocumentationController {

    private final DocumentationService documentService;

    @Autowired
    public DocumentationController(DocumentationService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("/project/{id}")
    public List<ProjectDocumentation> getAllDocumentsInProject(@PathVariable BigInteger id) {
        return documentService.getDocumentation(id);
    }

    @GetMapping("/project/document/{id}")
    public ProjectDocumentation getProjectDocument(@PathVariable BigInteger id) {
        return documentService.getProjectDocument(id);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getDocument(@PathVariable BigInteger id) {
        return ResponseEntity.ok(this.documentService.get(id));
    }

    @PostMapping(value = "/update", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> updateDocument(
            @RequestBody UpdateDocumentationRequestPayload payload, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }

        return ResponseEntity.ok(
                this.documentService.update(
                        payload.getObjectId(),
                        payload.getFilePath(),
                        payload.getParentId(),
                        payload.getName(),
                        payload.getCreatorParticipantId(),
                        "documentation"
                )
        );
    }

    @PostMapping(value = "/delete")
    public ResponseEntity<?> deleteDocument(@RequestBody DocumentationResponsePayload payload, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }
        return ResponseEntity.ok(this.documentService.delete(payload.getObjectId()));
    }

    @RequestMapping(value = "/download/{id}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public void downloadFile(HttpServletResponse response, @PathVariable BigInteger id) {
       this.documentService.downloadFile(response, id);
    }

    @PostMapping(value = "/add", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> addDocument(CreateNewDocumentationRequestPayload payload, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntityFactory.errorAfterValidation(errors);
        }
        return ResponseEntity.ok(
                this.documentService.create(
                        payload.getFilePath(),
                        payload.getParentId(),
                        payload.getCreatorParticipantId(),
                        payload.getCreationTime(),
                        "documentation"
                )
        );
    }

}
