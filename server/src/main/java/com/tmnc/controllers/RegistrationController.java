package com.tmnc.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmnc.data.exchange.users.CreateNewUserRequestPayload;
import com.tmnc.data.exchange.users.UserResponsePayload;
import com.tmnc.services.MailService;
import com.tmnc.services.UserService;
import com.tmnc.utils.ErrorsConverter;
import com.tmnc.validation.UserAccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@RestController
@RequestMapping(value = "/register",
        produces = {MediaType.APPLICATION_JSON_VALUE})
public class    RegistrationController {

    private final UserService userService;
    private final UserAccountValidator validator;
    private final MailService mailService;
    private final ObjectMapper objectMapper;

    @Autowired
    public RegistrationController(UserService userService,
                                  UserAccountValidator validator,
                                  MailService service,
                                  ObjectMapper objectMapper) {
        this.userService = userService;
        this.validator = validator;
        this.mailService = service;
        this.objectMapper = objectMapper;
    }

    @InitBinder("createNewUserRequestPayload")
    public void initCreateNewUserBinder(WebDataBinder binder) {
        binder.setValidator(this.validator);
    }

    @PostMapping(value = "/pass")
    @ResponseBody
    public void register(@Validated
                         @RequestBody CreateNewUserRequestPayload createNewUserRequestPayload,
                         Errors errors,
                         HttpServletResponse response,
                         @CookieValue(name = "Invite", required = false) String url) throws IOException {
        if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.getOutputStream().println(objectMapper.writeValueAsString(ErrorsConverter.convert(errors)));
            return;
        }

        UserResponsePayload newUser = this.userService.createNewUser(createNewUserRequestPayload);
        mailService.sendRegistrationMessage(createNewUserRequestPayload);

        if (Objects.nonNull(url)) {
            response.sendRedirect(url);
        } else {
            response.setStatus(HttpStatus.OK.value());
            response.getOutputStream().println(objectMapper.writeValueAsString(newUser));
        }
    }

    @GetMapping("/isMailExists")
    public int isEmailExists(@RequestParam("email") String email) {
        if (userService.isEmailExists(email)) {
            return 1;
        } else {
            return 0;
        }
    }

    @GetMapping("/isUsernameExists")
    public int isUsernameExists(@RequestParam("username") String username) {
        if (userService.isUserExist(username)) {
            return 1;
        } else {
            return 0;
        }
    }
}