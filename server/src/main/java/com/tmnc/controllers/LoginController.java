package com.tmnc.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmnc.data.entities.UserCredentials;
import com.tmnc.data.exchange.users.LoginUserRequestPayload;
import com.tmnc.data.exchange.users.LoginUserResponsePayload;
import com.tmnc.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@RestController
@RequestMapping
public class LoginController {

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public void login(@RequestBody LoginUserRequestPayload user,
                      @CookieValue(name = "Invite", required = false) String url,
                      HttpServletRequest request,
                      HttpServletResponse response,
                      ObjectMapper objectMapper) throws IOException {
        LoginUserResponsePayload payload = this.loginService.authenticate(user, request);

        if (Objects.nonNull(url)) {
            response.sendRedirect(url);
        } else {
            response.setStatus(HttpServletResponse.SC_OK);
        }

        response.getOutputStream().println(objectMapper.writeValueAsString(payload));
    }

    @GetMapping("/login/status")
    public boolean isAuthenticated(HttpServletRequest request) {
        return this.loginService.isAuthenticated(request);
    }

    @GetMapping("/login/user")
    public LoginUserResponsePayload getAuthenticatedUser() {
        UserCredentials principal = (UserCredentials) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        return this.loginService.getAuthenticatedUser(principal);
    }

    @GetMapping("/login/logout")
	public boolean logout(HttpServletRequest request) {
        return this.loginService.logout(request);
    }
}