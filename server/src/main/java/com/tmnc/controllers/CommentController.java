package com.tmnc.controllers;

import com.tmnc.data.entities.Comment;
import com.tmnc.data.exchange.comment.CommentResponsePayload;
import com.tmnc.data.exchange.comment.CommentView;
import com.tmnc.data.exchange.comment.CreateNewCommentRequestPayload;
import com.tmnc.data.exchange.task.TaskResponsePayload;
import com.tmnc.services.CommentService;
import com.tmnc.utils.ErrorsConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/commentById")
    public Comment getCurrentSprint(@RequestBody CommentResponsePayload payload) {
        return this.commentService.get(payload.getObjectId());
    }

    @PutMapping("/new")
    public ResponseEntity<?> createNewSprint(
            @RequestBody CreateNewCommentRequestPayload payload, Errors errors) {
        if (errors.hasErrors()) {
            return new ResponseEntity<>(ErrorsConverter.convert(errors), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(this.commentService.create(payload));
    }

    @GetMapping("/comments")
    public List<CommentView> getTasksInSprint(@RequestBody TaskResponsePayload payload) {
        return this.commentService.getComments(payload.getObjectId());
    }
}
