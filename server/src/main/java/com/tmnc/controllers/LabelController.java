package com.tmnc.controllers;

import com.tmnc.data.entities.Label;
import com.tmnc.data.exchange.label.CreateNewLabelRequestPayload;
import com.tmnc.services.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class LabelController {

    private final LabelService labelService;

    @Autowired
    public LabelController(LabelService labelService) {
        this.labelService = labelService;
    }

    @GetMapping("/labels/project/{projectId}")
    public List<Label> getAllLabelsInProject(@PathVariable BigInteger projectId) {
        return this.labelService.getLabelsInProject(projectId);
    }

    @PostMapping("/labels/new")
    public ResponseEntity<?> createNewLabel(
            @RequestBody CreateNewLabelRequestPayload payload){
        return ResponseEntity.ok(this.labelService.create(payload));

    }

    @GetMapping("/labels/task/{taskId}")
    public List<Label> getAllLabelsInTask(@PathVariable BigInteger taskId) {
        return this.labelService.getLabelsInTask(taskId);
    }

    @GetMapping("/labels/{IDs}")
    public List<Label> getAllLabels(@PathVariable List<BigInteger> IDs) {
        return this.labelService.getLabels(IDs);
    }

    @DeleteMapping("/labels/delete/{id}")
    public BigInteger delete(@PathVariable BigInteger id) {
        return this.labelService.delete(id);
    }
}
