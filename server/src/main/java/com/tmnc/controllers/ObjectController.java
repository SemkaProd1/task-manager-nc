package com.tmnc.controllers;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.data.converters.NCLToUserConverter;
import com.tmnc.services.UserAuthenticationService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ObjectController {

    private Logger logger = LogManager.getLogger(ObjectController.class);
    private NCLObjectRepository repository;
    private NCLToUserConverter converter;
    private JdbcTemplate template;
    private final PasswordEncoder passwordEncoder;
    private final UserAuthenticationService userService;

    @Autowired
    public ObjectController(NCLObjectRepository repository,
                            NCLToUserConverter converter,
                            JdbcTemplate template,
                            PasswordEncoder passwordEncoder,
                            UserAuthenticationService userService) {
        this.repository = repository;
        this.converter = converter;
        this.template = template;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @GetMapping("/obj")
    public List dummy() {
        return repository.selectObjects(Arrays.asList(new BigInteger("3"), new BigInteger("6"), new BigInteger("9")),
                Arrays.asList(new BigInteger("8"), new BigInteger("9")));
    }
}
