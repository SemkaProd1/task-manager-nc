package com.tmnc.controllers;

import com.tmnc.core.exceptions.NotValidInviteException;
import com.tmnc.data.entities.Invite;
import com.tmnc.data.entities.UserCredentials;
import com.tmnc.data.exchange.invite.CreateNewInviteRequestPayload;
import com.tmnc.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Objects;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "/api/invite")
public class InviteController {

    private InviteService inviteService;
    private MailService mailService;
    private UserService userService;
    private ParticipantService participantService;
    private final LoginService service;

    @Autowired
    public InviteController(InviteService inviteService,
                            MailService mailService,
                            UserService userService,
                            ParticipantService participantService,
                            LoginService service) {
        this.inviteService = inviteService;
        this.mailService = mailService;
        this.userService = userService;
        this.participantService = participantService;
        this.service = service;
    }

    @PostMapping(value = "/send")
    public String send(@RequestBody CreateNewInviteRequestPayload payload, HttpServletResponse response) throws IOException {
        if (isAnyNull(payload, payload.getEmail(), payload.getProjectId(), payload.getRole())) {
            response.sendError(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        String url = inviteService.generateLink(payload);
        mailService.sendInviteMessage(payload);
        inviteService.create(payload);
        return url;
    }

    @GetMapping(value = "/accept/**")
    public void accept(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        Invite invite = inviteService.get(request.getRequestURI());
        if (Objects.isNull(invite)) {
            throw new NotValidInviteException(request.getRequestURI());
        }
        response.addCookie(inviteService.addCookie(request.getRequestURI()));

        if (!userService.isEmailExists(invite.getEmail())) {
            response.sendRedirect("/login/register");
            response.setStatus(302);
            return;
        }

        if (!this.service.isAuthenticated(request)) {
            response.setStatus(200);
            return;
        }

        UserCredentials user = (UserCredentials) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        participantService.create(user, invite);
        inviteService.delete(invite.getObjectId());

        response.setStatus(200);
        response.getWriter().println("{\"message\": \"You are added to project successfully\"}");
    }

    private boolean isAnyNull(Object... objects) {
        return Stream.of(objects).anyMatch(Objects::isNull);
    }
}