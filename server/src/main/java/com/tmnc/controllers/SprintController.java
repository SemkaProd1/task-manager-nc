package com.tmnc.controllers;

import com.tmnc.data.entities.Sprint;
import com.tmnc.data.exchange.sprint.CreateNewSprintRequestPayload;
import com.tmnc.data.exchange.sprint.SprintTasksResponsePayload;
import com.tmnc.data.exchange.sprint.SprintWithStatistics;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.services.SprintService;
import com.tmnc.utils.ErrorsConverter;
import com.tmnc.validation.SprintValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/sprint")
public class SprintController {

    private final SprintService sprintService;
    private final SprintValidator validator;

    @Autowired
    public SprintController(SprintService sprintService, SprintValidator validator) {
        this.sprintService = sprintService;
        this.validator = validator;
    }

    @InitBinder("createNewSprintRequestPayload")
    public void createSprintInitBinder(WebDataBinder binder) {
        binder.setValidator(this.validator);
    }

    @GetMapping("/{id}")
    public Sprint getCurrentSprint(@PathVariable BigInteger id) {

        return this.sprintService.getSprint(id);
    }

    @PostMapping("/new")
    public ResponseEntity<?> createNewSprint(
            @Validated(SprintValidator.class) @RequestBody CreateNewSprintRequestPayload payload, Errors errors) {
        if (errors.hasErrors()) {
            return new ResponseEntity<>(ErrorsConverter.convert(errors), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(this.sprintService.createNewSprint(payload));
    }

    @GetMapping("/{sprintId}/tasks")
    public List<SprintTasksResponsePayload> getAllTasksInSprint(@PathVariable BigInteger sprintId) {
        return this.sprintService.getAllTasksInSprint(sprintId);
    }

    @GetMapping("/tasksByStatus")
    public List<SprintTasksResponsePayload> getTasksInSprintByStatus(@RequestParam BigInteger sprintId, @RequestParam String taskStatus) {
        return this.sprintService.getTaskByStatus(this.sprintService.getAllTasksInSprint(sprintId), TaskStatus.valueOf(taskStatus.toUpperCase()));
    }

    @GetMapping("/taskCountsByStatus")
    public Map<Object, BigInteger> getTaskCountsByStatus(@RequestParam BigInteger sprintId) {
        return sprintService.getTaskCountsByStatus(sprintId);
    }

    @GetMapping("/{projectId}/allInProject")
    public List<Sprint> getSprints(@PathVariable BigInteger projectId) {
        return this.sprintService.getSprints(projectId);
    }

    @GetMapping("/{projectId}/allInProject/withStatistics")
    public List<SprintWithStatistics> getSprintsWithStatistics(@PathVariable BigInteger projectId) {
        return this.sprintService.getSprintsWithStatistics(projectId);
    }
}