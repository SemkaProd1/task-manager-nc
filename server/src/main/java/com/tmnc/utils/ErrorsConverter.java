package com.tmnc.utils;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ErrorsConverter {

	public static Map<String, FieldErrorInfo> convert(Errors errors) {
		Map<String, FieldErrorInfo> errorMap = new HashMap<>();

		for (FieldError fieldError: errors.getFieldErrors()) {
			FieldErrorInfo fieldErrorInfo = new FieldErrorInfo();
			fieldErrorInfo.setErrorCodes(fieldError.getCodes() == null
					? Collections.emptyList()
					: Arrays.asList(fieldError.getCodes()));
			fieldErrorInfo.setFieldName(fieldError.getField());
			fieldErrorInfo.setRejectedValue(fieldError.getRejectedValue());
			fieldErrorInfo.setDefaultMessage(fieldError.getDefaultMessage());
			errorMap.putIfAbsent(fieldError.getField(), fieldErrorInfo);
		}

		return errorMap;
	}
}
