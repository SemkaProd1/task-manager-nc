package com.tmnc.utils.date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.sql.Date;
import java.time.format.DateTimeFormatter;

public class SqlDateSerializer extends JsonSerializer<Date> {

	private final DateTimeFormatter dateTimeFormatter;

	public SqlDateSerializer(DateTimeFormatter dateTimeFormatter) {
		this.dateTimeFormatter = dateTimeFormatter;
	}

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeString(SqlDateTransformUtils.toString(value, dateTimeFormatter));
	}

	@Override
	public Class<Date> handledType() {
		return Date.class;
	}
}
