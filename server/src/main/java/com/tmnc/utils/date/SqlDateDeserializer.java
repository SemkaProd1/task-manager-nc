package com.tmnc.utils.date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;

import java.io.IOException;
import java.sql.Date;
import java.time.format.DateTimeFormatter;


public class SqlDateDeserializer extends JsonDeserializer<Date> {

	private final DateTimeFormatter dateTimeFormatter;

	public SqlDateDeserializer(DateTimeFormatter dateTimeFormatter) {
		this.dateTimeFormatter = dateTimeFormatter;
	}

	@Override
	public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		return SqlDateTransformUtils.toDate(p.getValueAsString(), dateTimeFormatter);
	}

	@Override
	public Class<?> handledType() {
		return Date.class;
	}
}
