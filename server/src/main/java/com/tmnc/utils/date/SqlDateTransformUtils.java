package com.tmnc.utils.date;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SqlDateTransformUtils {
	public static Date toDate(String raw, DateTimeFormatter dateTimeFormatter) {
		LocalDateTime parse = LocalDateTime.parse(raw, dateTimeFormatter);
		LocalDate localDate = parse.toLocalDate();
		return Date.valueOf(localDate);
	}

	public static String toString(Date raw, DateTimeFormatter dateTimeFormatter) {
		LocalDate localDate = raw.toLocalDate();
		return localDate.atStartOfDay().format(dateTimeFormatter);
	}
}
