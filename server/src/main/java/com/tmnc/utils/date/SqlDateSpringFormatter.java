package com.tmnc.utils.date;

import org.springframework.format.Formatter;

import java.sql.Date;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class SqlDateSpringFormatter implements Formatter<Date> {
	private final DateTimeFormatter dateTimeFormatter;

	public SqlDateSpringFormatter(DateTimeFormatter dateTimeFormatter) {
		this.dateTimeFormatter = dateTimeFormatter;
	}

	@Override
	public Date parse(String text, Locale locale) throws ParseException {
		return SqlDateTransformUtils.toDate(text, dateTimeFormatter);
	}

	@Override
	public String print(Date object, Locale locale) {
		return SqlDateTransformUtils.toString(object, dateTimeFormatter);
	}
}
