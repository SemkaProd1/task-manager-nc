package com.tmnc.utils;

import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class ApplicationSessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) { }

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		SecurityContextHolder.clearContext();
	}
}
