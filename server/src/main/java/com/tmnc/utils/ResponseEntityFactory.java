package com.tmnc.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;

public class ResponseEntityFactory {
    public static ResponseEntity<?> errorAfterValidation(Errors errors) {
        return new ResponseEntity<>(ErrorsConverter.convert(errors), HttpStatus.BAD_REQUEST);
    }
}
