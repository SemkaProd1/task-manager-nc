package com.tmnc.utils;

import java.nio.file.Path;

public class PathSanitizeUtils {
    public static boolean isSanitizedPath(Path expectedPathStart, Path actualPath) {
        return !actualPath.startsWith(expectedPathStart);
    }
}
