package com.tmnc.services;

import com.tmnc.configs.MailConfiguration;
import com.tmnc.data.exchange.invite.CreateNewInviteRequestPayload;
import com.tmnc.data.exchange.users.CreateNewUserRequestPayload;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;

@Service
public class MailService {

    private final Logger logger = LogManager.getLogger(MailService.class);
    private static final String DOMAIN = "http://178.128.162.132:8090";

    private final JavaMailSender mailSender;
    private final MailConfiguration configuration;

    @Autowired
    public MailService(JavaMailSender mailSender, MailConfiguration configuration) {
        this.mailSender = mailSender;
        this.configuration = configuration;
    }

    public void sendInviteMessage(CreateNewInviteRequestPayload payload) {
        String text = String.format("Hi! You have just got an invite to the project with id:%s as %s, please accept it here: %s",
                payload.getProjectId(), payload.getRole(), DOMAIN + payload.getUrl());
        sendMail(getMimeMessagePreparator(text, payload.getEmail(),
                "Your registration on TM NC"));
    }

    public void sendRegistrationMessage(final CreateNewUserRequestPayload account) {
        String text = String.format("Welcome to our app!\nAccount Details: \nYour username is: %s",
                account.getUsername());
        sendMail(getMimeMessagePreparator(text,
                account.getEmail(), "TM NC Greetings"));
    }

    public void sendAssigneeMessage(String email) {
        String text = "Hi! You have new task in your project!";
        sendMail(getMimeMessagePreparator(text, email, "new Task in TM NC!"));
    }
    public void sendDocumentationMessage(String email, String documentName) {
        String text = String.format("Hi! You have new Document with name: '%s' in your project! " +
                "It makes sense to read the documentation. ", documentName);
        sendMail(getMimeMessagePreparator(text, email, "Added new documentation in TM NC!"));
    }

    private void sendMail(MimeMessagePreparator preparator) {
        try {
            mailSender.send(preparator);
        } catch (MailException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private MimeMessagePreparator getMimeMessagePreparator(String emailMsg, String email, String emailSubject) {
        return mimeMessage -> {
            mimeMessage.setFrom(configuration.getLogin());
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
            mimeMessage.setText(emailMsg);
            mimeMessage.setSubject(emailSubject);
        };
    }

}