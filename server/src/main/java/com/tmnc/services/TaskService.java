package com.tmnc.services;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.data.entities.Task;
import com.tmnc.data.exchange.documentation.CreateNewDocumentationRequestPayload;
import com.tmnc.data.exchange.task.CreateNewTaskRequestPayload;
import com.tmnc.data.exchange.task.TaskToAssigneeResponsePayload;
import com.tmnc.data.exchange.task.UpdateTaskRequestPayload;
import com.tmnc.data.repositories.TaskRepository;
import com.tmnc.data.utils.TaskStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final DocumentationService documentationService;
    private final UserService userService;
    private final MailService mailService;
    private final NCLObjectRepository objectRepository;
    @Autowired
    public TaskService(TaskRepository repository, DocumentationService documentationService,
                       UserService userService, MailService mailService, NCLObjectRepository objectRepository) {
        this.taskRepository = repository;
        this.documentationService = documentationService;
        this.userService = userService;
        this.mailService = mailService;
        this.objectRepository = objectRepository;
    }

    public BigInteger create(CreateNewTaskRequestPayload payload) {
        return taskRepository.create(payload);
    }

    public Task get(BigInteger objectId) {
        return taskRepository.get(objectId);
    }

    public BigInteger update(UpdateTaskRequestPayload payload) {

        return taskRepository.update(payload);
    }

    public boolean delete(BigInteger objectId) {
        documentationService.deleteDocumentsInTask(objectId, taskRepository.getProject(objectId));
        return taskRepository.delete(objectId).equals(BigInteger.ONE);
    }

    public boolean setTaskStatus(BigInteger objectId, TaskStatus status) {
        return taskRepository.setTaskStatus(objectId, status);
    }

    public boolean addLabelToTask(BigInteger taskId, BigInteger labelId) {
        return this.taskRepository.addLabelToTask(taskId, labelId);
    }

    public boolean addDocumentToTask(CreateNewDocumentationRequestPayload payload, BigInteger taskId) {
        return this.taskRepository.addDocumentToTask(taskId,
                this.documentationService.create(
                        payload.getFilePath(),
                        taskRepository.getProject(payload.getParentId()),
                        payload.getCreatorParticipantId(),
                        payload.getCreationTime(),
                        "task" + payload.getParentId().toString()));
    }

    public boolean addAssigneeToTask(BigInteger taskId, BigInteger participantId) {
        this.mailService.sendAssigneeMessage(this.userService.getUser(objectRepository.getFirstParent(participantId)).getEmail());
        return this.taskRepository.addAssigneeToTask(taskId, participantId);
    }

    public boolean unbindLabelFromTask(BigInteger taskId, BigInteger labelId) {
        return this.taskRepository.unbindLabelFromTask(taskId, labelId);
    }

    public boolean unbindAssigneeFromTask(BigInteger taskId, BigInteger participantId) {
        return this.taskRepository.unbindAssigneeFromTask(taskId, participantId);
    }

    public boolean unbindDocumentFromTask(BigInteger documentId) {
        this.documentationService.delete(documentId);
        this.taskRepository.unbindDocumentFromTask(documentId);
        return true;
    }

    public List<BigInteger> getLabelsInTaskIDs(BigInteger taskId) {
        return taskRepository.get(taskId).getLabels();
    }

    public List<TaskToAssigneeResponsePayload> getTasksByAssignee(BigInteger assignee) {
        return taskRepository.getTasksByAssignee(assignee);
    }

}