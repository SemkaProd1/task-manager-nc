package com.tmnc.services;

import com.tmnc.data.converters.NCLToSprintWithStatisticsConverter;
import com.tmnc.data.entities.Sprint;
import com.tmnc.data.exchange.sprint.CreateNewSprintRequestPayload;
import com.tmnc.data.exchange.sprint.SprintResponsePayload;
import com.tmnc.data.exchange.sprint.SprintTasksResponsePayload;
import com.tmnc.data.exchange.sprint.SprintWithStatistics;
import com.tmnc.data.repositories.SprintRepository;
import com.tmnc.data.repositories.TaskRepository;
import com.tmnc.data.utils.TaskStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;

@Service
public class SprintService {

    private final SprintRepository sprintRepository;
    private final TaskRepository taskRepository;
    private final NCLToSprintWithStatisticsConverter converter;

    @Autowired
    public SprintService(SprintRepository sprintRepository,
                         TaskRepository taskRepository,
                         NCLToSprintWithStatisticsConverter converter) {
        this.sprintRepository = sprintRepository;
        this.taskRepository = taskRepository;
        this.converter = converter;
    }

    public SprintResponsePayload createNewSprint(CreateNewSprintRequestPayload payload) {
        BigInteger newSprintId = this.sprintRepository.createNewSprint(payload);

        SprintResponsePayload responsePayload = new SprintResponsePayload();
        responsePayload.setObjectId(newSprintId);
        return responsePayload;
    }

    public Sprint getSprint(BigInteger id) {
        return sprintRepository.getSprintById(id);
    }

    public List<SprintTasksResponsePayload> getAllTasksInSprint(BigInteger sprintId) {
        return taskRepository.getTasksInSprint(sprintRepository.getTasksIDs(sprintId));
    }

    public List<SprintTasksResponsePayload> getTaskByStatus(List<SprintTasksResponsePayload> tasks, TaskStatus taskStatus) {
        List<SprintTasksResponsePayload> statusSortedTasks = new ArrayList<>();

        for (SprintTasksResponsePayload task : tasks) {
            if (task.getStatus().equals(taskStatus)) {
                statusSortedTasks.add(task);
            }
        }
        return statusSortedTasks;
    }

    public List<Sprint> getSprints(BigInteger projectId) {
        return this.sprintRepository.getSprintsInProject(sprintRepository.getSprintsIDs(projectId));
    }

    public List<SprintWithStatistics> getSprintsWithStatistics(BigInteger projectId) {
        return this.sprintRepository.getSprintsWithStatisticsInProject(
                sprintRepository.getSprintsIDs(projectId), converter);
    }


    public Map<Object, BigInteger> getTaskCountsByStatus(BigInteger sprintId) {
        return sprintRepository.getTaskCountsByStatus(sprintId);
    }
}
