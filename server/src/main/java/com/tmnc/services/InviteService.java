package com.tmnc.services;

import com.tmnc.data.entities.Invite;
import com.tmnc.data.exchange.invite.CreateNewInviteRequestPayload;
import com.tmnc.data.repositories.InviteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.math.BigInteger;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class InviteService {

    private final InviteRepository repository;

    @Autowired
    public InviteService(InviteRepository repository) {
        this.repository = repository;
    }

    public String generateLink(CreateNewInviteRequestPayload payload) {
        String url = generateLink();
        payload.setUrl(url);
        return url;
    }

    private String generateLink() {
        return String.format("/api/invite/accept/%s", UUID.randomUUID());
    }

    public BigInteger create(CreateNewInviteRequestPayload payload) {
        return repository.create(payload);
    }

    public Invite get(String url) {
        return repository.getByURL(url);
    }

    public void delete(BigInteger objectId) {
        repository.delete(objectId);
    }

    public Cookie addCookie(String url) {
        Cookie cookie = new Cookie("Invite", url);
        cookie.setMaxAge((int) TimeUnit.MINUTES.toSeconds(3));
        cookie.setPath("/");
        return cookie;
    }
}