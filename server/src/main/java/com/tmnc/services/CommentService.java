package com.tmnc.services;

import com.tmnc.data.entities.Comment;
import com.tmnc.data.exchange.comment.CommentView;
import com.tmnc.data.exchange.comment.CreateNewCommentRequestPayload;
import com.tmnc.data.exchange.comment.CommentResponsePayload;
import com.tmnc.data.exchange.comment.UpdateCommentRequestPayload;
import com.tmnc.data.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

@Service
public class CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public CommentResponsePayload create(CreateNewCommentRequestPayload requestPayload) {
        BigInteger newCommentId = this.commentRepository.create(requestPayload);

        CommentResponsePayload responsePayload = new CommentResponsePayload();
        responsePayload.setObjectId(newCommentId);
        return responsePayload;
    }

    public List<CommentView> getComments(BigInteger taskId) {
        return this.commentRepository.getCommentsView(this.commentRepository.getCommnents(taskId));
    }


    public Comment get(BigInteger objectId) {
        return commentRepository.get(objectId);
    }

    public BigInteger update(UpdateCommentRequestPayload payload) {
        return commentRepository.update(payload);
    }

    public BigInteger delete(BigInteger objectId) {
        return commentRepository.delete(objectId);
    }
}