package com.tmnc.services;

import com.tmnc.data.entities.Invite;
import com.tmnc.data.entities.Participant;
import com.tmnc.data.entities.UserCredentials;
import com.tmnc.data.exchange.participant.CreateNewParticipantRequestPayload;
import com.tmnc.data.exchange.participant.GetParticipantsRequestPayload;
import com.tmnc.data.exchange.participant.GetProjectsResponsePayload;
import com.tmnc.data.exchange.project.ProjectRequestPayload;
import com.tmnc.data.exchange.participant.UpdateParticipantRequestPayload;
import com.tmnc.data.repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class ParticipantService {

    private final ParticipantRepository participantRepository;

    @Autowired
    public ParticipantService(ParticipantRepository repository) {
        this.participantRepository = repository;
    }

    public BigInteger create(UserCredentials user, Invite invite) {
        CreateNewParticipantRequestPayload requestPayload = new CreateNewParticipantRequestPayload();
        requestPayload.setProjectId(invite.getProjectId());
        requestPayload.setUserId(user.getObjectId());
        requestPayload.setRole(invite.getRole());
        return participantRepository.create(requestPayload);
    }

    public Participant get(BigInteger objectId) {
        return participantRepository.get(objectId);
    }

    public BigInteger update(UpdateParticipantRequestPayload payload) {
        return participantRepository.update(payload);
    }

    public void remove(BigInteger objectId) {
        participantRepository.delete(objectId);
    }

    public List<GetParticipantsRequestPayload> getParticipants(BigInteger projectId) {
        return participantRepository.getParticipants(projectId);
    }

    public int countOfprojects(BigInteger userId) {
        return 0;
    }

    public String getEmailByParticipantId(BigInteger participantId){
        return participantRepository.getEmailByParticipantId(participantId);
    }

    public boolean isParticipantInProject(BigInteger projectId, BigInteger userId) {
        String hello = "select count(*) from objref";
        return true;
    }
}