package com.tmnc.services;

import com.tmnc.data.entities.Label;
import com.tmnc.data.exchange.label.CreateNewLabelRequestPayload;
import com.tmnc.data.exchange.label.LabelResponsePayload;
import com.tmnc.data.exchange.label.UpdateLabelRequestPayload;
import com.tmnc.data.repositories.LabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class LabelService {

    private final LabelRepository labelRepository;
    private final ProjectService projectService;
    private final TaskService taskService;

    @Autowired
    public LabelService(LabelRepository labelRepository, ProjectService projectService, TaskService taskService) {
        this.labelRepository = labelRepository;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public Label get(LabelResponsePayload payload) {
        return labelRepository.get(payload.getObjectId());
    }

    public List<Label> getLabelsInProject(BigInteger projectId) {
        return labelRepository.getLabels(projectService.getLabelsInProjectIDs(projectId));
    }
    public List<Label> getLabelsInTask(BigInteger taskId) {
        return labelRepository.getLabels(taskService.getLabelsInTaskIDs(taskId));
    }

    public List<Label> getLabels(List<BigInteger> IDs) {
        return labelRepository.getLabels(IDs);
    }

    public BigInteger create(CreateNewLabelRequestPayload payload){ return  labelRepository.create(payload); }

    public BigInteger update(UpdateLabelRequestPayload payload) {
        return labelRepository.update(payload);
    }

    public BigInteger delete(BigInteger objectId) {
        return labelRepository.delete(objectId);
    }
}