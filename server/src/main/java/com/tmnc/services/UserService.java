package com.tmnc.services;


import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.participant.ParticipantResponsePayload;
import com.tmnc.data.exchange.project.ProjectInfoForUserResponsePayload;
import com.tmnc.data.exchange.users.*;
import com.tmnc.data.repositories.UserRepository;
import com.tmnc.data.utils.StoreEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


@Service
public class UserService {

    private final FileUploadStorageService uploadService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(FileUploadStorageService service, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.uploadService = service;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public UserAccount getUser(BigInteger id) {
        return userRepository.getUserAccountById(id);
    }

    public UserResponsePayload createNewUser(CreateNewUserRequestPayload payload) {
        BigInteger newUserId = this.userRepository.createNewUser(payload);
        UserResponsePayload responsePayload = new UserResponsePayload();
        responsePayload.setObjectId(newUserId);
        return responsePayload;
    }

    public BigInteger updateUserRequestPayload(UpdateUserRequestPayload payload) {
        MultipartFile avatar = payload.getAvatar();
        Path absoluteLocation = null;
        if (avatar != null && !avatar.isEmpty()) {
            StoreEntry avatarEntry = uploadService.createStoreEntry(
                    avatar, uploadService.getConfig().getUsersPath(), payload.getObjectId(), "avatar"
            );
            absoluteLocation = avatarEntry.getAbsoluteLocation();
            if (!uploadService.isFileExists(absoluteLocation)) {
                uploadService.clearDirectory(absoluteLocation.getParent());
                uploadService.storeFiles(avatarEntry);
            }
        }
        return this.userRepository.updateUserAccount(absoluteLocation, payload);
    }

    public boolean isUserExist(String username) {
        return this.userRepository.isUserExist(username).doubleValue() > 0;
    }

    public boolean isEmailExists(String email) {
        return this.userRepository.isEmailExists(email).doubleValue() > 0;
    }

    public boolean delete(BigInteger objectId) {
        return userRepository.deleteUser(objectId).equals(BigInteger.ONE);
    }

    public BigInteger updateEmail(UpdateUserEmailRequestPayload payload) {
        return userRepository.updateEmail(payload);
    }

    public BigInteger updatePassword(UpdateUserPasswordRequestPayload payload) throws IOException {
        String encodePassword = passwordEncoder.encode(payload.getPassword());
        return userRepository.updatePassword(payload.getObjectId(), encodePassword);
    }

    public boolean passwordNotMatch(String password, String encodedPassword) {
        return !isPasswordMatch(password, encodedPassword);
    }

    public boolean isPasswordMatch(String password, String encodedPassword) {
        return !password.isEmpty() && !encodedPassword.isEmpty() && passwordEncoder.matches(password, encodedPassword);
    }

    public List<ProjectInfoForUserResponsePayload> getProjectInfo(BigInteger userId) {
        List<ProjectInfoForUserResponsePayload> projects = new ArrayList<>();
        List<BigInteger> participants = getUser(userId).getParticipants();

        for (BigInteger participant : participants) {
            projects.add(this.userRepository.getProjectInfo(participant));
        }

        return projects;
    }

    public void getUserAvatar(HttpServletResponse response, BigInteger userId) {
        String filePath = getUser(userId).getAvatar();
        uploadService.sendToDownload(response, filePath);
    }

    public ParticipantResponsePayload getCurrentParticipant(BigInteger userId, BigInteger projectId) {
        return userRepository.getCurrentParticipant(userId, projectId);
    }
}
