package com.tmnc.services;

import com.tmnc.configs.FileUploadConfig;
import com.tmnc.core.exceptions.file.FileDownloadException;
import com.tmnc.core.exceptions.file.FileStorageDeleteException;
import com.tmnc.core.exceptions.file.FileStorageInitException;
import com.tmnc.core.exceptions.file.FileStorageStoreException;
import com.tmnc.data.utils.StoreEntry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.nio.file.*;
import java.util.Comparator;

@Service
public class FileUploadStorageService {

    private Logger logger = Logger.getLogger(FileUploadStorageService.class);
    private FileUploadConfig config;

    @Autowired
    public FileUploadStorageService(FileUploadConfig config) {
        this.config = config;

        this.doInitFileStorage(this.config);
    }

    FileUploadConfig getConfig() {
        return config;
    }

    /**
     * Method, that used for storing files.
     */
    public boolean storeFiles(StoreEntry... files) {
        try {
            for (StoreEntry storeEntry : files) {
                MultipartFile content = storeEntry.getContent();

                Path absoluteLocation = storeEntry.getAbsoluteLocation();
                Path parentLocation = absoluteLocation.getParent();

                if (Files.notExists(parentLocation)) {
                    Files.createDirectories(parentLocation);
                }

                InputStream inputStream = content.getInputStream();
                OutputStream outputStream = Files.newOutputStream(absoluteLocation);

                byte[] buffer = new byte[8192];

                while (inputStream.read(buffer) != -1) {
                    outputStream.write(buffer);
                }

                inputStream.close();
                outputStream.close();
            }

            return true;
        } catch (IOException e) {
            throw new FileStorageStoreException("Application could not store files! ", e);
        }
    }

    long sendToDownload(HttpServletResponse response, String file) {
        try {
            Path filePath = Paths.get(file);
            String name = filePath.getFileName().toString();

            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");

            return Files.copy(filePath, response.getOutputStream());
        } catch (IOException | InvalidPathException | NullPointerException e) {
            throw new FileDownloadException(e);
        }
    }

    /**
     * Method, that instantiate {@link com.tmnc.data.utils.StoreEntry StoreEntry object} for storing files.
     * Method forms file path by template: {startLocationPath}/{arg1}/{arg2}/.../{argN}/multipartFile.originalFilename
     * <pre class="code">
     *    createStoreEntry(multipartFile, projectStartLocation, projectId)
     * </pre>
     *
     * @param multipartFile - file, that received from the client-side.
     * @param startLocation - start of the path, where file will be stored.
     * @param args          - arguments, that used to form file path to this file in the storage.
     * @return {@link com.tmnc.data.utils.StoreEntry StoreEntry object}
     */
    public StoreEntry createStoreEntry(MultipartFile multipartFile, Path startLocation, Object... args) {
        String originalFileName = multipartFile.getOriginalFilename();
        Path absoluteLocation = startLocation;

        for (Object arg : args) {
            absoluteLocation = absoluteLocation.resolve(arg.toString());
        }

        absoluteLocation = absoluteLocation.resolve(originalFileName);

        return StoreEntry.builder()
                .absoluteLocation(absoluteLocation)
                .content(multipartFile)
                .build();
    }

    boolean deleteFiles(Path... paths) {
        try {
            for (Path path : paths) {
                Files.delete(path);
            }

            return true;
        } catch (IOException e) {
            throw new FileStorageDeleteException(e);
        }
    }

    void clearDirectory(Path path) {
        try {
            Files.walk(path)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void deleteDocumentsInTask(BigInteger taskId, BigInteger projectId){

        Path files = Paths.get(getConfig().getProjectsPath().toString() +"/"+projectId.toString()+
                "/task"+taskId.toString()+ "/");
        clearDirectory(files);
    }

    void deleteDocumentsInProject(BigInteger projectId){

        Path files = Paths.get(getConfig().getProjectsPath().toString() +"/"+projectId.toString());
        clearDirectory(files);
    }

    private void doInitFileStorage(FileUploadConfig config) {
        try {
            if (Files.notExists(config.getStorageAbsolutePath())) {
                Files.createDirectories(config.getStorageAbsolutePath());
                Files.createDirectory(config.getProjectsPath());
                Files.createDirectory(config.getUsersPath());
            }
        } catch (IOException e) {
            throw new FileStorageInitException("Application could not initialize storage", e);
        }
    }

    boolean isFileExists(Path absoluteLocation) {
        return Files.exists(absoluteLocation);
    }

    Path replaceDocumentOnProjects(MultipartFile file,
                                   BigInteger parentId,
                                   String directoryName) {
        Path absoluteLocation = null;

        if (file != null && !file.isEmpty()) {
            StoreEntry fileEntry = createStoreEntry(
                    file, getConfig().getProjectsPath(), parentId, directoryName);

            absoluteLocation = fileEntry.getAbsoluteLocation();

            if (isFileExists(absoluteLocation)) {
                deleteFiles(absoluteLocation);
            }

            storeFiles(fileEntry);
        }

        return absoluteLocation;
    }
}

