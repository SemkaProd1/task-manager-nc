package com.tmnc.services;

import com.tmnc.configs.SessionConfig;
import com.tmnc.data.entities.SimpleUserPrincipal;
import com.tmnc.data.entities.UserCredentials;
import com.tmnc.data.exchange.users.LoginUserRequestPayload;
import com.tmnc.data.exchange.users.LoginUserResponsePayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Service
public class LoginService {

    private final AuthenticationManager manager;
    private final SessionConfig config;
    private final UserService userService;

    @Autowired
    private LoginService(AuthenticationManager manager,
                         SessionConfig config,
                         UserService userService) {
        this.manager = manager;
        this.config = config;
        this.userService = userService;
    }

    public LoginUserResponsePayload authenticate(LoginUserRequestPayload user,
                                                 HttpServletRequest request) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                user.getUsername(), user.getPassword());
        Authentication newAuth = manager.authenticate(authentication);

        return this.updatePrincipals(newAuth, request, false);
    }

    public LoginUserResponsePayload getAuthenticatedUser(UserCredentials principal) {
        return this.createLoginUserResponsePayload(principal);
    }

    public LoginUserResponsePayload updatePrincipals(Authentication newAuth,
                                                     HttpServletRequest request,
                                                     boolean sameSession) {
        SecurityContext context = SecurityContextHolder.getContext();

        context.setAuthentication(newAuth);

        HttpSession session = request.getSession(!sameSession);
        session.setAttribute("SPRING_SECURITY_CONTEXT", newAuth);
        session.setMaxInactiveInterval(config.getMaxInactiveInterval());

        UserCredentials principal = (UserCredentials) newAuth.getPrincipal();

        return this.createLoginUserResponsePayload(principal);
    }

    public boolean logout(HttpServletRequest request) {
        if (!isAuthenticated(request)) {
            return true;
        }

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        SecurityContextHolder.clearContext();

        return true;
    }

    public boolean isAuthenticated(HttpServletRequest request) {
        return this.isAuthenticationValid() && this.isSessionValid(request);
    }

    private LoginUserResponsePayload createLoginUserResponsePayload(UserCredentials principal) {
        LoginUserResponsePayload payload = new LoginUserResponsePayload();

        payload.setPrincipal(new SimpleUserPrincipal(principal));
        payload.setProjects(userService.getProjectInfo(principal.getObjectId()));

        return payload;
    }

    private boolean isAuthenticationValid() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null) {
            return false;
        }

        Authentication authentication = context.getAuthentication();
        return authentication != null && !authentication.getPrincipal().equals("anonymousUser");
    }

    private boolean isSessionValid(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        return session != null;
    }
}
