package com.tmnc.services;

import com.tmnc.data.entities.Participant;
import com.tmnc.data.entities.Project;
import com.tmnc.data.exchange.project.CreateNewProjectRequestPayload;
import com.tmnc.data.exchange.project.ProjectResponsePayload;
import com.tmnc.data.exchange.project.UpdateProjectRequestPayload;
import com.tmnc.data.repositories.ProjectRepository;
import com.tmnc.data.utils.StoreEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class ProjectService {

    private final FileUploadStorageService uploadService;
    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectService(FileUploadStorageService uploadService,
                          ProjectRepository projectRepository) {
        this.uploadService = uploadService;
        this.projectRepository = projectRepository;
    }

    public ProjectResponsePayload createNewProject(CreateNewProjectRequestPayload payload) {
        Participant newProjectId = this.projectRepository.createNewProject(payload);

        ProjectResponsePayload responsePayload = new ProjectResponsePayload();
        responsePayload.setNewParticipant(newProjectId);

        return responsePayload;
    }

    public List<BigInteger> getDocumentIDs(BigInteger projectId) {
        return projectRepository.getProjectById(projectId).getDocuments();
    }

    public List<BigInteger> getLabelsInProjectIDs(BigInteger projectId) {
        return projectRepository.getProjectById(projectId).getLabels();
    }

    public List<BigInteger> getParticipantsIDs(BigInteger projectId) {
        return projectRepository.getProjectById(projectId).getParticipants();
    }

    public List<BigInteger> getSprintsIDs(BigInteger projectId) {
        return projectRepository.getProjectById(projectId).getSprints();
    }

    public Project getProject(BigInteger id) {
        return projectRepository.getProjectById(id);
    }

    public BigInteger updateProject(UpdateProjectRequestPayload project) {
        MultipartFile logo = project.getLogo();

        Path absoluteLocation = null;

        if (logo != null && !logo.isEmpty()) {
            StoreEntry logoEntry = uploadService.createStoreEntry(
                    logo, uploadService.getConfig().getProjectsPath(), project.getObjectId(), "logo");

            absoluteLocation = logoEntry.getAbsoluteLocation();

            if (!uploadService.isFileExists(absoluteLocation)) {
                uploadService.clearDirectory(absoluteLocation.getParent());
                uploadService.storeFiles(logoEntry);
            }
        }

        return this.projectRepository.updateProject(
                project.getObjectId(),
                project.getName(),
                project.getDescription(),
                project.getPhase(),
                absoluteLocation,
                project.getStartDate(),
                project.getEndDate());
    }

    public boolean deleteProject(BigInteger projectId) {
        this.uploadService.deleteDocumentsInProject(projectId);
        return this.projectRepository.deleteProject(projectId).equals(BigInteger.ONE);
    }

    public void getProjectLogo(HttpServletResponse response, BigInteger projectId) {
        String filePath = getProject(projectId).getLogo();
        uploadService.sendToDownload(response, filePath);
    }
}