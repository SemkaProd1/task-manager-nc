package com.tmnc.services;

import com.tmnc.data.entities.Log;
import com.tmnc.data.exchange.log.CreateNewLogRequestPayload;
import com.tmnc.data.repositories.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class LogService {

    private final LogRepository repository;

    @Autowired
    public LogService(LogRepository repository) {
        this.repository = repository;
    }

    public BigInteger create(CreateNewLogRequestPayload payload) {
        return repository.create(payload);
    }

    public Log get(BigInteger objectId) {
        return repository.get(objectId);
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.delete(objectId);
    }
}