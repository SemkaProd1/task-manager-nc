package com.tmnc.services;

import com.tmnc.core.exceptions.TMNCApplicationException;
import com.tmnc.core.exceptions.file.FileStorageStoreException;
import com.tmnc.data.entities.Documentation;
import com.tmnc.data.entities.ProjectDocumentation;
import com.tmnc.data.repositories.DocumentationRepository;
import com.tmnc.data.utils.StoreEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
public class DocumentationService {

    private final DocumentationRepository documentationRepository;
    private final ProjectService projectService;
    private final FileUploadStorageService uploadService;
    private final ParticipantService participantService;
    private final MailService mailService;

    @Autowired
    public DocumentationService(DocumentationRepository documentationRepository,
                                ProjectService projectService,
                                FileUploadStorageService uploadService,
                                ParticipantService participantService,
                                MailService mailService) {
        this.documentationRepository = documentationRepository;
        this.projectService = projectService;
        this.uploadService = uploadService;
        this.participantService = participantService;
        this.mailService = mailService;
    }

    public BigInteger create(MultipartFile filePath,
                             BigInteger parentId,
                             BigInteger creatorId,
                             Date createdAt,
                             String directoryName) {
        Path projectsPath = uploadService.getConfig().getProjectsPath();
        StoreEntry storeEntry = uploadService.createStoreEntry(
                filePath, projectsPath, parentId, directoryName);

        if (uploadService.isFileExists(storeEntry.getAbsoluteLocation())) {
            throw new FileStorageStoreException("Application could not store files! ",
                    new FileAlreadyExistsException(storeEntry.getAbsoluteLocation().getFileName().toString()));
        }

        Path absoluteLocation = uploadService.replaceDocumentOnProjects(filePath, parentId, directoryName);

        return documentationRepository.create(
                parentId,
                filePath.getOriginalFilename(),
                absoluteLocation,
                creatorId,
                createdAt);
    }

    public Documentation get(BigInteger objectId) {
        return documentationRepository.get(objectId);
    }

    public void downloadFile(HttpServletResponse response, BigInteger id) {
        String file = get(id).getFilePath();

        uploadService.sendToDownload(response, file);
    }

    public BigInteger update(BigInteger documentId, MultipartFile filePath, BigInteger parentId, String documentName,
                             BigInteger creatorId, String directoryName) {
        Date sysdate = new Date(Calendar.getInstance().getTime().getTime());
        Path absoluteLocation = uploadService.replaceDocumentOnProjects(filePath,
                parentId, directoryName);

        return documentationRepository.update(parentId, documentName, absoluteLocation,
                creatorId, sysdate, documentId);
    }

    void deleteDocumentsInTask(BigInteger taskId, BigInteger projectId) {
        uploadService.deleteDocumentsInTask(taskId, projectId);
    }

    public List<ProjectDocumentation> getDocumentation(BigInteger projectId) {
        return documentationRepository.getProjectDocuments(projectService.getDocumentIDs(projectId));
    }

    public ProjectDocumentation getProjectDocument(BigInteger documentId) {
        return documentationRepository.getProjectDocuments(Collections.singletonList(documentId)).get(0);
    }

    public BigInteger delete(BigInteger objectId) {
        uploadService.deleteFiles(Paths.get(get(objectId).getFilePath()));
        return documentationRepository.delete(objectId);
    }


}