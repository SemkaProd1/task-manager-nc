package com.tmnc.data.exchange.project;

import com.tmnc.data.utils.ProjectPhase;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.sql.Date;

public class UpdateProjectRequestPayload {

    private BigInteger objectId;
    private String name;
    private String description;
    private ProjectPhase phase;
    private Date startDate;
    private Date endDate;
    private MultipartFile logo;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectPhase getPhase() {
        return phase;
    }

    public void setPhase(ProjectPhase phase) {
        this.phase = phase;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public MultipartFile getLogo() {
        return logo;
    }

    public void setLogo(MultipartFile logo) {
        this.logo = logo;
    }
}
