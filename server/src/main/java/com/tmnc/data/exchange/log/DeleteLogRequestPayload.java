package com.tmnc.data.exchange.log;

import java.math.BigInteger;

public class DeleteLogRequestPayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}