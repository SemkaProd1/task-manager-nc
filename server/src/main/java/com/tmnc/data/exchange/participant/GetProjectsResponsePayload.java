package com.tmnc.data.exchange.participant;

import com.tmnc.data.utils.ParticipantRole;

public class GetProjectsResponsePayload {

    private String projectName;

    private ParticipantRole role;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ParticipantRole getRole() {
        return role;
    }

    public void setRole(ParticipantRole role) {
        this.role = role;
    }
}
