package com.tmnc.data.exchange.documentation;

import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.sql.Date;

public class CreateNewDocumentationRequestPayload {

    private BigInteger parentId;            // or project or task
    private BigInteger creatorParticipantId;
    private MultipartFile filePath;
    private Date creationTime;

    public BigInteger getParentId() {
        return parentId;
    }

    public void setParentId(BigInteger parentId) {
        this.parentId = parentId;
    }

    public MultipartFile getFilePath() {
        return filePath;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public void setFilePath(MultipartFile filePath) {
        this.filePath = filePath;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}