package com.tmnc.data.exchange.users;

import java.math.BigInteger;

public class UpdateUserEmailRequestPayload {

    private BigInteger objectId;
    private String email;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
