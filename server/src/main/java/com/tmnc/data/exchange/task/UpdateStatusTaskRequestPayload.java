package com.tmnc.data.exchange.task;

import com.tmnc.data.utils.TaskStatus;

import java.math.BigInteger;

public class UpdateStatusTaskRequestPayload {

    private BigInteger objectId;
    private TaskStatus status;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }
}