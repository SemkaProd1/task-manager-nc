package com.tmnc.data.exchange.users;


import com.tmnc.data.entities.SimpleUserPrincipal;
import com.tmnc.data.entities.UserCredentials;
import com.tmnc.data.exchange.project.ProjectInfoForUserResponsePayload;

import java.math.BigInteger;
import java.util.List;

public class LoginUserResponsePayload {
    private List<ProjectInfoForUserResponsePayload> projects;
    private SimpleUserPrincipal principal;

    public List<ProjectInfoForUserResponsePayload> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectInfoForUserResponsePayload> projects) {
        this.projects = projects;
    }

    public SimpleUserPrincipal getPrincipal() {
        return principal;
    }

    public void setPrincipal(SimpleUserPrincipal principal) {
        this.principal = principal;
    }
}
