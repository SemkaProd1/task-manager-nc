package com.tmnc.data.exchange.project;

import java.math.BigInteger;
import java.sql.Date;

public class CreateNewProjectRequestPayload {

	private String projectName;
	private String projectDescription;
	private BigInteger creatorId;
	private Date startDate;
	private Date endDate;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public BigInteger getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(BigInteger creatorId) {
		this.creatorId = creatorId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
