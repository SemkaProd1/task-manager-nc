package com.tmnc.data.exchange.sprint;

import com.tmnc.data.entities.Sprint;

import java.math.BigInteger;
import java.util.Map;

public class SprintWithStatistics extends Sprint {
    private Map<Object, BigInteger> statistics;

    public SprintWithStatistics(Sprint sprint) {
        this.setName(sprint.getName());
        this.setDescription(sprint.getDescription());
        this.setStartDate(sprint.getStartDate());
        this.setStatus(sprint.getStatus());
        this.setProjectId(sprint.getProjectId());
        this.setTasks(sprint.getTasks());
        this.setEndDate(sprint.getEndDate());
        this.setObjectId(sprint.getObjectId());
    }

    public Map<Object, BigInteger> getStatistics() {
        return statistics;
    }

    public void setStatistics(Map<Object, BigInteger> statistics) {
        this.statistics = statistics;
    }
}
