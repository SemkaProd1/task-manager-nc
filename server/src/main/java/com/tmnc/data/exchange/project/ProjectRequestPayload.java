package com.tmnc.data.exchange.project;

import java.math.BigInteger;

public class ProjectRequestPayload {
    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}