package com.tmnc.data.exchange.comment;

import com.tmnc.data.entities.Comment;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.math.BigInteger;
import java.sql.Date;

public class CommentView {

    private BigInteger commentId;
    private BigInteger taskId;
    private BigInteger creatorParticipantId;
    private String content;
    private Date timeChanged;
    private Boolean isEdited;
    private String username;
    private String firstName;
    private BigInteger userId;

    public CommentView() {

    }

    public CommentView(Comment comment) {
        this.creatorParticipantId = comment.getCreatorParticipantId();
        this.content = comment.getContent();
        this.timeChanged = comment.getTimeChanged();
        this.isEdited = comment.getEdited();
        this.commentId = comment.getObjectId();
        this.taskId = comment.getTaskId();
    }

    public BigInteger getCommentId() {
        return commentId;
    }

    public void setCommentId(BigInteger commentId) {
        this.commentId = commentId;
    }

    public BigInteger getTaskId() {
        return taskId;
    }

    public void setTaskId(BigInteger taskId) {
        this.taskId = taskId;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimeChanged() {
        return timeChanged;
    }

    public void setTimeChanged(Date timeChanged) {
        this.timeChanged = timeChanged;
    }

    public Boolean getEdited() {
        return isEdited;
    }

    public void setEdited(Boolean edited) {
        isEdited = edited;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public BigInteger getUserId() {
        return userId;
    }

    public void setUserId(BigInteger userId) {
        this.userId = userId;
    }
}

