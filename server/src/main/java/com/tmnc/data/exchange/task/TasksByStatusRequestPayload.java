package com.tmnc.data.exchange.task;

import com.tmnc.data.utils.TaskStatus;

import java.math.BigInteger;

public class TasksByStatusRequestPayload {

    private BigInteger objectId;

    private TaskStatus taskStatus;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }
}