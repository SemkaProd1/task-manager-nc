package com.tmnc.data.exchange.project;

import com.tmnc.data.entities.Project;
import com.tmnc.data.utils.ProjectPhase;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class ProjectInfoResponsePayload {
    private BigInteger objectId;
    private BigInteger creator;
    private String name;
    private String description;
    private ProjectPhase phase;
    private Date startDate;
    private Date endDate;
    private List<BigInteger> participants;
    private List<BigInteger> sprints;
    private List<BigInteger> logs;
    private List<BigInteger> labels;
    private List<BigInteger> documents;

    public ProjectInfoResponsePayload(Project project) {
        this.objectId = project.getObjectId();
        this.creator = project.getCreator();
        this.name = project.getName();
        this.description = project.getDescription();
        this.phase = project.getPhase();
        this.startDate = project.getStartDate();
        this.endDate = project.getEndDate();
        this.participants = project.getParticipants();
        this.sprints = project.getSprints();
        this.logs = project.getLogs();
        this.labels = project.getLabels();
        this.documents = project.getDocuments();
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public BigInteger getCreator() {
        return creator;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ProjectPhase getPhase() {
        return phase;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public List<BigInteger> getParticipants() {
        return participants;
    }

    public List<BigInteger> getSprints() {
        return sprints;
    }

    public List<BigInteger> getLogs() {
        return logs;
    }

    public List<BigInteger> getLabels() {
        return labels;
    }

    public List<BigInteger> getDocuments() {
        return documents;
    }
}
