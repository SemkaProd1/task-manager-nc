package com.tmnc.data.exchange.project;

import com.tmnc.data.utils.ParticipantRole;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.math.BigInteger;

public class ProjectInfoForUserResponsePayload {

    private BigInteger objectId;
    private String projectName;
    private ParticipantRole participantRole;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ParticipantRole getParticipantRole() {
        return participantRole;
    }

    public void setParticipantRole(ParticipantRole participantRole) {
        this.participantRole = participantRole;
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}
