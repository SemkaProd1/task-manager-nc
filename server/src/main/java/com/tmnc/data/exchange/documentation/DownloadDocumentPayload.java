package com.tmnc.data.exchange.documentation;

import java.math.BigInteger;

public class DownloadDocumentPayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}
