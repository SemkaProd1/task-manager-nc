package com.tmnc.data.exchange.invite;

import com.tmnc.data.utils.ParticipantRole;

import java.math.BigInteger;

public class CreateNewInviteRequestPayload {

    private BigInteger projectId;
    private ParticipantRole role;
    private String url;
    private String email;

    public CreateNewInviteRequestPayload() {
    }

    public ParticipantRole getRole() {
        return role;
    }

    public void setRole(ParticipantRole role) {
        this.role = role;
    }

    public BigInteger getProjectId() {
        return projectId;
    }

    public void setProjectId(BigInteger projectId) {
        this.projectId = projectId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}