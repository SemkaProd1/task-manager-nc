package com.tmnc.data.exchange.log;

import java.math.BigInteger;

public class CreateNewLogRequestPayload {

    private BigInteger taskId;
    private BigInteger creatorParticipantId;
    private Object changeParam;
    private String newValue;
    private String oldValue;

    public BigInteger getTaskId() {
        return taskId;
    }

    public void setTaskId(BigInteger taskId) {
        this.taskId = taskId;
    }

    public Object getChangeParam() {
        return changeParam;
    }

    public void setChangeParam(Object changeParam) {
        this.changeParam = changeParam;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }
}