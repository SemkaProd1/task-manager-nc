package com.tmnc.data.exchange.users;

import org.springframework.web.multipart.MultipartFile;
import java.math.BigInteger;

public class UpdateUserRequestPayload extends CreateNewUserRequestPayload {

    private BigInteger objectId;
    private MultipartFile avatar;
    private String oldPassword;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public MultipartFile getAvatar() {
        return avatar;
    }

    public void setAvatar(MultipartFile avatar) {
        this.avatar = avatar;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
