package com.tmnc.data.exchange.sprint;

import com.tmnc.data.utils.SprintStatus;

import java.math.BigInteger;
import java.sql.Date;

public class CreateNewSprintRequestPayload {

    private BigInteger projectId;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private SprintStatus status;


    public String getName() {
        return name;
    }

    public BigInteger getProjectId() {
        return projectId;
    }

    public void setProjectId(BigInteger projectId) {
        this.projectId = projectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SprintStatus getStatus() {
        return status;
    }

    public void setStatus(SprintStatus status) {
        this.status = status;
    }

}
