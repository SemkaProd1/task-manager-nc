package com.tmnc.data.exchange.task;

import java.math.BigInteger;

public class TaskResponsePayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}