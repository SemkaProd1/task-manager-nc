package com.tmnc.data.exchange.participant;

import com.tmnc.data.utils.ParticipantRole;

import java.math.BigInteger;

public class UpdateParticipantRequestPayload {

    private BigInteger objectId;
    private BigInteger userId;
    private BigInteger projectId;
    private ParticipantRole role;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public BigInteger getUserId() {
        return userId;
    }

    public void setUserId(BigInteger userId) {
        this.userId = userId;
    }

    public BigInteger getProjectId() {
        return projectId;
    }

    public void setProjectId(BigInteger projectId) {
        this.projectId = projectId;
    }

    public ParticipantRole getRole() {
        return role;
    }

    public void setRole(ParticipantRole role) {
        this.role = role;
    }
}