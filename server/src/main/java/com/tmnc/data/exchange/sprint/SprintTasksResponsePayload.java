package com.tmnc.data.exchange.sprint;

import com.tmnc.data.entities.Label;
import com.tmnc.data.utils.TaskPriority;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.data.utils.TaskType;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class SprintTasksResponsePayload {

    private BigInteger taskId;
    private String name;
    private Date deadline;
    private List<Label> labels;
    private TaskStatus status;
    private BigInteger assignee;
    private String assigneeUsername;
    private TaskPriority priority;
    private TaskType type;


    public BigInteger getTaskId() {
        return taskId;
    }

    public void setTaskId(BigInteger taskId) {
        this.taskId = taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public BigInteger getAssignee() {
        return assignee;
    }

    public void setAssignee(BigInteger assignee) {
        this.assignee = assignee;
    }

    public String getAssigneeUsername() {
        return assigneeUsername;
    }

    public void setAssigneeUsername(String assigneeUsername) {
        this.assigneeUsername = assigneeUsername;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }
}