package com.tmnc.data.exchange.task;

import com.tmnc.data.utils.TaskPriority;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.data.utils.TaskType;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class UpdateTaskRequestPayload {

    private BigInteger objectId;
    private BigInteger sprintId;
    private BigInteger creatorParticipantId;
    private BigInteger assigneeParticipantId;
    private String name;
    private String description;
    private TaskType type;
    private TaskStatus status;
    private TaskPriority priority;
    private Date deadline;
    private List<BigInteger> labels;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public List<BigInteger> getLabels() {
        return labels;
    }

    public void setLabels(List<BigInteger> labels) {
        this.labels = labels;
    }

    public BigInteger getSprintId() {
        return sprintId;
    }

    public void setSprintId(BigInteger sprintId) {
        this.sprintId = sprintId;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public BigInteger getAssigneeParticipantId() {
        return assigneeParticipantId;
    }

    public void setAssigneeParticipantId(BigInteger assigneeParticipantId) {
        this.assigneeParticipantId = assigneeParticipantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }
}