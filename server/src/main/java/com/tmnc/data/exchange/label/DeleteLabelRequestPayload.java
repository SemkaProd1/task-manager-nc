package com.tmnc.data.exchange.label;

import java.math.BigInteger;

public class DeleteLabelRequestPayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}