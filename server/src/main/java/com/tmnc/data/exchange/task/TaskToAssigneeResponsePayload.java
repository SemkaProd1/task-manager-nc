package com.tmnc.data.exchange.task;

import com.tmnc.data.entities.Label;
import com.tmnc.data.entities.Task;
import com.tmnc.data.utils.TaskStatus;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class TaskToAssigneeResponsePayload {

    private BigInteger taskId;
    private String taskName;
    private Date deadline;
    private List<Label> labels;
    private TaskStatus status;
    private BigInteger projectId;
    private String projectName;

    TaskToAssigneeResponsePayload(){

    }

    public TaskToAssigneeResponsePayload(Task task) {
        this.taskId = task.getObjectId();
        this.taskName = task.getName();
        this.deadline = task.getDeadline();
        this.status = task.getStatus();
    }

    public BigInteger getTaskId() {
        return taskId;
    }

    public void setTaskId(BigInteger taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public BigInteger getProjectId() {
        return projectId;
    }

    public void setProjectId(BigInteger projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
