package com.tmnc.data.exchange.sprint;

import java.math.BigInteger;

public class SprintResponsePayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}
