package com.tmnc.data.exchange.users;

import java.math.BigInteger;

public class UserResponsePayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}