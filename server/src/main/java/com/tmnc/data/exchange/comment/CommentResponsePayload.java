package com.tmnc.data.exchange.comment;

import java.math.BigInteger;

public class CommentResponsePayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}