package com.tmnc.data.exchange.comment;

import java.math.BigInteger;

public class CreateNewCommentRequestPayload {

    private BigInteger taskId;
    private BigInteger creatorParticipantId;
    private String content;

    public BigInteger getTaskId() {
        return taskId;
    }

    public void setTaskId(BigInteger taskId) {
        this.taskId = taskId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }
}