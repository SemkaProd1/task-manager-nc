package com.tmnc.data.exchange.participant;

import java.math.BigInteger;

public class DeleteParticipantRequestPayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}