package com.tmnc.data.exchange.invite;

import java.math.BigInteger;

public class DeleteInviteRequestPayload {

    private BigInteger objectId;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }
}