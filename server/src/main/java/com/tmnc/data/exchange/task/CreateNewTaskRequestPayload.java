package com.tmnc.data.exchange.task;

import com.tmnc.data.utils.TaskPriority;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.data.utils.TaskType;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class CreateNewTaskRequestPayload {

    private BigInteger sprintId;
    private BigInteger assigneeParticipantId;
    private BigInteger creatorParticipantId;
    private String name;
    private String description;
    private Date deadline;
    private TaskType type;
    private TaskStatus status;
    private TaskPriority priority;
    private List<BigInteger> labels;
    private List<MultipartFile> documents;

    public BigInteger getSprintId() {
        return sprintId;
    }

    public void setSprintId(BigInteger sprintId) {
        this.sprintId = sprintId;
    }

    public BigInteger getAssigneeParticipantId() {
        return assigneeParticipantId;
    }

    public void setAssigneeParticipantId(BigInteger assigneeParticipantId) {
        this.assigneeParticipantId = assigneeParticipantId;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public List<BigInteger> getLabels() {
        return labels;
    }

    public void setLabels(List<BigInteger> labels) {
        this.labels = labels;
    }

    public List<MultipartFile> getDocuments() {
        return documents;
    }

    public void setDocuments(List<MultipartFile> documents) {
        this.documents = documents;
    }
}