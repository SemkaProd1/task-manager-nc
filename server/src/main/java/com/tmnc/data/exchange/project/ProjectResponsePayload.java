package com.tmnc.data.exchange.project;

import com.tmnc.data.entities.Participant;

public class ProjectResponsePayload {

	private Participant newParticipant;

	public Participant getNewParticipant() {
		return newParticipant;
	}

	public void setNewParticipant(Participant newParticipant) {
		this.newParticipant = newParticipant;
	}
}