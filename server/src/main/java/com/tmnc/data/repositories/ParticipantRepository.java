package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.core.ncl.NCLUpdateChangeWatcher;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.core.ncl.update.NCLReferenceRecord;
import com.tmnc.data.converters.NCLToParticipantConverter;
import com.tmnc.data.converters.NCLToParticipantsSettingsConverter;
import com.tmnc.data.entities.Participant;
import com.tmnc.data.exchange.participant.CreateNewParticipantRequestPayload;
import com.tmnc.data.exchange.participant.GetParticipantsRequestPayload;
import com.tmnc.data.exchange.participant.UpdateParticipantRequestPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@Repository
public class ParticipantRepository {

    private final NCLToParticipantConverter participantConverter;
    private final NCLObjectRepository repository;
    private final NCLUpdateChangeWatcher changeWatcher;
    private final ProjectRepository projectRepository;
    private final NCLToParticipantsSettingsConverter participantsSettingsConverter;
    private final NamedParameterJdbcTemplate template;


    @Autowired
    public ParticipantRepository(NCLToParticipantConverter participantConverter,
                                 NCLObjectRepository repository,
                                 NCLUpdateChangeWatcher changeWatcher,
                                 ProjectRepository projectRepository,
                                 NCLToParticipantsSettingsConverter participantsSettingsConverter, NamedParameterJdbcTemplate template) {
        this.participantConverter = participantConverter;
        this.repository = repository;
        this.changeWatcher = changeWatcher;
        this.projectRepository = projectRepository;
        this.participantsSettingsConverter = participantsSettingsConverter;
        this.template = template;
    }


    public BigInteger create(CreateNewParticipantRequestPayload payload) {
        SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("user_id", Types.NUMERIC),
                new SqlParameter("project_id", Types.NUMERIC),
                new SqlParameter("role", Types.NUMERIC)
        };

        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("user_id", payload.getUserId())
                .addValue("project_id", payload.getProjectId())
                .addValue("role", payload.getRole().getListTypeId());

        return repository.saveObject("tmnc_commons", "create_new_participant", parameters, source);
    }

    public Participant get(BigInteger objectId) {
        return repository.getObject(objectId, participantConverter);
    }

    public BigInteger update(UpdateParticipantRequestPayload payload) {
        Participant actual = get(payload.getObjectId());

        Map<String, BigInteger> attributes = new HashMap<>();
        attributes.put("role", BigInteger.valueOf(15));

        Map<String, BigInteger> references = new HashMap<>();
        references.put("projectId", BigInteger.valueOf(14));

        List<NCLAttributeRecord> attributeRecords = changeWatcher.compareAttributes(actual, payload, attributes);
        List<NCLReferenceRecord> referenceRecords = changeWatcher.compareReference(actual, payload, references);

        return repository.update(actual.getObjectId(), payload.getUserId(), attributeRecords, referenceRecords);
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }

    public List<GetParticipantsRequestPayload> getParticipants(BigInteger projectId) {
        List<BigInteger> userAttrs = Arrays.asList(BigInteger.valueOf(-1 /*object_id Attr ID*/),
                BigInteger.valueOf(-12 /*name Attr ID*/), BigInteger.valueOf(8 /*last_name Attr ID*/),
                BigInteger.valueOf(9 /*first_name Attr ID*/), BigInteger.valueOf(12 /*avatar Attr ID*/));

        List<BigInteger> roleAttr = Collections.singletonList(BigInteger.valueOf(15 /*role Attr ID*/));
        List<BigInteger> participantsIDs = new ArrayList<>(projectRepository.getParticipantsIDs(projectId));

        List<BigInteger> usersIds = new ArrayList<>();

        for (BigInteger participantsID : participantsIDs) {
            usersIds.add(repository.getFirstParent(participantsID));
        }

        List<NCLObject> objects = repository.selectObjects(usersIds, userAttrs);
        List<GetParticipantsRequestPayload> participants = new ArrayList<>();
        for (int i = 0; i < objects.size(); i++) {

            GetParticipantsRequestPayload entity = participantsSettingsConverter.convert(objects.get(i));
            entity.setRole(participantConverter.convert(repository.selectObject(participantsIDs.get(i), roleAttr)).getRole());
            participants.add(entity);

        }
        return participants;

    }

    public String getEmailByParticipantId(BigInteger participantId) {
        SimpleJdbcCall call = new SimpleJdbcCall(template.getJdbcTemplate())
                .withCatalogName("tmnc_commons")
                .withFunctionName("get_email_by_participant_id")
                .declareParameters(new SqlParameter("part_id", Types.NUMERIC))
                .withReturnValue();
        return call.executeFunction(
                String.class,
                new MapSqlParameterSource()
                        .addValue("part_id", participantId)

        );
    }
}