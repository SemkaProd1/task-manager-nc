package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.data.converters.NCLToLogConverter;
import com.tmnc.data.entities.Log;
import com.tmnc.data.exchange.log.CreateNewLogRequestPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.Types;

@Repository
public class LogRepository {

    private final NCLToLogConverter converter;
    private final NCLObjectRepository repository;

    @Autowired
    public LogRepository(NCLToLogConverter converter, NCLObjectRepository repository) {
        this.converter = converter;
        this.repository = repository;
    }

    public BigInteger create(CreateNewLogRequestPayload payload) {
        SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("task_id", Types.NUMERIC),
                new SqlParameter("user_id", Types.NUMERIC),
                new SqlParameter("change_param", Types.VARCHAR),
                new SqlParameter("new_value", Types.VARCHAR),
                new SqlParameter("old_value", Types.VARCHAR)
        };
        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("task_id", payload.getTaskId())
                .addValue("user_id", payload.getCreatorParticipantId())
                .addValue("change_param", payload.getChangeParam())
                .addValue("new_value", payload.getNewValue())
                .addValue("old_value", payload.getOldValue());

        return repository.saveObject("tmnc_commons", "create_new_log", parameters, source);
    }

    public Log get(BigInteger objectId) {
        return repository.getObject(objectId, converter);
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }
}