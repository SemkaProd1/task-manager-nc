package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.data.converters.NCLToParticipantConverter;
import com.tmnc.data.converters.NCLToProjectInfoConverter;
import com.tmnc.data.converters.NCLToUserConverter;
import com.tmnc.data.converters.NCLToUserDetailsConverter;
import com.tmnc.data.entities.Participant;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.participant.ParticipantResponsePayload;
import com.tmnc.data.exchange.project.ProjectInfoForUserResponsePayload;
import com.tmnc.data.exchange.users.CreateNewUserRequestPayload;
import com.tmnc.data.exchange.users.UpdateUserEmailRequestPayload;
import com.tmnc.data.exchange.users.UpdateUserRequestPayload;
import com.tmnc.services.ProjectService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {
    private final static String GET_USER_DETAILS_BY_USERNAME =
            "SELECT * from table (tmnc_commons.GET_USER_BY_USER_NAME(:username))";
    private final static String GET_OBJ_INFO =
            "SELECT * from table (tmnc_commons.GET_OBJ_INFO(:id))";

    private final Logger logger = LogManager.getLogger(UserRepository.class);
    private final NCLToUserConverter userAccountConverter;
    private final NCLObjectRepository repository;
    private final NCLToUserDetailsConverter userDetailsConverter;
    private final NCLToParticipantConverter participantConverter;
    private final NCLToProjectInfoConverter infoConverter;
    private NamedParameterJdbcTemplate jdbcTemplate;
    private final PasswordEncoder encoder;
    private final ProjectService projectService;


    @Autowired
    public UserRepository(NCLToUserDetailsConverter userDetailsConverter,
                          NCLToUserConverter userAccountConverter,
                          NCLObjectRepository repository,
                          NCLToParticipantConverter participantConverter,
                          NCLToProjectInfoConverter infoConverter,
                          NamedParameterJdbcTemplate jdbcTemplate,
                          PasswordEncoder encoder,
                          ProjectService projectService) {
        this.userAccountConverter = userAccountConverter;
        this.repository = repository;
        this.userDetailsConverter = userDetailsConverter;
        this.participantConverter = participantConverter;
        this.infoConverter = infoConverter;
        this.jdbcTemplate = jdbcTemplate;

        this.projectService = projectService;
        this.encoder = encoder;
    }

    public UserDetails getUserDetailsByName(String username) {
        return this.repository.executeQuery(GET_USER_DETAILS_BY_USERNAME,
                new MapSqlParameterSource("username", username),
                userDetailsConverter
        );
    }

    public UserAccount getUserAccountById(BigInteger id) {
        return this.repository.executeQuery(GET_OBJ_INFO,
                new MapSqlParameterSource("id", id),
                userAccountConverter
        );
    }

    public BigDecimal isUserExist(String username) {
        SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate.getJdbcTemplate())
                .withCatalogName("tmnc_commons")
                .withFunctionName("is_user_exists")
                .declareParameters(new SqlParameter("username", Types.VARCHAR))
                .withReturnValue();
        return call.executeFunction(BigDecimal.class,
                new MapSqlParameterSource("username", username));
    }

    public BigDecimal isEmailExists(String email) {
        SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate.getJdbcTemplate())
                .withCatalogName("tmnc_commons")
                .withFunctionName("is_email_exists")
                .declareParameters(new SqlParameter("email_in", Types.VARCHAR))
                .withReturnValue();
        return call.executeFunction(BigDecimal.class,
                new MapSqlParameterSource("email_in", email));
    }

    public BigInteger createNewUser(CreateNewUserRequestPayload user) {
        SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate.getJdbcTemplate())
                .withCatalogName("tmnc_commons").withFunctionName("create_new_user")
                .declareParameters(new SqlParameter("username", Types.VARCHAR),
                        new SqlParameter("last_name", Types.VARCHAR),
                        new SqlParameter("first_name", Types.VARCHAR),
                        new SqlParameter("password", Types.VARCHAR),
                        new SqlParameter("email", Types.VARCHAR)).withReturnValue();

        return call.executeFunction(BigDecimal.class, new MapSqlParameterSource()
                .addValue("username", user.getUsername())
                .addValue("last_name", user.getLastName())
                .addValue("first_name", user.getFirstName())
                .addValue("password", this.encoder.encode(user.getPassword()))
                .addValue("email", user.getEmail())).toBigInteger();
    }

    public BigInteger updateUserAccount(
            Path avatar, UpdateUserRequestPayload updateUserRequestPayload) {

        UserAccount currentUserAccount = this.getUserAccountById(updateUserRequestPayload.getObjectId());
        List<NCLAttributeRecord> attributes = new ArrayList<>();

        if (updateUserRequestPayload.getUsername() != null && updateUserRequestPayload.getUsername().trim().length() != 0) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(-12 /*name Attr ID*/), updateUserRequestPayload.getUsername(), null, null));
        }
        if (updateUserRequestPayload.getFirstName() != null && updateUserRequestPayload.getLastName() != null
                && updateUserRequestPayload.getFirstName().trim().length() != 0
                && updateUserRequestPayload.getLastName().trim().length() != 0) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(8 /*last_name Attr ID*/), updateUserRequestPayload.getLastName(), null, null));
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(9 /*first_name Attr ID*/), updateUserRequestPayload.getFirstName(), null, null));
        }

        String avatarInString = avatar == null ? currentUserAccount.getAvatar() : avatar.toString();


        if (currentUserAccount.getAvatar() == null || !currentUserAccount.getAvatar().equals(avatarInString)) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(12 /*avatar Attr ID*/), avatarInString, null, null));
        }

        this.repository.update(updateUserRequestPayload.getObjectId(), BigInteger.ZERO, attributes, null);

        return updateUserRequestPayload.getObjectId();
    }

    public BigInteger updateEmail(UpdateUserEmailRequestPayload payload) {
        List<NCLAttributeRecord> attributes = new ArrayList<>();
        if (payload.getEmail() != null &&
                payload.getEmail().trim().length() != 0) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(11 /*email Attr ID*/),
                    payload.getEmail(), null, null));
        }
        this.repository.update(payload.getObjectId(), BigInteger.ZERO, attributes, null);

        return payload.getObjectId();
    }

    public BigInteger updatePassword(BigInteger objectId, String encodedPassword) {
        return updateAttributes(
                objectId,
                new NCLAttributeRecord(BigInteger.valueOf(10 /* password Attr ID */), encodedPassword, null, null)
        );
    }

    private BigInteger updateAttributes(BigInteger objectId, NCLAttributeRecord... attributes) {
        return repository.update(objectId, BigInteger.ZERO, Arrays.asList(attributes), null);
    }

    public BigInteger deleteUser(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }

    public ProjectInfoForUserResponsePayload getProjectInfo(BigInteger participantId) {
        List<BigInteger> attrs = new ArrayList<>(Arrays.asList(new BigInteger("15"), new BigInteger("-12")));
        BigInteger projectId = repository.getObject(participantId, participantConverter).getProjectId();
        List<NCLObject> objects = repository.selectObjects(Arrays.asList(participantId, projectId), attrs);

        return infoConverter.convert(objects);
    }

    public ParticipantResponsePayload getCurrentParticipant(BigInteger userId, BigInteger projectId) {
        UserAccount account = this.repository.getObject(userId, userAccountConverter);
        ParticipantResponsePayload response = new ParticipantResponsePayload();

        for (int i = 0; i < account.getParticipants().size(); i++) {
            Participant participant = this.repository.getObject(account.getParticipants().get(i), participantConverter);
            if (participant.getProjectId().equals(projectId)) {
                response.setObjectId(participant.getObjectId());
                return response;
            }
        }
        return response;
    }
}