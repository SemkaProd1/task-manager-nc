package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.data.converters.NCLToInviteConverter;
import com.tmnc.data.entities.Invite;
import com.tmnc.data.exchange.invite.CreateNewInviteRequestPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.Types;

@Repository
public class InviteRepository {

    private final static String GET_INVITE_BY_URL = "SELECT * from table (tmnc_commons.GET_INVITE_BY_URL(:url))";
    private final NCLToInviteConverter converter;
    private final NCLObjectRepository repository;

    @Autowired
    public InviteRepository(NCLToInviteConverter converter, NCLObjectRepository repository) {
        this.converter = converter;
        this.repository = repository;
    }

    public BigInteger create(CreateNewInviteRequestPayload payload) {
        SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("project_id", Types.NUMERIC),
                new SqlParameter("email", Types.VARCHAR),
                new SqlParameter("url", Types.VARCHAR),
                new SqlParameter("role", Types.NUMERIC)
        };
        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("project_id", payload.getProjectId())
                .addValue("email", payload.getEmail())
                .addValue("url", payload.getUrl())
                .addValue("role", payload.getRole().getListTypeId());

        return repository.saveObject("tmnc_commons", "create_new_invite", parameters, source);
    }

    public Invite get(BigInteger objectId) {
        return repository.getObject(objectId, converter);
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }

    public Invite getByURL(String url) {
        return repository.executeQuery(
                GET_INVITE_BY_URL,
                new MapSqlParameterSource("url", url),
                converter);
    }
}