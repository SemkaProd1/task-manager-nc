package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.core.ncl.NCLUpdateChangeWatcher;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.data.converters.NCLToLabelConverter;
import com.tmnc.data.entities.Label;
import com.tmnc.data.exchange.label.CreateNewLabelRequestPayload;
import com.tmnc.data.exchange.label.UpdateLabelRequestPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.Types;
import java.util.*;

@Repository
public class LabelRepository {

    private final NCLToLabelConverter converter;
    private final NCLObjectRepository repository;
    private final NCLUpdateChangeWatcher changeWatcher;

    @Autowired
    public LabelRepository(NCLToLabelConverter converter, NCLObjectRepository repository, NCLUpdateChangeWatcher changeWatcher) {
        this.converter = converter;
        this.repository = repository;
        this.changeWatcher = changeWatcher;
    }

    public BigInteger create(CreateNewLabelRequestPayload payload) {
        SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("project_id", Types.NUMERIC),
                new SqlParameter("name", Types.VARCHAR),
                new SqlParameter("color", Types.VARCHAR),
                new SqlParameter("label_description", Types.VARCHAR),
        };
        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("project_id", payload.getProjectId())
                .addValue("name", payload.getName())
                .addValue("color", payload.getColor())
                .addValue("label_description", payload.getDescription());

        return repository.saveObject("tmnc_commons", "create_new_label", parameters, source);
    }

    public Label get(BigInteger objectId) {
        return repository.getObject(objectId, converter);
    }

    public List<Label> getLabels(List<BigInteger> labelsIDs) {
        return this.repository.getListEntities(labelsIDs, converter);
    }

    public BigInteger update(UpdateLabelRequestPayload payload) {
        Label actual = get(payload.getObjectId());

        Map<String, BigInteger> attributes = new HashMap<>();
        attributes.put("name", BigInteger.valueOf(-12));
        attributes.put("description", BigInteger.valueOf(34));
        attributes.put("color", BigInteger.valueOf(33));

        List<NCLAttributeRecord> attributeRecords = changeWatcher.compareAttributes(actual, payload, attributes);
        return repository.update(actual.getObjectId(), payload.getProjectId(), attributeRecords, null);
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }
}