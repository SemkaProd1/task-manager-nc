package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.core.ncl.NCLUpdateChangeWatcher;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.core.ncl.update.NCLReferenceRecord;
import com.tmnc.data.converters.NCLToLabelConverter;
import com.tmnc.data.converters.NCLToLiteTaskConverter;
import com.tmnc.data.converters.NCLToTaskConverter;
import com.tmnc.data.converters.NCLToUserConverter;
import com.tmnc.data.entities.Task;
import com.tmnc.data.exchange.documentation.CreateNewDocumentationRequestPayload;
import com.tmnc.data.exchange.sprint.SprintTasksResponsePayload;
import com.tmnc.data.exchange.task.CreateNewTaskRequestPayload;
import com.tmnc.data.exchange.task.TaskToAssigneeResponsePayload;
import com.tmnc.data.exchange.task.UpdateTaskRequestPayload;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.services.DocumentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Types;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Repository
public class TaskRepository {

    private final NCLToTaskConverter converter;
    private final NCLObjectRepository repository;
    private final NCLUpdateChangeWatcher changeWatcher;
    private final LabelRepository labelRepository;
    private final NCLToUserConverter userConverter;
    private final NCLToLabelConverter labelConverter;
    private final DocumentationService documentationService;

    @Autowired
    public TaskRepository(NCLToTaskConverter converter, NCLObjectRepository repository,
                          NCLUpdateChangeWatcher changeWatcher,
                          LabelRepository labelRepository, NCLToUserConverter userConverter,
                          NCLToLabelConverter labelConverter, DocumentationService documentationService) {
        this.converter = converter;
        this.repository = repository;
        this.changeWatcher = changeWatcher;
        this.labelRepository = labelRepository;
        this.userConverter = userConverter;
        this.labelConverter = labelConverter;
        this.documentationService = documentationService;
    }

    public BigInteger create(CreateNewTaskRequestPayload payload) {
        SqlParameter[] parameters = {
                new SqlParameter("sprint_id", Types.NUMERIC),
                new SqlParameter("assignee_id", Types.NUMERIC),
                new SqlParameter("type", Types.NUMERIC),
                new SqlParameter("task_status", Types.NUMERIC),
                new SqlParameter("task_creator", Types.NUMERIC),
                new SqlParameter("deadline", Types.DATE),
                new SqlParameter("name", Types.VARCHAR),
                new SqlParameter("description", Types.VARCHAR),
                new SqlParameter("priority", Types.NUMERIC)
        };
        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("sprint_id", payload.getSprintId())
                .addValue("assignee_id", payload.getAssigneeParticipantId())
                .addValue("type", payload.getType().getListTypeId())
                .addValue("task_status", payload.getStatus().getListTypeId())
                .addValue("task_creator", payload.getCreatorParticipantId())
                .addValue("deadline", payload.getDeadline())
                .addValue("name", payload.getName())
                .addValue("description", payload.getDescription());
        if (payload.getPriority() != null) {
            source.addValue("priority", payload.getPriority().getListTypeId());
        } else {
            source.addValue("priority", BigInteger.valueOf(24));
        }

        BigInteger newTask = repository.saveObject("tmnc_commons", "create_new_task", parameters, source);

        List<BigInteger> labels = payload.getLabels();
        if (labels != null) {
            for (BigInteger label : labels) {
                addLabelToTask(newTask, label);
            }
        }

        List<MultipartFile> files = payload.getDocuments();
        if (files != null) {
            for (MultipartFile file : files) {
                addDocumentToTask(newTask, this.documentationService.create(file, getProject(newTask), payload.getCreatorParticipantId(),
                        Date.valueOf(LocalDate.now()), "task" + newTask.toString()));
            }
        }

        return newTask;
    }

    public BigInteger getProject(BigInteger taskId) {
        return repository.getFirstParent(repository.getFirstParent(taskId));
    }

    public boolean addLabelToTask(BigInteger taskId, BigInteger labelId) {
        return this.repository.updateReference(true, BigInteger.valueOf(26 /*Label Attr ID*/), taskId, labelId);
    }

    public boolean addDocumentToTask(BigInteger taskId, BigInteger documentId) {
        this.repository.update(documentId, taskId, null, null);
        return true;
    }

    public boolean addAssigneeToTask(BigInteger taskId, BigInteger participantId) {
        return this.repository.updateReference(true, BigInteger.valueOf(20 /*Assignee Attr ID*/), taskId, participantId);
    }

    public boolean unbindLabelFromTask(BigInteger taskId, BigInteger labelId) {
        return this.repository.updateReference(false, BigInteger.valueOf(26 /*Label Attr ID*/), taskId, labelId);
    }

    public boolean unbindAssigneeFromTask(BigInteger taskId, BigInteger participantId) {
        return this.repository.updateReference(false, BigInteger.valueOf(20 /*Assignee Attr ID*/), taskId, participantId);
    }

    public boolean unbindDocumentFromTask(BigInteger documentId) {
        this.repository.deleteObject(documentId);
        return true;
    }

    public List<SprintTasksResponsePayload> getTasksInSprint(List<BigInteger> tasksIDs) {
        NCLToLiteTaskConverter liteTaskConverter = new NCLToLiteTaskConverter();
        List<BigInteger> attrs = new ArrayList<>(Arrays.asList(BigInteger.valueOf(20 /*assignee Attr ID*/),
                BigInteger.valueOf(-12 /*name Attr ID*/), BigInteger.valueOf(25 /*deadline Attr ID*/),
                BigInteger.valueOf(22 /*task_status Attr ID*/), BigInteger.valueOf(21 /*type Attr ID*/),
                BigInteger.valueOf(24 /*priority Attr ID*/)));
        List<NCLObject> objects = new ArrayList<>(this.repository.selectObjects(tasksIDs, attrs));
        List<SprintTasksResponsePayload> tasksInSprint = new ArrayList<>();


        for (NCLObject object : objects) {
            SprintTasksResponsePayload task = liteTaskConverter.convert(object);
            if(task.getAssignee() != null) {
                BigInteger userId = repository.getFirstParent(task.getAssignee());
                task.setAssigneeUsername(repository.getObject(userId, userConverter).getUserName());
                List<BigInteger> labelsInTaskIDs = getLabelsInTaskIDs(task.getTaskId());

                if (labelsInTaskIDs != null && !labelsInTaskIDs.isEmpty()) {
                    task.setLabels(labelRepository.getLabels(labelsInTaskIDs));
                }

                tasksInSprint.add(task);
            }
        }
        return tasksInSprint;
    }

    public boolean setTaskStatus(BigInteger objectId, TaskStatus status) {
        List<NCLAttributeRecord> attributes = new ArrayList<>();
        attributes.add(new NCLAttributeRecord(BigInteger.valueOf(22), null, null, status));
        return repository.update(objectId, BigInteger.ZERO, attributes, null).equals(BigInteger.valueOf(2));
    }

    public List<BigInteger> getLabelsInTaskIDs(BigInteger taskId) {
        return get(taskId).getLabels();
    }

    public Task get(BigInteger objectId) {
        return repository.getObject(objectId, converter);
    }

    public BigInteger update(UpdateTaskRequestPayload payload) {
        Task actual = get(payload.getObjectId());

        Map<String, BigInteger> attributes = new HashMap<>();
        attributes.put("name", BigInteger.valueOf(-12));
        attributes.put("description", BigInteger.valueOf(28));
        attributes.put("type", BigInteger.valueOf(21));
        attributes.put("status", BigInteger.valueOf(22));
        attributes.put("priority", BigInteger.valueOf(24));
        attributes.put("deadline", BigInteger.valueOf(25));

        Map<String, BigInteger> references = new HashMap<>();
        references.put("creatorParticipantId", BigInteger.valueOf(23));
        references.put("assigneeParticipantId", BigInteger.valueOf(20));

        List<NCLAttributeRecord> attributeRecords = changeWatcher.compareAttributes(actual, payload, attributes);
        List<NCLReferenceRecord> referenceRecords = changeWatcher.compareReference(actual, payload, references);

        // unbinding all old labels from task
        actual.getLabels().forEach( labelId -> unbindLabelFromTask(actual.getObjectId(), labelId) );

        // bind all new label to task
        if(payload.getLabels() != null){
            payload.getLabels().forEach( labelId -> addLabelToTask(payload.getObjectId(), labelId));
        }


//        if (!Objects.equals(actual.getSpentTime(), payload.getSpentTime()))
//            attributeRecords.add(new NCLAttributeRecord(BigInteger.valueOf(29), null,
//                    convertToDate(payload.getSpentTime()), null));

        return repository.update(actual.getObjectId(), payload.getSprintId(), attributeRecords, referenceRecords);

    }

    public List<TaskToAssigneeResponsePayload> getTasksByAssignee(BigInteger assigneeId) {

        List<NCLObject> tasksNCLObjects = repository.getObjectsByRefference(
                BigInteger.valueOf(20/*assignee Attr ID*/), assigneeId);
        List<Task> tasks = new ArrayList<>();
        for (NCLObject tasksNCLObject : tasksNCLObjects) {
            tasks.add(converter.convert(tasksNCLObject));
        }
        List<TaskToAssigneeResponsePayload> taskToAssignee = new ArrayList<>();
        for (Task task : tasks) {
            TaskToAssigneeResponsePayload response = new TaskToAssigneeResponsePayload(task);
            response.setLabels(repository.getListEntities(task.getLabels(), labelConverter));
            response.setProjectId(repository.getFirstParent(repository.getFirstParent(response.getTaskId())));
            response.setProjectName(repository.getObjectName(response.getProjectId()));
            taskToAssignee.add(response);

        }
        return taskToAssignee;

//get listTasksIDs by assignee
        // getListEntities
    }

    private Date convertToDate(String date) {
        if (Objects.isNull(date)) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.getDefault());
        return Date.valueOf(LocalDate.parse(date, formatter));
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }


}