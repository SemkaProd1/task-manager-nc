package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.data.converters.NCLToSprintConverter;
import com.tmnc.data.converters.NCLToSprintWithStatisticsConverter;
import com.tmnc.data.entities.Sprint;
import com.tmnc.data.exchange.sprint.CreateNewSprintRequestPayload;
import com.tmnc.data.exchange.sprint.SprintTasksResponsePayload;
import com.tmnc.data.exchange.sprint.SprintWithStatistics;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Types;
import java.util.*;

@Repository
public class SprintRepository {

    private final static String GET_SPRINT_BY_ID = "SELECT * from table (tmnc_commons.GET_OBJ_INFO(:id))";

    private final NCLToSprintConverter converter;
    private final NCLObjectRepository objectRepository;
    private final NamedParameterJdbcTemplate template;
    private final ProjectService projectService;
    private final TaskRepository taskRepository;

    @Autowired
    public SprintRepository(NCLToSprintConverter converter,
                            NCLObjectRepository objectRepository,
                            NamedParameterJdbcTemplate template,
                            ProjectService projectService,
                            TaskRepository taskRepository) {
        this.converter = converter;
        this.objectRepository = objectRepository;
        this.template = template;
        this.projectService = projectService;
        this.taskRepository = taskRepository;
    }

    public BigInteger createNewSprint(CreateNewSprintRequestPayload payload) {
        SimpleJdbcCall call = new SimpleJdbcCall(template.getJdbcTemplate())
                .withCatalogName("tmnc_commons")
                .withFunctionName("create_new_sprint")
                .declareParameters(

                        new SqlParameter("current_project_id", Types.NUMERIC),
                        new SqlParameter("name", Types.VARCHAR),
                        new SqlParameter("description", Types.VARCHAR),
                        new SqlParameter("start_date", Types.DATE),
                        new SqlParameter("end_date", Types.DATE),
                        new SqlParameter("status", Types.NUMERIC))
                .withReturnValue();
        return call.executeFunction(
                BigDecimal.class,
                new MapSqlParameterSource()
                        .addValue("current_project_id", payload.getProjectId())
                        .addValue("name", payload.getName())
                        .addValue("description", payload.getDescription())
                        .addValue("start_date", payload.getStartDate())
                        .addValue("end_date", payload.getEndDate())
                        .addValue("status", payload.getStatus().getListTypeId())
        ).toBigInteger();
    }

    public Sprint getSprintById(BigInteger id) {
        return this.objectRepository.executeQuery(GET_SPRINT_BY_ID,
                new MapSqlParameterSource("id", id),
                converter);
    }

    public List<BigInteger> getTasksIDs(BigInteger sprintId) {
        Sprint current = getSprintById(sprintId);
        return new ArrayList<>(current.getTasks());
    }

    public List<Sprint> getSprintsInProject(List<BigInteger> sprintIDs) {
        return this.objectRepository.getListEntities(sprintIDs, converter);
    }

    public List<SprintWithStatistics> getSprintsWithStatisticsInProject(List<BigInteger> sprintIDs,
                                                                        NCLToSprintWithStatisticsConverter  converter) {
        return this.objectRepository.getListEntities(sprintIDs, converter);
    }

    public List<BigInteger> getSprintsIDs(BigInteger projectId) {
        return this.projectService.getProject(projectId).getSprints();
    }


    public Map<Object, BigInteger> getTaskCountsByStatus(BigInteger sprintId) {
        Map<Object, BigInteger> counts = new HashMap<>();
        List<SprintTasksResponsePayload> tasks = taskRepository.getTasksInSprint(this.getTasksIDs(sprintId));
        BigInteger all = BigInteger.ZERO;
        BigInteger completed = BigInteger.ZERO;

        for (TaskStatus status : TaskStatus.values()) {
            BigInteger value = BigInteger.valueOf(
                    tasks.stream()
                            .filter(task -> task.getStatus() == status)
                            .count()
            );
            counts.put(status, value);
            all = all.add(value);

            if (status == TaskStatus.CLOSED) {
                completed = completed.add(value);
            }
        }

        counts.put("completed", completed);
        counts.put("all", all);

        return counts;
    }
}
