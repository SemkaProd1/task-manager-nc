package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.core.ncl.NCLUpdateChangeWatcher;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.core.ncl.update.NCLReferenceRecord;
import com.tmnc.data.converters.NCLToDocumentationConverter;
import com.tmnc.data.converters.NCLToProjectDocumentationConverter;
import com.tmnc.data.entities.Documentation;
import com.tmnc.data.entities.ProjectDocumentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.nio.file.Path;
import java.sql.Date;
import java.sql.Types;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

@Repository
public class DocumentationRepository {

    private final NCLToProjectDocumentationConverter projectDocumentationConverter;
    private final NCLToDocumentationConverter converter;
    private final NCLObjectRepository repository;
    private final NCLUpdateChangeWatcher changeWatcher;

    @Autowired
    public DocumentationRepository(NCLToProjectDocumentationConverter projectDocumentationConverter,
                                   NCLToDocumentationConverter converter,
                                   NCLObjectRepository repository,
                                   NCLUpdateChangeWatcher changeWatcher) {
        this.projectDocumentationConverter = projectDocumentationConverter;
        this.converter = converter;
        this.repository = repository;
        this.changeWatcher = changeWatcher;
    }

    public BigInteger create(BigInteger projectId,
                             String name,
                             Path filePath,
                             BigInteger creatorParticipantId,
                             Date sysdate) {
        SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("parent_id", Types.NUMERIC),
                new SqlParameter("file_path", Types.VARCHAR),
                new SqlParameter("doc_creator", Types.NUMERIC),
                new SqlParameter("name", Types.VARCHAR),
                new SqlParameter("creation_time", Types.DATE)
        };
        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("parent_id", projectId)
                .addValue("file_path", filePath.toString())
                .addValue("doc_creator", creatorParticipantId)
                .addValue("name", name)
                .addValue("creation_time", sysdate);

        return repository.saveObject("tmnc_commons", "create_new_document", parameters, source);
    }

    public Documentation get(BigInteger objectId) {
        return repository.getObject(objectId, converter);
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }

    public BigInteger update(BigInteger projectId, String name, Path filePath,
                             BigInteger creatorParticipantId, Date sysdate, BigInteger documentId) {
        Documentation payload = new Documentation();
        payload.setCreationTime(sysdate);
        payload.setCreatorParticipantId(creatorParticipantId);
        payload.setFilePath(filePath.toString());
 //       payload.setParentId(projectId);
        payload.setName(name);
        payload.setObjectId(documentId);

        Documentation actual = get(payload.getObjectId());

        Map<String, BigInteger> attributes = new HashMap<>();
        attributes.put("name", BigInteger.valueOf(-12));
        attributes.put("filePath", BigInteger.valueOf(30));
        attributes.put("creationTime", BigInteger.valueOf(31));

        Map<String, BigInteger> references = new HashMap<>();
        references.put("creatorParticipantId", BigInteger.valueOf(32));

        List<NCLAttributeRecord> attributeRecords = changeWatcher.compareAttributes(actual, payload, attributes);
        List<NCLReferenceRecord> referenceRecords = changeWatcher.compareReference(actual, payload, references);
        return repository.update(actual.getObjectId(), projectId, attributeRecords, referenceRecords);
    }

    public List<Documentation> getDocuments(List<BigInteger> documents) {
        return repository.getListEntities(documents, converter);
    }

    public List<ProjectDocumentation> getProjectDocuments(List<BigInteger> documents) {
        return repository.getListEntities(documents, projectDocumentationConverter);
    }
}