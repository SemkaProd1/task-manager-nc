package com.tmnc.data.repositories;

import com.tmnc.core.ncl.*;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.data.converters.NCLToParticipantConverter;
import com.tmnc.data.converters.NCLToProjectConverter;
import com.tmnc.data.entities.*;
import com.tmnc.data.exchange.project.CreateNewProjectRequestPayload;
import com.tmnc.data.exchange.project.ProjectInfoForUserResponsePayload;
import com.tmnc.data.exchange.project.ProjectRequestPayload;
import com.tmnc.data.exchange.project.ProjectResponsePayload;
import com.tmnc.data.utils.ProjectPhase;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Repository
public class ProjectRepository {
    private final static String GET_PROJECT_BY_ID = "SELECT * from table (tmnc_commons.GET_OBJ_INFO(:id))";
    private final NCLToProjectConverter converter;
    private final NCLObjectConvertExecutor executor;
    private final NCLToParticipantConverter participantConverter;
	private final NCLFromMapConverter nclFromMapConverter;
    private final NCLObjectRepository nclObjectRepository;
    private final NamedParameterJdbcTemplate template;

    @Autowired
    public ProjectRepository(NCLObjectRepository nclObjectRepository,
                             NamedParameterJdbcTemplate template,
                             NCLToProjectConverter converter,
                             NCLObjectConvertExecutor executor,
                             NCLToParticipantConverter participantConverter,
                             NCLFromMapConverter nclFromMapConverter) {
        this.nclObjectRepository = nclObjectRepository;
        this.template = template;
        this.converter = converter;
        this.executor = executor;
        this.participantConverter = participantConverter;
	    this.nclFromMapConverter = nclFromMapConverter;
    }

    @SuppressWarnings("unchecked")
    public Participant createNewProject(CreateNewProjectRequestPayload payload) {
        SimpleJdbcCall call = new SimpleJdbcCall(template.getJdbcTemplate())
                .withCatalogName("tmnc_commons")
                .withFunctionName("create_new_project")
                .declareParameters(new SqlParameter("creator_id", Types.NUMERIC),
                        new SqlParameter("project_name", Types.VARCHAR),
                        new SqlParameter("project_description", Types.VARCHAR),
                        new SqlParameter("start_date", Types.DATE),
                        new SqlParameter("end_date", Types.DATE))
                .withReturnValue();

	    List<Map<String, Object>> arrayList = call.executeFunction(
			    List.class,
			    new MapSqlParameterSource()
					    .addValue("creator_id", payload.getCreatorId())
					    .addValue("project_name", payload.getProjectName())
					    .addValue("project_description", payload.getProjectDescription())
					    .addValue("start_date", payload.getStartDate())
					    .addValue("end_date", payload.getEndDate())
	    );

	    NCLObject convert = nclFromMapConverter.convert(arrayList);

	    return executor.executeConvert(convert, participantConverter);
    }

    public Project getProjectById(BigInteger id) {
        return this.nclObjectRepository.executeQuery(GET_PROJECT_BY_ID,
                new MapSqlParameterSource("id", id),
                converter);
    }

    public List<BigInteger> getLabelsIDs(BigInteger projectId) {
       return getProjectById(projectId).getLabels();
    }

    public List<BigInteger> getDocumentIDs(BigInteger projectId) {
        return getProjectById(projectId).getDocuments();
    }

    public List<BigInteger> getParticipantsIDs(BigInteger projectId) {
        return getProjectById(projectId).getParticipants();
    }
    public BigInteger updateProject(BigInteger projectId,
                                    String name,
                                    String description,
                                    ProjectPhase projectPhase,
                                    Path projectLogo,
                                    Date startDate,
                                    Date endDate) {
        Project actualProject = this.getProjectById(projectId);
        List<NCLAttributeRecord> attributes = new ArrayList<>();

        if (!actualProject.getName().equals(name)) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(-12 /*name Attr ID*/ ), name, null, null));
        }

        if (!actualProject.getDescription().equals(description)) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(1 /*description Attr ID*/), description, null, null));
        }

        if (actualProject.getPhase() != projectPhase) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(4 /*phase Attr ID*/), null, null, projectPhase));
        }

        String projectLogoInString = projectLogo == null ? actualProject.getLogo() : projectLogo.toString();

        if (actualProject.getLogo() == null || !actualProject.getLogo().equals(projectLogoInString)) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(2 /*logo Attr ID*/), projectLogoInString, null, null));
        }

        if (!actualProject.getStartDate().equals(startDate)) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(6 /*startDate Attr ID*/), null, startDate, null));
        }

        if (!actualProject.getEndDate().equals(endDate)) {
            attributes.add(new NCLAttributeRecord(BigInteger.valueOf(7 /*endDate Attr ID*/), null, endDate, null));
        }

        return this.nclObjectRepository.update(projectId, BigInteger.ZERO, attributes, null);
    }

    public BigInteger deleteProject(BigInteger projectId) {
        SimpleJdbcCall call = new SimpleJdbcCall(template.getJdbcTemplate())
                .withCatalogName("tmnc_commons")
                .withFunctionName("delete_project")
                .declareParameters(new SqlParameter("project_id", Types.NUMERIC))
                .withReturnValue();
        return call.executeFunction(
                BigDecimal.class,
                new MapSqlParameterSource()
                        .addValue("project_id", projectId))
                .toBigInteger();
    }

}