package com.tmnc.data.repositories;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.core.ncl.NCLUpdateChangeWatcher;
import com.tmnc.core.ncl.update.NCLAttributeRecord;
import com.tmnc.core.ncl.update.NCLReferenceRecord;
import com.tmnc.data.converters.NCLToCommentConverter;
import com.tmnc.data.converters.NCLToTaskConverter;
import com.tmnc.data.entities.Comment;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.exchange.comment.CommentView;
import com.tmnc.data.exchange.comment.CreateNewCommentRequestPayload;
import com.tmnc.data.exchange.comment.UpdateCommentRequestPayload;
import com.tmnc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.sql.Types;
import java.util.*;

@Repository
public class CommentRepository {

    private final NCLToCommentConverter converter;
    private final NCLObjectRepository repository;
    private final NCLUpdateChangeWatcher changeWatcher;
    private final UserService userService;

    @Autowired
    public CommentRepository(NCLToCommentConverter converter, NCLObjectRepository repository,
                             NCLUpdateChangeWatcher changeWatcher, UserService userService) {
        this.converter = converter;
        this.repository = repository;
        this.changeWatcher = changeWatcher;
        this.userService = userService;
    }

    public BigInteger create(CreateNewCommentRequestPayload comment) {
        SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("task_id", Types.NUMERIC),
                new SqlParameter("com_creator", Types.NUMERIC),
                new SqlParameter("content", Types.VARCHAR),
        };

        MapSqlParameterSource source = new MapSqlParameterSource()
                .addValue("task_id", comment.getTaskId())
                .addValue("com_creator", comment.getCreatorParticipantId())
                .addValue("content", comment.getContent());

        return repository.saveObject("tmnc_commons", "create_new_comment", parameters, source);
    }

    public Comment get(BigInteger objectId) {
        return repository.getObject(objectId, converter);
    }

    public BigInteger delete(BigInteger objectId) {
        return repository.deleteObject(objectId);
    }

    public BigInteger update(UpdateCommentRequestPayload payload) {
        Comment actual = get(payload.getObjectId());

        Map<String, BigInteger> attributes = new HashMap<>();
        attributes.put("timeChanged", new BigInteger("35"));
        attributes.put("content", new BigInteger("37"));
        attributes.put("isEdited", new BigInteger("38"));

        Map<String, BigInteger> references = new HashMap<>();
        references.put("creatorParticipantId", new BigInteger("36"));

        List<NCLAttributeRecord> attributeRecords = changeWatcher.compareAttributes(actual, payload, attributes);
        List<NCLReferenceRecord> referenceRecords = changeWatcher.compareReference(actual, payload, references);

        return repository.update(actual.getObjectId(), payload.getTaskId(), attributeRecords, referenceRecords);
    }

    public List<Comment> getCommnents(BigInteger taskId) {
        NCLToTaskConverter taskConverter = new NCLToTaskConverter();
        NCLObject objects = this.repository.selectObject(taskId,
                Collections.singletonList(BigInteger.valueOf(-8)));
        List<Comment> list = new ArrayList<>();
        List<BigInteger> IDs = taskConverter.convert(objects).getComments();
        for (BigInteger id : IDs) {
            list.add(get(id));
        }

        return list;
    }

    public List<CommentView> getCommentsView(List<Comment> comments) {
        List<CommentView> page = new ArrayList<>();
        for (Comment comment : comments) {
            CommentView commentView = new CommentView(comment);
            BigInteger userId = repository.getFirstParent(comment.getCreatorParticipantId());
            UserAccount userAccount = userService.getUser(userId);
            commentView.setUsername(userAccount.getUserName());
            commentView.setFirstName(userAccount.getFirstName());
            commentView.setUserId(userId);

            page.add(commentView);
        }

        return page;
    }
}