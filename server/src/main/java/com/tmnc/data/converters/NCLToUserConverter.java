package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.data.utils.UserAccountRole;
import org.springframework.stereotype.Component;

@Component
public class NCLToUserConverter implements NCLToObjectConverter<UserAccount> {

    @Override
    public UserAccount convert(NCLObject object) {
        UserAccount user = new UserAccount();

        user.setObjectId(object.getObjectId());
        user.setLastName(object.getAttributes().get("LAST_NAME").getAttributeValue().getValue());
        user.setFirstName(object.getAttributes().get("FIRST_NAME").getAttributeValue().getValue());
        user.setPassword(object.getAttributes().get("PASSWORD").getAttributeValue().getValue());
        user.setEmail(object.getAttributes().get("EMAIL").getAttributeValue().getValue());
        user.setUserName(object.getAttributes().get("NAME").getAttributeValue().getValue());
        user.setAvatar(getValue(object, "AVATAR"));
        user.setRole((UserAccountRole) object.getAttributes().get("SYSTEM_ROLE").getAttributeValue().getListValue());
        user.setParticipants(getReferences(object, "PARTICIPANT"));

        return user;
    }

    @Override
    public NCLType accept() {
        return NCLType.USER_ACCOUNT;
    }
}
