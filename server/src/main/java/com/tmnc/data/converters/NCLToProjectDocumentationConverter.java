package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLObjectRepository;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Documentation;
import com.tmnc.data.entities.ProjectDocumentation;
import com.tmnc.data.entities.UserAccount;
import com.tmnc.data.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NCLToProjectDocumentationConverter implements NCLToObjectConverter<ProjectDocumentation> {

    private final NCLToDocumentationConverter converter;
    private final UserRepository repository;
    private final NCLObjectRepository nclObjectRepository;

    @Autowired
    public NCLToProjectDocumentationConverter(NCLToDocumentationConverter converter,
                                              UserRepository repository,
                                              NCLObjectRepository nclObjectRepository) {
        this.converter = converter;
        this.repository = repository;
        this.nclObjectRepository = nclObjectRepository;
    }

    @Override
    public ProjectDocumentation convert(NCLObject object) {
        Documentation convert = this.converter.convert(object);
        UserAccount userAccountById = this.repository.getUserAccountById(
                this.nclObjectRepository.getFirstParent(convert.getCreatorParticipantId()));

        ProjectDocumentation projectDocumentation = new ProjectDocumentation(convert);
        projectDocumentation.setCreatorParticipantName(userAccountById.getUserName());
        return projectDocumentation;
    }

    @Override
    public NCLType accept() {
        return NCLType.DOCUMENT;
    }
}
