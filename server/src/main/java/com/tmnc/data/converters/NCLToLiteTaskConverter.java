package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.exchange.sprint.SprintTasksResponsePayload;
import com.tmnc.data.utils.TaskPriority;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.data.utils.TaskType;


public class NCLToLiteTaskConverter implements NCLToObjectConverter<SprintTasksResponsePayload> {

    @Override
    public SprintTasksResponsePayload convert(NCLObject object) {
        SprintTasksResponsePayload payload = new SprintTasksResponsePayload();
        payload.setTaskId(object.getObjectId());
        payload.setDeadline(getDate(object, "DEADLINE"));
        payload.setName(getValue(object, "NAME"));
        payload.setStatus((TaskStatus) getListValue(object, "TASK_STATUS"));
        payload.setAssignee(getReference(object, "ASSIGNEE"));
        payload.setPriority((TaskPriority) getListValue(object, "PRIORITY"));
        payload.setType((TaskType) getListValue(object, "TYPE"));
        return payload;
    }

    @Override
    public NCLType accept() {
        return NCLType.TASK;
    }

}
