package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.exchange.participant.GetParticipantsRequestPayload;
import org.springframework.stereotype.Component;

@Component
public class NCLToParticipantsSettingsConverter implements NCLToObjectConverter<GetParticipantsRequestPayload> {


    @Override
    public GetParticipantsRequestPayload convert(NCLObject object) {
        GetParticipantsRequestPayload payload = new GetParticipantsRequestPayload();

        payload.setObjectId(object.getObjectId());
        payload.setLastName(getValue(object, "FIRST_NAME"));
        payload.setFirstName(getValue(object, "LAST_NAME"));
        payload.setUsername(getValue(object, "NAME"));
        payload.setAvatar(getValue(object, "AVATAR"));
        return payload;
    }

    @Override
    public NCLType accept() {
        return NCLType.USER_ACCOUNT;

    }


}
