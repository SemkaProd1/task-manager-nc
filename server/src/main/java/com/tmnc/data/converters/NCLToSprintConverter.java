package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Sprint;
import com.tmnc.data.utils.SprintStatus;
import org.springframework.stereotype.Component;

@Component
public class NCLToSprintConverter implements NCLToObjectConverter<Sprint> {
    @Override
    public Sprint convert(NCLObject object) {

        Sprint sprint = new Sprint();
        sprint.setObjectId(object.getObjectId());
        sprint.setProjectId(object.getParentId());
        sprint.setDescription(getValue(object, "SP_DESCRIPTION"));
        sprint.setName(getValue(object, "NAME"));
        sprint.setStartDate(getDate(object, "SP_START_DATE"));
        sprint.setEndDate(getDate(object, "SP_END_DATE"));
        sprint.setStatus((SprintStatus) getListValue(object, "SP_STATUS"));
        sprint.setTasks(getReferences(object, "TASK"));
        return sprint;
    }

    @Override
    public NCLType accept() {
        return NCLType.SPRINT;
    }
}
