package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLObjectConvertExecutor;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Sprint;
import com.tmnc.data.exchange.sprint.SprintWithStatistics;
import com.tmnc.data.repositories.SprintRepository;
import com.tmnc.services.SprintService;
import org.springframework.stereotype.Component;

@Component
public class NCLToSprintWithStatisticsConverter implements NCLToObjectConverter<SprintWithStatistics> {

    private NCLToSprintConverter converter;
    private final SprintRepository repository;
    private final NCLObjectConvertExecutor executor;

    public NCLToSprintWithStatisticsConverter(NCLToSprintConverter converter,
                                              SprintRepository repository,
                                              NCLObjectConvertExecutor executor) {
        this.converter = converter;
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public SprintWithStatistics convert(NCLObject object) {
        Sprint sprint = this.executor.executeConvert(object, converter);
        SprintWithStatistics sprintWithStatistics = new SprintWithStatistics(sprint);
        sprintWithStatistics.setStatistics(
                this.repository.getTaskCountsByStatus(sprint.getObjectId()));
        return sprintWithStatistics;
    }

    @Override
    public NCLType accept() {
        return NCLType.SPRINT;
    }
}
