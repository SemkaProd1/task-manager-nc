package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Comment;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class NCLToCommentConverter implements NCLToObjectConverter<Comment> {

    @Override
    public Comment convert(NCLObject object) {
        Comment comment = new Comment();

        comment.setObjectId(
                object.getObjectId()
        );

        comment.setTaskId(object.getParentId());
        comment.setTimeChanged(
                getDate(object, "TIME_CHANGED")
        );
        comment.setCreatorParticipantId(
                getReference(object, "COM_CREATOR")
        );
        comment.setContent(
                getValue(object, "CONTENT")
        );
        comment.setEdited(
                convertToBoolean(getValue(object, "IS_EDITED"))
        );
        return comment;
    }

    private boolean convertToBoolean(String val) {
        return Objects.equals(val, "0");
    }

    @Override
    public NCLType accept() {
        return NCLType.COMMENT;
    }
}