package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Log;
import org.springframework.stereotype.Component;

@Component
public class NCLToLogConverter implements NCLToObjectConverter<Log> {

    @Override
    public Log convert(NCLObject object) {
        Log log = new Log();

        log.setObjectId(
                object.getObjectId()
        );
        log.setCreatorParticipantId(
                getReference(object, "USER")
        );
        log.setChangeParam(
                getValue(object, "CHANGE_PARAM")
        );
        log.setNewValue(
                getValue(object, "NEW_VALUE")
        );
        log.setOldValue(
                getValue(object, "OLD_VALUE")
        );
        log.setTime(
                getDate(object, "TIME")
        );
        return log;
    }

    @Override
    public NCLType accept() {
        return NCLType.LOG;
    }
}