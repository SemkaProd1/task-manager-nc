package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Label;
import org.springframework.stereotype.Component;

@Component
public class NCLToLabelConverter implements NCLToObjectConverter<Label> {

    @Override
    public Label convert(NCLObject object) {
        Label label = new Label();

        label.setObjectId(
                object.getObjectId()
        );
        label.setName(
                getValue(object, "NAME")
        );
        label.setDescription(
                getValue(object, "LABEL_DESCRIPTION")
        );
        label.setColor(
                getValue(object, "COLOR")
        );
        return label;
    }

    @Override
    public NCLType accept() {
        return NCLType.LABEL;
    }
}