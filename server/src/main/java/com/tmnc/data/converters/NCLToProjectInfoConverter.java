package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.exchange.project.ProjectInfoForUserResponsePayload;
import com.tmnc.data.utils.ParticipantRole;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;


@Component
public class NCLToProjectInfoConverter {
    private NCLToObjectConverter converter = new NCLToTaskConverter();

    public ProjectInfoForUserResponsePayload convert(List<NCLObject> object) {
        ProjectInfoForUserResponsePayload payload = new ProjectInfoForUserResponsePayload();

        payload.setObjectId(object.get(1).getObjectId());
        payload.setParticipantRole((ParticipantRole) converter.getListValue(object.get(0), "PROJECT_ROLE"));
        payload.setProjectName(converter.getValue(object.get(1), "NAME"));

        return payload;
    }


    public NCLType accept() {
        return NCLType.USER_ACCOUNT;
    }
}
