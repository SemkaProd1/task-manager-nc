package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLAttribute;
import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Project;
import com.tmnc.data.utils.ProjectPhase;
import org.springframework.stereotype.Component;

@Component
public class NCLToProjectConverter implements NCLToObjectConverter<Project> {

    @Override
    public Project convert(NCLObject object) {
        Project project = new Project();

        project.setObjectId(object.getObjectId());
        project.setName(getValue(object, "NAME"));
        project.setDescription(getValue(object, "PR_DESCRIPTION"));
        project.setPhase((ProjectPhase) getListValue(object, "PHASE"));

        NCLAttribute logo = object.getAttributes().get("LOGO");
        String logoValue = logo == null ? null : logo.getAttributeValue().getValue();
        project.setLogo(logoValue);
        project.setCreator(getReference(object, "PR_CREATOR"));
        project.setParticipants(getReferences(object, "PARTICIPANTS"));
        project.setStartDate(getDate(object, "PR_START_DATE"));
        project.setEndDate(getDate(object, "PR_END_DATE"));
        project.setLabels(getReferences(object, "LABEL"));
        project.setDocuments(getReferences(object, "DOCUMENT"));
        project.setSprints(getReferences(object, "SPRINT"));
        project.setLogs(getReferences(object, "LOG"));
        return project;
    }

    @Override
    public NCLType accept() {
        return NCLType.PROJECT;
    }
}