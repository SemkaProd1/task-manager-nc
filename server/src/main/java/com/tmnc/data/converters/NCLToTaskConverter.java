package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Task;
import com.tmnc.data.utils.TaskPriority;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.data.utils.TaskType;
import org.springframework.stereotype.Component;

@Component
public class NCLToTaskConverter implements NCLToObjectConverter<Task> {

    @Override
    public Task convert(NCLObject object) {
        Task task = new Task();

        task.setObjectId(
                object.getObjectId()
        );
        task.setAssigneeParticipantId(
                getReference(object, "ASSIGNEE")
        );
        task.setCreatorParticipantId(
                getReference(object, "TASK_CREATOR")
        );
        task.setType(
                (TaskType) getListValue(object, "TYPE")
        );
        task.setStatus(
                (TaskStatus) getListValue(object, "TASK_STATUS")
        );
        task.setPriority(
                (TaskPriority) getListValue(object, "PRIORITY")
        );
        task.setDeadline(
                getDate(object, "DEADLINE")
        );
        task.setLabels(
                getReferences(object, "LABEL")
        );
        task.setName(
                getValue(object, "NAME")
        );
        task.setWatchers(
                getReferences(object, "WATCHER_LIST")
        );
        task.setDescription(
                getValue(object, "DESCRIPTION")
        );
        if (getDate(object, "TASK_SPENT_TIME") != null) {
            task.setSpentTime(
                    getDate(object, "TASK_SPENT_TIME").toString()
            );
        }
        task.setComments(
                getReferences(object, "COMMENT")
        );
        return task;
    }

    @Override
    public NCLType accept() {
        return NCLType.TASK;
    }
}