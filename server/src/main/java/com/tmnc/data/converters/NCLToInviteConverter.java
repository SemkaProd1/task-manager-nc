package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Invite;
import com.tmnc.data.utils.ParticipantRole;
import org.springframework.stereotype.Component;

@Component
public class NCLToInviteConverter implements NCLToObjectConverter<Invite> {

    @Override
    public Invite convert(NCLObject object) {
        Invite invite = new Invite();

        invite.setObjectId(
                object.getObjectId()
        );
        invite.setProjectId(
                object.getParentId()
        );
        invite.setEmail(
                getValue(object, "RECIPIENT_EMAIL")
        );
        invite.setUrl(
                getValue(object, "URL")
        );
        invite.setRole(
                (ParticipantRole) getListValue(object, "ROLE")
        );
        return invite;
    }

    @Override
    public NCLType accept() {
        return NCLType.INVITE;
    }
}