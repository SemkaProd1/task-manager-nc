package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Participant;
import com.tmnc.data.utils.ParticipantRole;
import org.springframework.stereotype.Component;

@Component
public class NCLToParticipantConverter implements NCLToObjectConverter<Participant> {

    @Override
    public Participant convert(NCLObject object) {
        Participant participant = new Participant();

        participant.setObjectId(
                object.getObjectId()
        );
        participant.setUserId(
                object.getParentId()
        );
        participant.setProjectId(
                getReference(object, "PROJECT_ID")
        );
        participant.setRole(
                (ParticipantRole) getListValue(object, "PROJECT_ROLE")
        );
        return participant;
    }

    @Override
    public NCLType accept() {
        return NCLType.PARTICIPANT;
    }
}