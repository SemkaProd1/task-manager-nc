package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.Documentation;
import org.springframework.stereotype.Component;

@Component
public class NCLToDocumentationConverter implements NCLToObjectConverter<Documentation> {

    @Override
    public Documentation convert(NCLObject object) {
        Documentation documentation = new Documentation();

        documentation.setObjectId(
                object.getObjectId()
        );
        documentation.setName(
                getValue(object, "NAME")
        );
        documentation.setFilePath(
                getValue(object, "FILE_PATH")
        );
        documentation.setCreationTime(
                getDate(object, "CREATION_TIME")
        );
        documentation.setCreatorParticipantId(
                getReference(object, "DOC_CREATOR")
        );
        return documentation;
    }

    @Override
    public NCLType accept() {
        return NCLType.DOCUMENT;
    }
}