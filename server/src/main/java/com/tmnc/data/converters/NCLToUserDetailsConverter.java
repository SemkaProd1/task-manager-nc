package com.tmnc.data.converters;

import com.tmnc.core.ncl.NCLObject;
import com.tmnc.core.ncl.NCLToObjectConverter;
import com.tmnc.core.ncl.NCLType;
import com.tmnc.data.entities.UserCredentials;
import com.tmnc.data.utils.UserAccountRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class NCLToUserDetailsConverter implements NCLToObjectConverter<UserDetails> {

    @Override
    public UserDetails convert(NCLObject object) {
        String username = object.getAttributes().get("NAME").getAttributeValue().getValue();
        String password = object.getAttributes().get("PASSWORD").getAttributeValue().getValue();
        String firstName = object.getAttributes().get("FIRST_NAME").getAttributeValue().getValue();
        String lastName = object.getAttributes().get("LAST_NAME").getAttributeValue().getValue();
        String email = object.getAttributes().get("EMAIL").getAttributeValue().getValue();
        UserAccountRole nclListType = (UserAccountRole) object.getAttributes()
                .get("SYSTEM_ROLE")
                .getAttributeValue()
                .getListValue();
        List<GrantedAuthority> authorities = Collections.singletonList(nclListType);

        return UserCredentials
                .fromObjectId(object.getObjectId())
                .username(username)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .authorities(authorities)
                .build();
    }

    @Override
    public NCLType accept() {
        return NCLType.USER_ACCOUNT;
    }
}
