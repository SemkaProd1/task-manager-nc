package com.tmnc.data.entities;

import java.math.BigInteger;
import java.sql.Date;

public class Comment {

    private BigInteger objectId;
    private BigInteger creatorParticipantId;             // Participant's id
    private String content;
    private Date timeChanged;
    private Boolean isEdited;
    private BigInteger taskId;

    public Comment() {
    }


    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimeChanged() {
        return timeChanged;
    }

    public void setTimeChanged(Date timeChanged) {
        this.timeChanged = timeChanged;
    }

    public Boolean getEdited() {
        return isEdited;
    }

    public void setEdited(Boolean edited) {
        isEdited = edited;
    }

    public BigInteger getTaskId() {
        return taskId;
    }

    public void setTaskId(BigInteger taskId) {
        this.taskId = taskId;
    }
}