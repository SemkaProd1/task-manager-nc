package com.tmnc.data.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.math.BigInteger;
import java.util.Collection;

public class SecurityUser extends User {

    private BigInteger securityUserId;

    public BigInteger getSecurityUserId() {
        return securityUserId;
    }

    public void setSecurityUserId(BigInteger securityUserId) {
        this.securityUserId = securityUserId;
    }

    public SecurityUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}