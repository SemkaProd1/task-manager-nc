package com.tmnc.data.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.math.BigInteger;
import java.util.*;

public class UserCredentials extends User {

    private String firstName;
    private String lastName;
    private BigInteger objectId;
    private String email;

    private UserCredentials(Builder builder) {
        super(builder.username,
                builder.password,
                builder.enabled,
                builder.accountNonExpired,
                builder.credentialsNonExpired,
                builder.accountNonLocked,
                builder.authorities
            );
        this.objectId = builder.objectId;
        this.email = builder.email;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {

        return "UserCredentials{" +
                "objectId=" + objectId +
                '}';
    }

    public static Builder fromObjectId(BigInteger objectId) {
        return new Builder(objectId);
    }

    public static class Builder {
        private BigInteger objectId;
        private String username;
        private String password;
        private String email;
        private boolean enabled;
        private boolean accountNonExpired;
        private boolean credentialsNonExpired;
        private boolean accountNonLocked;
        private String firstName;
        private String lastName;
        private Collection<GrantedAuthority> authorities;

        private Builder(BigInteger objectId) {
            this.objectId = objectId;
            this.authorities = new HashSet<>();
            this.enabled = true;
            this.accountNonExpired = true;
            this.accountNonLocked = true;
            this.credentialsNonExpired = true;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder objectId(BigInteger objectId) {
            this.objectId = objectId;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder enabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Builder accountNonExpired(boolean accountNonExpired) {
            this.accountNonExpired = accountNonExpired;
            return this;
        }

        public Builder credentialsNonExpired(boolean credentialsNonExpired) {
            this.credentialsNonExpired = credentialsNonExpired;
            return this;
        }

        public Builder accountNonLocked(boolean accountNonLocked) {
            this.accountNonLocked = accountNonLocked;
            return this;
        }

        public Builder authority(GrantedAuthority authority) {
            this.authorities.add(authority);
            return this;
        }

        public Builder authorities(Collection<GrantedAuthority> authorities) {
            this.authorities = authorities;
            return this;
        }

        public UserCredentials build() {
            return new UserCredentials(this);
        }
    }
}