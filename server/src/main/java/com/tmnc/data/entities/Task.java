package com.tmnc.data.entities;

import com.tmnc.data.utils.TaskPriority;
import com.tmnc.data.utils.TaskStatus;
import com.tmnc.data.utils.TaskType;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class Task {

    private BigInteger objectId;
    private BigInteger assigneeParticipantId;
    private BigInteger creatorParticipantId;        // Participant's id
    private TaskType type;
    private TaskStatus status;
    private TaskPriority priority;
    private Date deadline;
    private String name;
    private List<BigInteger> labels;
    private List<BigInteger> watchers;              // Participant's id
    private String description;
    private String spentTime;
    private List<BigInteger> documentation;
    public Task() {
    }

    public List<BigInteger> getComments() {
        return comments;
    }

    public void setComments(List<BigInteger> comments) {
        this.comments = comments;
    }

    private List<BigInteger> comments;

    public BigInteger getObjectId() {
        return objectId;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public BigInteger getAssigneeParticipantId() {
        return assigneeParticipantId;
    }

    public void setAssigneeParticipantId(BigInteger assigneeParticipantId) {
        this.assigneeParticipantId = assigneeParticipantId;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public List<BigInteger> getLabels() {
        return labels;
    }

    public void setLabels(List<BigInteger> labels) {
        this.labels = labels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BigInteger> getWatchers() {
        return watchers;
    }

    public void setWatchers(List<BigInteger> watchers) {
        this.watchers = watchers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpentTime() {
        return spentTime;
    }

    public void setSpentTime(String spentTime) {
        this.spentTime = spentTime;
    }

    public List<BigInteger> getDocumentation() {
        return documentation;
    }

    public void setDocumentation(List<BigInteger> documentation) {
        this.documentation = documentation;
    }
}