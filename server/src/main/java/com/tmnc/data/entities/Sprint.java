package com.tmnc.data.entities;

import com.tmnc.data.utils.SprintStatus;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class Sprint {

    private BigInteger objectId;
    private BigInteger projectId;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private SprintStatus status;

    private List<BigInteger> tasks;

    public Sprint() {
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public BigInteger getProjectId() {
        return projectId;
    }

    public void setProjectId(BigInteger projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SprintStatus getStatus() {
        return status;
    }

    public void setStatus(SprintStatus status) {
        this.status = status;
    }

    public List<BigInteger> getTasks() {
        return tasks;
    }

    public void setTasks(List<BigInteger> tasks) {
        this.tasks = tasks;
    }
}