package com.tmnc.data.entities;

import com.tmnc.data.utils.ProjectPhase;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.math.BigInteger;
import java.sql.Date;

public class ProjectInfo {
    private BigInteger objectId;
    private CreatorInfo creator;
    private String name;
    private String description;
    private ProjectPhase phase;
    private Date startDate;
    private Date endDate;
    private StreamingResponseBody logo;

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public CreatorInfo getCreator() {
        return creator;
    }

    public void setCreator(CreatorInfo creator) {
        this.creator = creator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectPhase getPhase() {
        return phase;
    }

    public void setPhase(ProjectPhase phase) {
        this.phase = phase;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public StreamingResponseBody getLogo() {
        return logo;
    }

    public void setLogo(StreamingResponseBody logo) {
        this.logo = logo;
    }
}
