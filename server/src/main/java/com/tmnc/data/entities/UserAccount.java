package com.tmnc.data.entities;

import com.tmnc.data.utils.UserAccountRole;

import java.math.BigInteger;
import java.util.List;

public class UserAccount {

	private BigInteger objectId;
	private String lastName;
	private String firstName;
	private String password;
	private String email;
	private String userName;
	private String avatar;
	private UserAccountRole role;
	private List<BigInteger> participants;

	public UserAccount() {
	}

	public BigInteger getObjectId() {
		return objectId;
	}

	public void setObjectId(BigInteger objectId) {
		this.objectId = objectId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAvatar() {

		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public UserAccountRole getRole() {
		return role;
	}

	public void setRole(UserAccountRole role) {
		this.role = role;
	}

	public List<BigInteger> getParticipants() {
		return participants;
	}

	public void setParticipants(List<BigInteger> participants) {
		this.participants = participants;
	}

	@Override
	public String toString() {
		return "UserAccount{" +
				"objectId=" + objectId +
				", lastName='" + lastName + '\'' +
				", firstName='" + firstName + '\'' +
				", password='" + password + '\'' +
				", email='" + email + '\'' +
				", username='" + userName + '\'' +
				", avatar='" + avatar + '\'' +
				'}';
	}
}