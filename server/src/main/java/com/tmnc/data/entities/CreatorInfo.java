package com.tmnc.data.entities;

import java.math.BigInteger;

public class CreatorInfo {
    private BigInteger creatorId;
    private String userName;

    public CreatorInfo(BigInteger creatorId, String userName) {
        this.creatorId = creatorId;
        this.userName = userName;
    }

    public BigInteger getCreatorId() {
        return creatorId;
    }

    public String getUserName() {
        return userName;
    }
}
