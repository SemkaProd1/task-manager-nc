package com.tmnc.data.entities;

import com.tmnc.data.utils.ProjectPhase;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public class Project {

    private BigInteger objectId;
    private BigInteger creator;
    private String name;
    private String description;
    private String logo;
    private ProjectPhase phase;
    private Date startDate;
    private Date endDate;
    private List<BigInteger> participants;
    private List<BigInteger> sprints;
    private List<BigInteger> logs;
    private List<BigInteger> labels;
    private List<BigInteger> documents;

    public Project() {
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ProjectPhase getPhase() {
        return phase;
    }

    public void setPhase(ProjectPhase phase) {
        this.phase = phase;
    }

    public BigInteger getCreator() {
        return creator;
    }

    public void setCreator(BigInteger creator) {
        this.creator = creator;
    }

    public List<BigInteger> getParticipants() {
        return participants;
    }

    public void setParticipants(List<BigInteger> participants) {
        this.participants = participants;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<BigInteger> getSprints() {
        return sprints;
    }

    public void setSprints(List<BigInteger> sprints) {
        this.sprints = sprints;
    }

    public List<BigInteger> getLogs() {
        return logs;
    }

    public void setLogs(List<BigInteger> logs) {
        this.logs = logs;
    }

    public List<BigInteger> getLabels() {
        return labels;
    }

    public void setLabels(List<BigInteger> labels) {
        this.labels = labels;
    }

    public List<BigInteger> getDocuments() {
        return documents;
    }

    public void setDocuments(List<BigInteger> documents) {
        this.documents = documents;
    }
}