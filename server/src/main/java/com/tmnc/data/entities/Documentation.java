package com.tmnc.data.entities;

import java.math.BigInteger;
import java.sql.Date;

public class Documentation {

    private BigInteger objectId;
    private BigInteger creatorParticipantId;
    private String name;
    private String filePath;
    private Date creationTime;

    public Documentation() {
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }
}