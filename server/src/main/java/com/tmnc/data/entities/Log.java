package com.tmnc.data.entities;

import java.math.BigInteger;
import java.sql.Date;

public class Log {

    private BigInteger objectId;
    private BigInteger creatorParticipantId;             // Participant's id
    private Object changeParam;
    private String newValue;
    private String oldValue;
    private Date time;

    public Log() {
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Object getChangeParam() {
        return changeParam;
    }

    public void setChangeParam(Object changeParam) {
        this.changeParam = changeParam;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }
}