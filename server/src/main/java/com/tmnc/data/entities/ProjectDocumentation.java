package com.tmnc.data.entities;

import java.math.BigInteger;
import java.sql.Date;

public class ProjectDocumentation {
    private String creatorParticipantName;
    private BigInteger objectId;
    private BigInteger creatorParticipantId;
    private String name;
    private Date creationTime;

    public ProjectDocumentation(Documentation documentation) {
        this.name = documentation.getName();
        this.creationTime = documentation.getCreationTime();
        this.creatorParticipantId = documentation.getCreatorParticipantId();
        this.objectId = documentation.getObjectId();
    }

    public String getCreatorParticipantName() {
        return creatorParticipantName;
    }

    public void setCreatorParticipantName(String creatorParticipantName) {
        this.creatorParticipantName = creatorParticipantName;
    }

    public BigInteger getObjectId() {
        return objectId;
    }

    public void setObjectId(BigInteger objectId) {
        this.objectId = objectId;
    }

    public BigInteger getCreatorParticipantId() {
        return creatorParticipantId;
    }

    public void setCreatorParticipantId(BigInteger creatorParticipantId) {
        this.creatorParticipantId = creatorParticipantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
