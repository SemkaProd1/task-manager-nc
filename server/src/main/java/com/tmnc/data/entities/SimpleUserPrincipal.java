package com.tmnc.data.entities;

import java.math.BigInteger;

public class SimpleUserPrincipal {
    private String firstName;
    private String lastName;
    private BigInteger userId;
    private String email;
    private String username;

    public SimpleUserPrincipal(UserCredentials credentials) {
        this.firstName = credentials.getFirstName();
        this.lastName = credentials.getLastName();
        this.userId = credentials.getObjectId();
        this.email = credentials.getEmail();
        this.username = credentials.getUsername();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public BigInteger getObjectId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }
}
