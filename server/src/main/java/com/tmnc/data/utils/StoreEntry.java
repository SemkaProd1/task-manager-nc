package com.tmnc.data.utils;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

/**
 * Class, that must be constructed for storing files to the storage.
 * Used as parameter type of {@link com.tmnc.services.FileUploadStorageService#storeFiles(StoreEntry...)
 * FileUploadStorageService.storeFiles} for determine, where and what need to store.
 * Can be instantiated via {@link com.tmnc.services.FileUploadStorageService#createStoreEntry(MultipartFile, Path, Object...)
 * FileUploadStorageService.createStoreEntry}
 *
 * @see com.tmnc.services.FileUploadStorageService#createStoreEntry(MultipartFile, Path, Object...)
 */
public class StoreEntry {
    private Path absoluteLocation;
    private MultipartFile content;

    public StoreEntry(Builder builder) {
        this.absoluteLocation = builder.absoluteLocation;
        this.content = builder.content;
    }

    public Path getAbsoluteLocation() {
        return absoluteLocation;
    }

    public MultipartFile getContent() {
        return content;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Path absoluteLocation;
        private MultipartFile content;

        public Builder absoluteLocation(Path absoluteLocation) {
            this.absoluteLocation = absoluteLocation;
            return this;
        }

        public Builder content(MultipartFile content) {
            this.content = content;
            return this;
        }

        public StoreEntry build() {
            return new StoreEntry(this);
        }
    }
}
