package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;

import java.math.BigInteger;

public enum TaskPriority implements NCLListType {

    BLOCKER(22, "Blocker"),
    CRITICAL(23, "Critical"),
    NORMAL(24, "Normal"),
    LOW(25, "Low"),
    MAJOR(26, "Major");

    private final BigInteger listId;

    private final String listName;

    TaskPriority(int listId, String listName) {
        this.listId = BigInteger.valueOf(listId);
        this.listName = listName;
    }

    @Override
    public BigInteger getListTypeId() {
        return listId;
    }

    @Override
    public String getListTypeValue() {
        return listName;
    }
}
