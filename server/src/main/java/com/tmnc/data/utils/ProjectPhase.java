package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;

import java.math.BigInteger;

public enum ProjectPhase implements NCLListType {
    DEVELOPMENT(1, "Development"),

    TESTING(2, "Testing"),

    BUGFIXING(3, "Bug fixing");

    private final BigInteger listId;

    private final String listName;

    ProjectPhase(int listId, String listName) {
        this.listId = BigInteger.valueOf(listId);
        this.listName = listName;
    }

    @Override
    public BigInteger getListTypeId() {
        return listId;
    }

    @Override
    public String getListTypeValue() {
        return listName;
    }
}
