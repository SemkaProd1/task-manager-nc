package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;
import org.springframework.security.core.GrantedAuthority;

import java.math.BigInteger;

public enum UserAccountRole implements NCLListType, GrantedAuthority {

    USER(4, "User"),

    SYSTEM_ADMINISTRATOR(5, "System Administrator");

    private final BigInteger listId;

    private final String listName;

    UserAccountRole(int listId, String listName) {
        this.listId = BigInteger.valueOf(listId);
        this.listName = listName;
    }

    @Override
    public BigInteger getListTypeId() {
        return listId;
    }

    @Override
    public String getListTypeValue() {
        return listName;
    }

    @Override
    public String getAuthority() {
        return listName;
    }
}


