package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;

import java.math.BigInteger;

public enum SprintStatus implements NCLListType {
    ACTIVE(8, "Active"),
    READY(9, "Ready"),
    COMPLETED(10, "Completed");

    private final BigInteger listId;

    private final String listName;

    SprintStatus(int listId, String listName) {
        this.listId = BigInteger.valueOf(listId);
        this.listName = listName;
    }

    @Override
    public BigInteger getListTypeId() {
        return listId;
    }

    @Override
    public String getListTypeValue() {
        return listName;
    }
}
