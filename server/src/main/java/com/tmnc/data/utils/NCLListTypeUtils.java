package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class NCLListTypeUtils {
	private static final NCLListTypeUtils INSTANCE = new NCLListTypeUtils();

	private Set<Class<? extends NCLListType>> nclListTypes = new HashSet<>();

	private NCLListTypeUtils() {
		nclListTypes.add(ParticipantRole.class);
		nclListTypes.add(ProjectPhase.class);
		nclListTypes.add(SprintStatus.class);
		nclListTypes.add(TaskPriority.class);
		nclListTypes.add(TaskStatus.class);
		nclListTypes.add(TaskType.class);
		nclListTypes.add(UserAccountRole.class);
	}

	public static NCLListType resolve(int listTypeId) {
		for (Class<? extends NCLListType> nclListTypeClass: INSTANCE.nclListTypes) {
			for (NCLListType nclListType: nclListTypeClass.getEnumConstants()) {
				if (nclListType.getListTypeId().equals(BigInteger.valueOf(listTypeId))) {
					return nclListType;
				}
			}
		}

		return null;
	}
}
