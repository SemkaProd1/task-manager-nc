package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;

import java.math.BigInteger;

public enum TaskType implements NCLListType {
    TASK(11, "Task"),
    BUG(12, "Bug"),
    STORY(13, "Story"),
    EPIC(14, "Epic");

    private final BigInteger listId;

    private final String listName;

    TaskType(int listId, String listName) {
        this.listId = BigInteger.valueOf(listId);
        this.listName = listName;
    }

    @Override
    public BigInteger getListTypeId() {
        return listId;
    }

    @Override
    public String getListTypeValue() {
        return listName;
    }
}
