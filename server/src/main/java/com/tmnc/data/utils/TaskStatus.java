package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;

import java.math.BigInteger;

public enum TaskStatus implements NCLListType {
    OPEN(15, "Open"),
    IN_PROGRESS(16, "In Progress"),
    IMPLEMENTED(17, "Implemented"),
    READY_FOR_TESTING(18, "Ready For Testing"),
    REOPENED(19, "Reopened"),
    ON_HOLD(20, "On Hold"),
    CLOSED(21, "Closed");

    private final BigInteger listId;

    private final String listName;

    TaskStatus(int listId, String listName) {
        this.listId = BigInteger.valueOf(listId);
        this.listName = listName;
    }

    @Override
    public BigInteger getListTypeId() {
        return listId;
    }

    @Override
    public String getListTypeValue() {
        return listName;
    }
}
