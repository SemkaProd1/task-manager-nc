package com.tmnc.data.utils;

import com.tmnc.core.ncl.NCLListType;

import java.math.BigInteger;

public enum ParticipantRole implements NCLListType {

    USER(6, "User"),

    PROJECT_MANAGER(7, "Project Manager");

    private final BigInteger listId;

    private final String listName;

    ParticipantRole(int listId, String listName) {
        this.listId = BigInteger.valueOf(listId);
        this.listName = listName;
    }

    @Override
    public BigInteger getListTypeId() {
        return listId;
    }

    @Override
    public String getListTypeValue() {
        return listName;
    }
}
