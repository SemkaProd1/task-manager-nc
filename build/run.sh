#!/usr/bin/env bash


# Функция завершает скрипт, в случае возникновения ошибки при выполнении одной из команд скрипта.
# Вызывается в команде @execute, чтобы завершить скрипт с возвратом кода 1!
function abort() {
	COMMAND=$1

    echo "Error executing command \"${COMMAND}\", aborting!"
    exit 1
}

# Функция запускает команду, в случае возникновения ошибки завершает скрипт с кодом 1.
# Параметр $1 - строка с командой, которую надо запустить
function execute() {
	COMMAND=$1

    echo "[RUNCMD] \"$COMMAND\""
    bash -c "$COMMAND" || abort ${COMMAND}
}

# Запускает обновление Ubuntu-репозитория
function before_script() {
    execute "apt-get update"
}

# Запускает установку OpenJDK с проверкой что все необходимые команды были установлены.
function install_jdk() {
    execute "apt-get install -y default-jdk"
    execute "javac -version"
    execute "java -version"
}

# Запускает установку Apache Maven с проверкой что все необходимые команды были установлены.
function install_maven() {
    execute "apt-get install -y maven"
    execute "mvn -version"
    execute "mkdir ~/.m2 && cp ./build/configs/maven/settings.xml ~/.m2/settings.xml"
}

# Запускает установку NodeJS с проверкой что все необходимые команды были установлены.
function install_node() {
    execute "curl -sL https://deb.nodesource.com/setup_10.x | bash"
	execute "apt-get install -y nodejs"
    execute "node -v"
    execute "npm -v"
}


# Запускает установку Apache Tomcat с проверкой что все необходимые команды были установлены.
#function install_tomcat() {
#	CURRENT_DIR=$(pwd)
#
#	execute "cd /tmp"
#	execute "curl -O https://www-us.apache.org/dist/tomcat/tomcat-9/v9.0.13/bin/apache-tomcat-9.0.13.tar.gz"
#	execute "mkdir /opt/tomcat"
#	execute "tar xzvf apache-tomcat-9.0.13.tar.gz -C /opt/tomcat --strip-components=1"
#	execute "cd /opt/tomcat"
#	execute "chmod -R u+rwx /opt/tomcat"
#	execute "cd ${CURRENT_DIR}"
#
#	execute "cp ./build/configs/tomcat/tomcat-users.xml /opt/tomcat/conf/tomcat-users.xml"
#	execute "cp ./build/configs/tomcat/server.xml /opt/tomcat/conf/server.xml"
#	execute "cp ./build/configs/tomcat/context.xml /opt/tomcat/webapps/manager/META-INF/context.xml"
#	execute "cp ./build/configs/tomcat/context.xml /opt/tomcat/webapps/host-manager/META-INF/context.xml"
#	execute "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64"
#	execute "export CATALINA_PID=/opt/tomcat/temp/tomcat.pid"
#	execute "export CATALINA_HOME=/opt/tomcat"
#	execute "export CATALINA_BASE=/opt/tomcat"
#	execute "export CATALINA_OPTS='-Xms512M -Xmx1024M -server -XX:+UseParallelGC'"
#	execute "export JAVA_OPTS='-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'"
#	execute "/opt/tomcat/bin/./startup.sh"
#}

# Запускает Maven и собирает проект
function build_project() {
    execute "mvn install -Doracle.install -Dclient-build=full"
}

function main() {
    before_script
	install_jdk
	install_maven
	install_node
	build_project
}

main