drop table objtype cascade constraints;
drop table attrtype cascade constraints;
drop table lists cascade constraints;
drop table objects cascade constraints;
drop table attributes cascade constraints;
drop table objreference cascade constraints;

create table objtype
(
	object_type_id number(20)   not null enable,
	parent_id      number(20),
	name           varchar2(20) not null unique,
	constraint con_object_type_id primary key (object_type_id),
	constraint con_parent_id foreign key (parent_id) references objtype (object_type_id) on delete cascade enable
);

create table attrtype
(
	attr_id            number(20) not null,
	object_type_id     number(20) not null,
	object_type_id_ref number(20),
	name               varchar2(20),
	constraint con_attr_id primary key (attr_id),
	constraint con_attr_object_type_id foreign key (object_type_id) references objtype (object_type_id),
	constraint con_attr_object_type_id_ref foreign key (object_type_id_ref) references objtype (object_type_id)
);

create table lists
(
	attr_id       number(10) not null,
	list_value_id number(10) not null,
	value         varchar(4000),
	constraint con_list_value_id primary key (list_value_id),
	constraint con_l_attr_id foreign key (attr_id) references attrtype (attr_id) on delete cascade
);

create table objects
(
	object_id      number(20) not null,
	parent_id      number(20),
	object_type_id number(20) not null,
	name           varchar2(4000 byte),
	constraint con_objects_id primary key (object_id),
	constraint con_parents_id foreign key (parent_id) references objects (object_id) on delete cascade deferrable,
	constraint con_obj_type_id foreign key (object_type_id) references objtype (object_type_id)
);

create table attributes
(
	object_id     number(20) not null,
	attr_id       number(10) not null,
	value         varchar2(4000 byte),
	date_value    date,
	list_value_id number(10),
	constraint con_atr_list_value_id foreign key (list_value_id) references lists (list_value_id) on delete cascade,
	constraint con_atr_attr_id foreign key (attr_id) references attrtype (attr_id) on delete cascade,
	constraint con_atr_object_id foreign key (object_id) references objects (object_id) on delete cascade
);

create table objreference
(
	attr_id   number(20) not null,
	object_id number(20) not null,
	reference number(20) not null,
	constraint con_objreference_pk primary key (attr_id, reference, object_id),
	constraint con_reference foreign key (reference) references objects (object_id) on delete cascade,
	constraint con_robject_id foreign key (object_id) references objects (object_id) on delete cascade,
	constraint con_rattr_id foreign key (attr_id) references attrtype (attr_id) on delete cascade
);

insert into objtype (object_type_id, parent_id, name)
values (1, null, 'project');
insert into objtype (object_type_id, parent_id, name)
values (2, null, 'useraccount');
insert into objtype (object_type_id, parent_id, name)
values (3, null, 'participant');
insert into objtype (object_type_id, parent_id, name)
values (4, null, 'sprint');
insert into objtype (object_type_id, parent_id, name)
values (5, null, 'task');
insert into objtype (object_type_id, parent_id, name)
values (6, null, 'document');
insert into objtype (object_type_id, parent_id, name)
values (7, null, 'label');
insert into objtype (object_type_id, parent_id, name)
values (8, null, 'comment');
insert into objtype (object_type_id, parent_id, name)
values (9, null, 'log');
insert into objtype (object_type_id, parent_id, name)
values (10, null, 'invite');

--project

insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (1, 1, null, 'pr_description');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (2, 1, null, 'logo');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (3, 1, 3, 'pr_creator');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (4, 1, null, 'phase');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (5, 1, 3, 'participants');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (6, 1, null, 'pr_start_date');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (7, 1, null, 'pr_end_date');

--useraccount
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (8, 2, null, 'last_name');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (9, 2, null, 'first_name');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (10, 2, null, 'password');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (11, 2, null, 'email');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (12, 2, null, 'avatar');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (13, 2, null, 'system_role');

--participant
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (14, 3, null, 'project_id');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (15, 3, null, 'project_role');

--sprint
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (16, 4, null, 'sp_description');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (17, 4, null, 'sp_start_date');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (18, 4, null, 'sp_end_date');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (19, 4, null, 'sp_status');

--task
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (20, 5, 3, 'assignee');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (21, 5, null, 'type');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (22, 5, null, 'task_status');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (23, 5, 3, 'task_creator');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (24, 5, null, 'priority');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (25, 5, null, 'deadline');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (26, 5, 7, 'label');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (27, 5, 3, 'watcher_list');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (28, 5, null, 'description');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (29, 5, null, 'task_spent_time');

--documentation
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (30, 6, null, 'file_path');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (31, 6, null, 'creation_time');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (32, 6, 3, 'doc_creator');

--label
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (33, 7, null, 'color');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (34, 7, null, 'label_description');
--comment
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (35, 8, null, 'time_changed');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (36, 8, 3, 'com_creator');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (37, 8, null, 'content');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (38, 8, null, 'is_edited');

--log
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (39, 9, null, 'old_value');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (40, 9, null, 'new_value');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (41, 9, null, 'time');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (42, 9, null, 'change_param');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (43, 9, 3, 'user');


--invite
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (44, 10, null, 'url');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (45, 10, null, 'recipient_email');
insert into attrtype (attr_id, object_type_id, object_type_id_ref, name)
values (46, 10, null, 'role');

--some lists

--project phase
insert into lists (attr_id, list_value_id, value)
values (4, 1, 'development');
insert into lists (attr_id, list_value_id, value)
values (4, 2, 'testing');
insert into lists (attr_id, list_value_id, value)
values (4, 3, 'bugfixing');

--system role
insert into lists (attr_id, list_value_id, value)
values (13, 4, 'user');
insert into lists (attr_id, list_value_id, value)
values (13, 5, 'system administrator');
--project role
insert into lists (attr_id, list_value_id, value)
values (15, 6, 'user');
insert into lists (attr_id, list_value_id, value)
values (15, 7, 'project manager');

--sprint status
insert into lists (attr_id, list_value_id, value)
values (19, 8, 'active');
insert into lists (attr_id, list_value_id, value)
values (19, 9, 'ready');
insert into lists (attr_id, list_value_id, value)
values (19, 10, 'completed');

--task type
insert into lists (attr_id, list_value_id, value)
values (21, 11, 'task');
insert into lists (attr_id, list_value_id, value)
values (21, 12, 'bug');
insert into lists (attr_id, list_value_id, value)
values (21, 13, 'story');
insert into lists (attr_id, list_value_id, value)
values (21, 14, 'epic');

--task status
insert into lists (attr_id, list_value_id, value)
values (22, 15, 'open');
insert into lists (attr_id, list_value_id, value)
values (22, 16, 'in progress');
insert into lists (attr_id, list_value_id, value)
values (22, 17, 'implemented');
insert into lists (attr_id, list_value_id, value)
values (22, 18, 'ready for testing');
insert into lists (attr_id, list_value_id, value)
values (22, 19, 'reopened');
insert into lists (attr_id, list_value_id, value)
values (22, 20, 'on hold');
insert into lists (attr_id, list_value_id, value)
values (22, 21, 'closed');

--task priority
insert into lists (attr_id, list_value_id, value)
values (24, 22, 'blocker');
insert into lists (attr_id, list_value_id, value)
values (24, 23, 'critical');
insert into lists (attr_id, list_value_id, value)
values (24, 24, 'normal');
insert into lists (attr_id, list_value_id, value)
values (24, 25, 'low');
insert into lists (attr_id, list_value_id, value)
values (24, 26, 'major');