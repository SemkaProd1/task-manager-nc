#!/usr/bin/env bash
export USERS_CREDENTIALS_OUTPUT=$1

echo USERS_CREDENTIALS_OUTPUT=${USERS_CREDENTIALS_OUTPUT}

sqlplus -s tmnc/123456 << EOF
set echo off
set heading off

set serveroutput on;

@$(dirname $0)/run_create_model.sql
@$(dirname $0)/packages/tmnc_generate.sql
@$(dirname $0)/packages/tmnc_commons.sql
@$(dirname $0)/run_generate_data.sql

exit;
EOF