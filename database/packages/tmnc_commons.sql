drop type object_info_table;
drop type object_info_record;
drop type attr_table;
drop type attr_record;
drop type reference_table;
drop type reference_record;

create or replace type numbers as table of number;
/

create or replace type object_info_record as object
(
	attribute_id             number,
	attribute_name           varchar2(100),
	attribute_value          varchar2(4000),
	attribute_type           varchar2(100),
	attribute_object_type_id number
);
/

create or replace type object_info_table is table of object_info_record;
/

create or replace type attr_record as object
(
	attr_id         number,
	attr_value      varchar2(4000 byte),
	attr_date_value date,
	attr_list_value number(10)
);
/

create or replace type attr_table is table of attr_record;
/

create or replace type reference_record as object
(
	ref_action varchar(6),
	attr_id    number,
	ref_id     number,
	new_ref_id number,
	constructor function reference_record(
		ref_action varchar,
		attr_id number,
		ref_id number,
		new_ref_id number default null) return self as result
);
/

create or replace type body reference_record as
	constructor function reference_record(
		ref_action varchar,
		attr_id number,
		ref_id number,
		new_ref_id number default null) return self as result
		as
		new_ref_id_not_specified exception;
		pragma exception_init (new_ref_id_not_specified, -20001);
		begin
			self.ref_action := ref_action;
			self.ref_id := ref_id;
			self.attr_id := attr_id;

			if new_ref_id is null and ref_action = 'UPDATE' then
				raise_application_error(-20001, 'New ref id must be specified when ref action equals "UPDATE"');
			end if;

			self.new_ref_id := new_ref_id;
			return;
		end;
end;
/

create or replace type reference_table is table of reference_record;
/

create or replace package tmnc_commons as
	type object_info_cursor_record is record (
		attribute_id number,
		attribute_name varchar2(100),
		attribute_value varchar2(4000),
		attribute_type varchar2(100),
		attribute_object_type_id number
		);
	type object_info_cursor is ref cursor return object_info_cursor_record;

	procedure bind_ref(attribute_id number, object_id_in number, reference_id number);
	procedure bind_parent_id(object_id_in number, parent_id_in number);
	procedure unbind_ref(attribute_id number, object_id_in number, reference_id number);
	procedure unbind_parent_id(object_id_in number, parent_id_in number);
	function get_new_object_id return number;
	function get_objects_all_attrs(ids numbers) return object_info_table;
	function create_new_project(creator_id number,
	                            project_name varchar,
	                            project_description varchar,
	                            start_date date,
	                            end_date date) return tmnc_commons.object_info_cursor;
	function create_new_invite(project_id number,
	                           email varchar,
	                           url varchar,
	                           role number) return number;
	function create_new_comment(task_id number,
	                            com_creator number,
	                            content varchar) return number;
	function create_new_participant(user_id number,
	                                project_id number,
	                                role number) return number;
	function create_new_label(project_id number,
	                          name varchar,
	                          color varchar,
	                          label_description varchar) return number;
	function create_new_task(sprint_id number,
	                         assignee_id number,
	                         type number,
	                         task_status number,
	                         task_creator number,
	                         priority number,
	                         deadline date,
	                         name varchar,
	                         description varchar) return number;
	function create_new_document(parent_id number,
	                             file_path varchar,
	                             doc_creator number,
	                             name varchar) return number;
	function assign_user_to_project(project_id number, user_account_entity number) return number;
	function unassign_user_from_project(project_id number, user_account number) return number;
	function get_user_by_user_name(username_in varchar2) return object_info_table;
	function get_obj_type_by_id(actual_object_id number) return number;
	function get_obj_info(id number) return object_info_table;
	function get_obj_infos(id number, attr_ids numbers) return object_info_table;
	function get_objects(ids numbers, attr_ids numbers) return object_info_table;
	function get_obj_type_name(id number) return varchar2;
	function get_obj_parents(id number) return object_info_table;
	function get_obj_references(id number) return object_info_table;
	function delete_object(obj_id number) return number;
	function update_object(object_id_in number,
	                       parent_id_in number default 0,
	                       attrs attr_table default attr_table(),
	                       references reference_table default reference_table()) return number;
	function create_new_log(task_id number,
	                        user_id number,
	                        change_param varchar,
	                        new_value varchar,
	                        old_value varchar) return number;
	function get_obj_info_table_id(object_info object_info_table) return number;
	function is_user_exists(username varchar2) return number;
	function is_email_exists(email_in varchar2) return number;
	function get_invite_by_url(url varchar2) return object_info_table;
	function create_new_user(username varchar,
	                         last_name varchar,
	                         first_name varchar,
	                         password varchar,
	                         email varchar) return number;
	function create_new_sprint(current_project_id number,
	                           name varchar,
	                           description varchar,
	                           start_date date,
	                           end_date date,
	                           status number) return number;
	function delete_project(project_id number) return number;
	function get_parent(object_id_in number) return number;
	function random_password(v_length number) return varchar2;
	function pbkdf2(password in varchar2, salt in varchar2, iterations in integer,
	                key_length in integer) return varchar2;
	function get_email_by_participant_id(part_id number) return varchar2;
	function get_obj_id_by_ref(attr_id_in number, ref_id_in number) return object_info_table;
	function get_object_name(object_id_in number) return varchar2;

end tmnc_commons;
/

create or replace package body tmnc_commons as
	procedure bind_ref(attribute_id number, object_id_in number, reference_id number) is
	begin
		insert into objreference values (attribute_id, object_id_in, reference_id);
	end;

	procedure unbind_ref(attribute_id number, object_id_in number, reference_id number) is
	begin
		delete
		from objreference
		where attr_id = attribute_id
		  and object_id = object_id_in
		  and reference = reference_id;
	end;

	procedure bind_parent_id(object_id_in number, parent_id_in number) is
	begin
		update objects set parent_id = parent_id_in where object_id = object_id_in;
	end;

	procedure unbind_parent_id(object_id_in number, parent_id_in number) is
	begin
		update objects set parent_id = null where object_id = object_id_in and parent_id = parent_id_in;
	end;

	function assign_user_to_project(project_id number, user_account_entity number)
		return number
	as
		new_user_entity_id number;
	begin
		new_user_entity_id := tmnc_commons.get_new_object_id();

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_user_entity_id, user_account_entity, 3 /* UserEntity Obj_Type ID */,
		        new_user_entity_id || '_' || user_account_entity);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_user_entity_id, 15 /* Role Attr ID */, null, null, 7/* project manager List value ID */);

		tmnc_commons.bind_ref(14 /* project_id Attr ID */, new_user_entity_id, project_id);
		tmnc_commons.bind_ref(5 /* participants Attr ID */, project_id, new_user_entity_id);

		return sql%rowcount;
	end;

	function unassign_user_from_project(project_id number, user_account number)
		return number
	as
	begin
		delete
		from objects
		where object_id = (
			select reference
			from objreference
			where object_id = project_id
			  and attr_id = 5
			  and reference = (
				select object_id
				from objects
				where parent_id = user_account
			)
		);

		return sql%rowcount;
	end;

	function get_obj_info_table_id(object_info object_info_table)
		return number as
		id number;
		object_info_rec object_info_record;
	begin
		for i in 1..object_info.count
		loop
			object_info_rec := object_info(i);

			if object_info_rec.attribute_name = 'OBJECT_ID' then
				return to_number(object_info_rec.attribute_value);
			end if;
		end loop;
	end;

	function create_new_user(username varchar,
	                         last_name varchar,
	                         first_name varchar,
	                         password varchar,
	                         email varchar) return number as
		new_user_id number;
	begin
		new_user_id := get_new_object_id();
		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_user_id, null, 2/*User Obj_Type ID*/, username);
		insert all
			into attributes(object_id, attr_id, value, date_value, list_value_id)
		values (new_user_id, 8 /* last_name Attr Id*/, last_name, null, null)
		into attributes(object_id, attr_id, value, date_value, list_value_id)
		values (new_user_id, 9 /* first_name Attr Id*/, first_name, null, null)
		into attributes(object_id, attr_id, value, date_value, list_value_id)
		values (new_user_id, 10 /* password Attr Id*/, password, null, null)
		into attributes(object_id, attr_id, value, date_value, list_value_id)
		values (new_user_id, 11 /* email Attr Id*/, email, null, null)
		into attributes(object_id, attr_id, value, date_value, list_value_id)
		values (new_user_id, 13 /*system_role Attr Id*/, null, null, 4)
		select *
		from dual;

		return new_user_id;
	end;

	function create_new_project(creator_id number,
	                            project_name varchar,
	                            project_description varchar,
	                            start_date date,
	                            end_date date) return tmnc_commons.object_info_cursor as
		new_project_id number;
		new_user_entity_id number;
		new_participant object_info_table := object_info_table();
		result tmnc_commons.object_info_cursor;
	begin
		new_user_entity_id := tmnc_commons.get_new_object_id();
		new_project_id := new_user_entity_id + 1;

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_user_entity_id, creator_id, 3 /* UserEntity Obj_Type ID */,
		        new_user_entity_id || '_' || project_name);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_user_entity_id, 15/* project_role Attr ID */, null, null, 7 /* project_role List Attr ID */);

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_project_id, null, 1 /* Project Obj_Type ID */, project_name);

		insert all
			into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_project_id, 1 /* PR_DESCRIPTION Attr ID */, project_description, null, null)
		into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_project_id, 4 /* PHASE Attr ID */, null, null, 1)
		into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_project_id, 6 /* PR_START_DATE Attr ID */, null, start_date, null)
		into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_project_id, 7 /* PR_END_DATE Attr ID */, null, end_date, null)
		select *
		from dual;

		tmnc_commons.bind_ref(14 /* project_id Attr ID */, new_user_entity_id, new_project_id);
		tmnc_commons.bind_ref(3 /* creator Attr ID */, new_project_id, new_user_entity_id);
		tmnc_commons.bind_ref(5 /* participants Attr ID */, new_project_id, new_user_entity_id);

		new_participant.extend(6);
		new_participant(1) := object_info_record(
			0, 'OBJECT_ID', to_char(new_user_entity_id), 'ATTR', null);
		new_participant(2) := object_info_record(
			-11, 'OBJECT_TYPE_ID', to_char(3), 'ATTR', null);
		new_participant(3) := object_info_record(
			-12, 'NAME', new_user_entity_id || '_' || project_name, 'ATTR', null);
		new_participant(4) := object_info_record(
			-13, 'PARENT_ID', creator_id, 'REF', 2);
		new_participant(5) := object_info_record(
			14, 'PROJECT_ID', to_char(new_project_id), 'REF', 1);
		new_participant(6) := object_info_record(
			15, 'PROJECT_ROLE', to_char(7), 'LIST_VALUE', null);

		open result for select * from table (new_participant);
		return result;
	end;

	function get_user_by_user_name(username_in varchar2)
		return object_info_table
	as
		obj_id number;
	begin
		select object_id into obj_id from objects where object_type_id = 2 and objects.name = username_in;
		return get_obj_info(obj_id);
	end;

	function get_obj_type_by_id(actual_object_id number) return number as
		actual_type_id number := 0;
	begin
		select object_type_id into actual_type_id
		from objects
		where object_id = actual_object_id;

		return actual_type_id;
	end;

	function get_obj_info(id number) return object_info_table as
		object_info object_info_table := object_info_table();
	begin
		for object_info_row in (
			select distinct 0           as attribute_id,
			                'OBJECT_ID' as attribute_name,
			                to_char(id) as attribute_value,
			                'ATTR'      as attribute_type,
			                null        as attribute_object_type_id
			from objects
			where object_id = id
			union all
			select -11                     as attribute_id,
			       'OBJECT_TYPE_ID'        as attribute_name,
			       to_char(object_type_id) as attribute_value,
			       'ATTR'                  as attribute_type,
			       null                    as attribute_object_type_id
			from objects
			where object_id = id
			union all
			select -12    as attribute_id,
			       'NAME' as attribute_name,
			       name   as attribute_value,
			       'ATTR' as attribute_type,
			       null   as attribute_object_type_id
			from objects
			where object_id = id
			union all
			select -13                                        as attribute_id,
			       'PARENT_ID'                                as attribute_name,
			       to_char(parent_id)                         as attribute_value,
			       'REF'                                      as attribute_type,
			       tmnc_commons.get_obj_type_by_id(parent_id) as attribute_object_type_id
			from objects
			where object_id = id
			union all
			select attr.attr_id               as attribute_id,
			       upper(attribute_type.name) as attribute_name,
			       coalesce(
				       attr.value,
				       to_char(attr.date_value, 'dd-mm-yyyy'),
				       to_char(attr.list_value_id)
				       )                      as attribute_value,
			       case
				       when attr.value is not null then 'ATTR'
				       when attr.date_value is not null then 'DATE'
				       when attr.list_value_id is not null then 'LIST_VALUE' end
			                                  as attribute_type,
			       null                       as attribute_object_type_id
			from objects object,
			     attrtype attribute_type,
			     attributes attr
			where object.object_id = attr.object_id
			  and attribute_type.attr_id = attr.attr_id
			  and object.object_id = id
			union all
			select * from (select reference.attr_id                                    as attribute_id,
														upper(attribute_type.name)                           as attribute_name,
														to_char(reference.reference)                         as attribute_value,
														'REF'                                                as attribute_type,
														tmnc_commons.get_obj_type_by_id(reference.reference) as attribute_object_type_id
										 from attrtype attribute_type,
													objreference reference,
													objects object
										 where object.object_id = reference.object_id
											 and reference.attr_id = attribute_type.attr_id
											 and object.object_id = id
			  order by attribute_name)
			union all
			select * from (select -objects.object_type_id    as attribute_id,
														upper(tmnc_commons.get_obj_type_name(
																objects.object_id))    as attribute_name,
														to_char(objects.object_id) as attribute_value,
														'REF'                      as attribute_type,
														objects.object_type_id     as attribute_object_type_id
										 from objects
										 where parent_id = id
			  							order by attribute_name)
			)
		loop
			object_info.extend;
			object_info(object_info.count) := object_info_record(
				object_info_row.attribute_id,
				object_info_row.attribute_name,
				object_info_row.attribute_value,
				object_info_row.attribute_type,
				object_info_row.attribute_object_type_id);
		end loop;

		return object_info;
	end;

	function get_obj_infos(id number, attr_ids numbers) return object_info_table as
		object_info object_info_table := object_info_table();
	begin
		for object_info_row in (
			select distinct 0           as attribute_id,
			                'OBJECT_ID' as attribute_name,
			                to_char(id) as attribute_value,
			                'ATTR'      as attribute_type,
			                null        as attribute_object_type_id
			from objects
			where object_id = id
			union all
			select -11                     as attribute_id,
			       'OBJECT_TYPE_ID'        as attribute_name,
			       to_char(object_type_id) as attribute_value,
			       'ATTR'                  as attribute_type,
			       null                    as attribute_object_type_id
			from objects
			where object_id = id
			union all
			select -12    as attribute_id,
			       'NAME' as attribute_name,
			       name   as attribute_value,
			       'ATTR' as attribute_type,
			       null   as attribute_object_type_id
			from objects
			where object_id = id
			  and -12 member of attr_ids
			union all
			select -13                                        as attribute_id,
			       'PARENT_ID'                                as attribute_name,
			       to_char(parent_id)                         as attribute_value,
			       'REF'                                      as attribute_type,
			       tmnc_commons.get_obj_type_by_id(parent_id) as attribute_object_type_id
			from objects
			where object_id = id
			  and -13 member of attr_ids
			union all
			select attr.attr_id               as attribute_id,
			       upper(attribute_type.name) as attribute_name,
			       coalesce(
				       attr.value,
				       to_char(attr.date_value, 'dd-mm-yyyy'),
				       to_char(attr.list_value_id)
				       )                      as attribute_value,
			       case
				       when attr.value is not null then 'ATTR'
				       when attr.date_value is not null then 'DATE'
				       when attr.list_value_id is not null then 'LIST_VALUE' end
			                                  as attribute_type,
			       null                       as attribute_object_type_id
			from objects object,
			     attrtype attribute_type,
			     attributes attr
			where object.object_id = attr.object_id
			  and attribute_type.attr_id = attr.attr_id
			  and object.object_id = id
			  and attribute_type.attr_id member of attr_ids
			union all
			select * from (select reference.attr_id                                    as attribute_id,
														 upper(attribute_type.name)                           as attribute_name,
														 to_char(reference.reference)                         as attribute_value,
														 'REF'                                                as attribute_type,
														 tmnc_commons.get_obj_type_by_id(reference.reference) as attribute_object_type_id
											from attrtype attribute_type,
													 objreference reference,
													 objects object
											where object.object_id = reference.object_id
												and reference.attr_id = attribute_type.attr_id
												and object.object_id = id
												and reference.attr_id member of attr_ids
			  							order by attribute_name)
			union all
			select * from (select -objects.object_type_id    as attribute_id,
															upper(tmnc_commons.get_obj_type_name(
																	objects.object_id))    as attribute_name,
															to_char(objects.object_id) as attribute_value,
															'REF'                      as attribute_type,
															objects.object_type_id     as attribute_object_type_id
											 from objects
											 where parent_id = id
												 and -objects.object_type_id member of attr_ids
			  order by attribute_name)
			)
		loop
			object_info.extend;
			object_info(object_info.count) := object_info_record(
				object_info_row.attribute_id,
				object_info_row.attribute_name,
				object_info_row.attribute_value,
				object_info_row.attribute_type,
				object_info_row.attribute_object_type_id);
		end loop;

		return object_info;
	end;

	function get_objects(ids numbers, attr_ids numbers) return object_info_table as
		object_info object_info_table := object_info_table();
		object_info_buf object_info_table := object_info_table();
		b_index number := 1;
	begin
		for id_index in 1..ids.count
		loop
			object_info_buf := tmnc_commons.get_obj_infos(ids(id_index), attr_ids);
			object_info.extend(object_info_buf.count);

			for object_info_buf_index in 1..object_info_buf.count
			loop
				dbms_output.put_line(b_index);
				object_info(b_index) := object_info_buf(object_info_buf_index);
				dbms_output.put_line(object_info_buf(object_info_buf_index).attribute_id);
				b_index := b_index + 1;
			end loop;
		end loop;

		return object_info;
	end;

	function get_objects_all_attrs(ids numbers) return object_info_table
	as
		object_info object_info_table := object_info_table();
		object_info_buf object_info_table := object_info_table();
		b_index number := 1;
	begin
		for id_index in 1..ids.count
		loop
			object_info_buf := tmnc_commons.get_obj_info(ids(id_index));
			object_info.extend(object_info_buf.count);

			for object_info_buf_index in 1..object_info_buf.count
			loop
				object_info(b_index) := object_info_buf(object_info_buf_index);
				b_index := b_index + 1;
			end loop;
		end loop;
		return object_info;
	end;

	function get_new_object_id return number is
		new_object_id number;
	begin
		select nvl(max(objects.object_id) + 1, 1) into new_object_id
		from objects;
		return new_object_id;
	end;

	function get_obj_type_name(id number) return varchar2 as
		type_name varchar2(100);
	begin
		select objtype.name into type_name
		from objects,
		     objtype
		where objtype.object_type_id = objects.object_type_id
		  and objects.object_id = id;
		return type_name;
	end;

	function create_new_invite(project_id number,
	                           email varchar,
	                           url varchar,
	                           role number) return number as
		new_invite_id number;
	begin
		new_invite_id := tmnc_commons.get_new_object_id();

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_invite_id, project_id, 10 /* Invite entity Obj_Type ID */,
		        email);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_invite_id, 44 /* url Attr ID */ , url, null, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_invite_id, 45 /* email Attr ID */ , email, null, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_invite_id, 46 /* project_role Attr ID */ , null, null, role);

		return new_invite_id;
	end;

	function create_new_comment(task_id number,
	                            com_creator number,
	                            content varchar) return number as
		new_comment_id number;
	begin
		new_comment_id := tmnc_commons.get_new_object_id();

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_comment_id, task_id, 8 /* Comment entity Obj_Type ID */,
		        content);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_comment_id, 35 /* time_changed Attr ID */ , null, sysdate, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_comment_id, 37 /* content Attr ID */ , content, null, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_comment_id, 38 /* is_edited Attr ID */ , '0', null, null);

		tmnc_commons.bind_ref(36 /* com_creator Attr ID */, new_comment_id, com_creator);

		return new_comment_id;
	end;

	function create_new_participant(user_id number,
	                                project_id number,
	                                role number) return number as
		new_participant_id number;
	begin
		new_participant_id := tmnc_commons.get_new_object_id();

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_participant_id, user_id, 3 /* Participant Obj_Type ID */,
		        'participant');

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_participant_id, 15 /* project_role Attr ID */ , null, null, role);

		tmnc_commons.bind_ref(14 /* project Attr ID */, new_participant_id, project_id);
		tmnc_commons.bind_ref(5 /* participants Attr ID */, project_id, new_participant_id);

		return new_participant_id;
	end;

	function create_new_label(project_id number,
	                          name varchar,
	                          color varchar,
	                          label_description varchar) return number as
		new_object_id number;
	begin
		new_object_id := tmnc_commons.get_new_object_id();

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_object_id, project_id, 7 /* Label entity Obj_Type ID */,
		        name);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 33 /* color Attr ID */ , color, null, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 34 /* label_description Attr ID */ , label_description, null, null);

		return new_object_id;
	end;

	function create_new_log(task_id number,
	                        user_id number,
	                        change_param varchar,
	                        new_value varchar,
	                        old_value varchar) return number as
		new_object_id number;
	begin
		new_object_id := tmnc_commons.get_new_object_id();

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_object_id, task_id, 9 /* Log Obj_Type ID */,
		        'log');

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 39 /* old_value Attr ID */ , old_value, null, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 40 /* new_value Attr ID */ , new_value, null, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 41 /* time Attr ID */ , null, sysdate, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 42 /* change_param Attr ID */ , change_param, null, null);

		tmnc_commons.bind_ref(43 /* user Attr ID */, new_object_id, user_id);

		return new_object_id;
	end;

	function create_new_task(sprint_id number,
	                         assignee_id number,
	                         type number,
	                         task_status number,
	                         task_creator number,
	                         priority number,
	                         deadline date,
	                         name varchar,
	                         description varchar) return number as
		new_object_id number;
	begin
		new_object_id := tmnc_commons.get_new_object_id();

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_object_id, sprint_id, 5 /* Task Obj_Type ID */,
		        name);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 21 /* type Attr ID */ , null, null, type);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 22 /* task_status Attr ID */ , null, null, task_status);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 24 /* priority Attr ID */ , null, null, priority);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 25 /* deadline Attr ID */ , null, deadline, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_object_id, 28 /* description Attr ID */ , description, null, null);


		tmnc_commons.bind_ref(20 /* assignee Attr ID */, new_object_id, assignee_id);
		tmnc_commons.bind_ref(23 /* task_creator Attr ID */, new_object_id, task_creator);

		return new_object_id;
	end;

	function create_new_document(parent_id number,
	                             file_path varchar,
	                             doc_creator number,
	                             name varchar) return number as
		new_document_id number;
	begin
		new_document_id := tmnc_commons.get_new_object_id();

		insert into objects
			(object_id, parent_id, object_type_id, name)
		values (new_document_id, parent_id, 6 /* Document entity Obj_Type ID */,
		        name);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_document_id, 30 /* file_path Attr ID */ , file_path, null, null);

		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_document_id, 31 /* creation_time Attr ID */ , null, sysdate, null);

		tmnc_commons.bind_ref(32 /* doc_creator Attr ID */, new_document_id, doc_creator);

		return new_document_id;
	end;

	function get_obj_references(id number) return object_info_table as
		object_info object_info_table := object_info_table();
	begin
		for object_info_row in (
			select reference.attr_id                                    as attribute_id,
			       attribute_type.name                                  as attribute_name,
			       to_char(reference.reference)                         as attribute_value,
			       'REF'                                                as attribute_type,
			       tmnc_commons.get_obj_type_by_id(reference.reference) as attribute_object_type_id
			from attrtype attribute_type,
			     objreference reference,
			     objects object
			where object.object_id = reference.object_id
			  and reference.attr_id = attribute_type.attr_id
			  and object.object_id = id
			order by attribute_name
			)
		loop
			object_info.extend;
			object_info(object_info.count) := object_info_record(
				object_info_row.attribute_id,
				object_info_row.attribute_name,
				object_info_row.attribute_value,
				object_info_row.attribute_type,
				object_info_row.attribute_object_type_id);
		end loop;

		return object_info;
	end;

	function get_obj_parents(id number) return object_info_table as
		object_info object_info_table := object_info_table();
	begin
		for object_info_row in (
			select -objects.object_type_id                           as attribute_id,
			       tmnc_commons.get_obj_type_name(objects.object_id) as attribute_name,
			       to_char(objects.object_id)                        as attribute_value,
			       'REF'                                             as attribute_type,
			       objects.object_type_id                            as attribute_object_type_id
			from objects
			where parent_id = id
			order by attribute_name
			)
		loop
			object_info.extend;
			object_info(object_info.count) := object_info_record(
				object_info_row.attribute_id,
				object_info_row.attribute_name,
				object_info_row.attribute_value,
				object_info_row.attribute_type,
				object_info_row.attribute_object_type_id);
		end loop;

		return object_info;
	end;

	function delete_object(obj_id number) return number
	is
	begin
		delete objects where object_id = obj_id;
		return sql%rowcount;
	end;

	function update_object(object_id_in number,
	                       parent_id_in number default 0,
	                       attrs attr_table,
	                       references reference_table default reference_table())
		return number
	is
		attribute_count number;
		changed_attrs number := 0;
		object_type_id_in number := tmnc_commons.get_obj_type_by_id(object_id_in);
	begin
		if parent_id_in is null or parent_id_in > 0 then
			update objects
			set parent_id = parent_id_in
			where object_id = object_id_in;
		end if;

		for attribute_index in 1..attrs.count
		loop
			select count(*) into attribute_count
			from attributes
			where object_id = object_id_in
			  and attr_id = attrs(attribute_index).attr_id;

			if attrs(attribute_index).attr_id = -12 then
				update objects
				set name = attrs(attribute_index).attr_value
				where object_id = object_id_in;

				changed_attrs := changed_attrs + sql%rowcount;
			elsif attribute_count > 0 then
				update attributes
				set list_value_id = attrs(attribute_index).attr_list_value,
				    value         = attrs(attribute_index).attr_value,
				    date_value    = attrs(attribute_index).attr_date_value
				where object_id = object_id_in
				  and attr_id = attrs(attribute_index).attr_id;

				changed_attrs := changed_attrs + sql%rowcount;
			else
				insert into attributes (object_id, attr_id, value, date_value, list_value_id)
				values (object_id_in,
				        attrs(attribute_index).attr_id,
				        attrs(attribute_index).attr_value,
				        attrs(attribute_index).attr_date_value,
				        attrs(attribute_index).attr_list_value);

				changed_attrs := changed_attrs + sql%rowcount;
			end if;
		end loop;

		for reference_index in 1..references.count
		loop
			if references(reference_index).ref_action = 'INSERT' then
				tmnc_commons.bind_ref(references(reference_index).attr_id, object_id_in,
				                      references(reference_index).ref_id);
			elsif references(reference_index).ref_action = 'DELETE' then
				tmnc_commons.unbind_ref(references(reference_index).attr_id, object_id_in,
				                        references(reference_index).ref_id);
			elsif references(reference_index).ref_action = 'UPDATE' then
				tmnc_commons.unbind_ref(references(reference_index).attr_id, object_id_in,
				                        references(reference_index).ref_id);
				tmnc_commons.bind_ref(references(reference_index).attr_id, object_id_in,
				                      references(reference_index).new_ref_id);
			end if;
			changed_attrs := changed_attrs + 1;
		end loop;

		changed_attrs := changed_attrs + sql%rowcount;

		return changed_attrs;
	end;

	function is_user_exists(username varchar2)
		return number
	is
		count_of_request number;
	begin
		select count(*) into count_of_request from objects where object_type_id = 2 and objects.name = username;
		return count_of_request;
	end;

	function is_email_exists(email_in varchar2)
		return number
	is
		count_of_request number;
	begin
		select count(*) into count_of_request
		from attributes
		where attr_id = 11 /* email Attr ID */and value = email_in;
		return count_of_request;
	end;

	function get_invite_by_url(url varchar2)
		return object_info_table
	as
		obj_id number;
	begin
		select object_id into obj_id from attributes where attr_id = 44 /* url Attr ID */ and value = url;
		return get_obj_info(obj_id);
	end;

	function create_new_sprint(current_project_id number,
	                           name varchar,
	                           description varchar,
	                           start_date date,
	                           end_date date,
	                           status number) return number as
		new_sprint_id number;
	begin
		new_sprint_id := tmnc_commons.get_new_object_id();

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_sprint_id, current_project_id, 4 /* Sprint Obj_Type ID */,
		        name);
		insert all
			into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint_id, 16 /* sp_description Attr ID */, description, null, null)
		into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint_id, 17 /* sp_start_date Attr ID */, null, start_date, null)
		into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint_id, 18 /* sp_end_date Attr ID */, null, end_date, null)
		into attributes
			(object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint_id, 19 /* sp_status Attr ID */, null, null, status)
		select *
		from dual;
		return new_sprint_id;
	end;

	function delete_project(project_id number) return number
	is
		object_type number;
	begin
		--maybe need to add 'if project_id exists block'
		select object_type_id into object_type from objects where object_id = project_id;
		if object_type = 1 /* project Object Type ID */ then
			delete objects where object_id in (select reference from objreference where object_id = project_id);
			return delete_object(project_id);
		else
			return 0;
		end if;
	end;

	function get_parent(object_id_in number) return number as
		out_parent_id number := 0;
	begin
		select parent_id into out_parent_id from objects where object_id = object_id_in;
		return out_parent_id;
	end;

	function random_password(v_length number) return varchar2 is
		type char_array is table of char;

		result varchar2(4000);
		random_string varchar(4);
		random_number number;
		chars char_array := char_array(
			'#', '$', '!',
			'%', '&', '(', ')', ':',
			'@', '+', ',', '-', '.',
			'<', '=', '>', '?', '[',
			']', '^', '_', '`', '{',
			'}', '~', '*', '/', '|');
	begin
		for i in 1..v_length
		loop
			random_number := dbms_random.value(0, 1);

			result := result || case
				                    when random_number <= 0.33 then
					                    to_char(trunc(dbms_random.value(0, 9)))
				                    when random_number > 0.33 and random_number <= 0.66 then
					                    chars(trunc(dbms_random.value(1, chars.count)))
				                    else
					                    dbms_random.string('a', 1)
				end;
		end loop;
		return result;
	end;

	function pbkdf2(password in varchar2, salt in varchar2, iterations in integer, key_length in integer)
		return varchar2
	is
		block_count integer;

		last raw(32767);
		xorsum raw(32767);
		result raw(32767);
	begin
		block_count := ceil(key_length / 20); -- 20 bytes for sha1.

		for i in 1..block_count
		loop
			last := utl_raw.concat(
				utl_raw.cast_to_raw(salt), utl_raw.cast_from_binary_integer(i, utl_raw.big_endian));

			xorsum := null;

			for j in 1..iterations
			loop
				last := dbms_crypto.mac(last, dbms_crypto.hmac_sh1, utl_raw.cast_to_raw(password));

				if xorsum is null
				then
					xorsum := last;
				else
					xorsum := utl_raw.bit_xor(xorsum, last);
				end if;
			end loop;

			result := utl_raw.concat(result, xorsum);
		end loop;

		return rawtohex(utl_raw.substr(result, 1, key_length));
	end;
	function get_email_by_participant_id(part_id number) return varchar2
	as
		email varchar2(40);
		user_id number;
	begin
		select parent_id into user_id from objects where object_id = part_id;
		select value into email from attributes where object_id = user_id and attr_id = 11; /*email Attr ID*/
		return email;
	end;

	function get_obj_id_by_ref(attr_id_in number, ref_id_in number) return object_info_table
	as
		array numbers;
	begin
		select object_id bulk collect into array
		from objreference
		where attr_id = attr_id_in
		  and reference = ref_id_in;

		return tmnc_commons.get_objects_all_attrs(array);
	end;

	function get_object_name(object_id_in number) return varchar2
	as
		name_out varchar(50);
	begin
		select name into name_out from objects where object_id = object_id_in;

		return name_out;
	end;

end tmnc_commons;
/