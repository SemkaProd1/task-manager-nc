create or replace package tmnc_generate as
	procedure add_ref_to_object_by_attr(reference_id number, object_id number, attr_id number);
	procedure update_parent(object_id_in number, parent_id_in number);
	function generate_new_project return number;
	function generate_new_user_account(file_handler sys.utl_file.file_type) return number;
	function generate_new_participant return number;
	function generate_new_sprint return number;
	function generate_new_task return number;
	function generate_new_documentation return number;
	function generate_new_label return number;
	function generate_new_comment return number;
	function generate_new_log return number;
	function generate_new_invite return number;
	procedure generate_data(projects_count number);
end;
/

create or replace package body tmnc_generate as
	--add references into table OBJREFERENCE
	procedure add_ref_to_object_by_attr(reference_id number, object_id number, attr_id number)
	is
	begin
		insert into objreference values (attr_id, object_id, reference_id);
	end;

	--add parent_id into table OBJECTS
	procedure update_parent(object_id_in number, parent_id_in number)
	is
	begin
		update objects set parent_id = parent_id_in where object_id = object_id_in;
	end;
	--FUNCTIONS

	/*
	Insert new Project in OBJECTS with unique params in ATTRIBUTES
	*/
	function generate_new_project
		return number
	is
		project_name_buffer varchar(50);
		project_start_date_buffer timestamp;
		project_end_date_buffer timestamp;
		new_project /*next OBJECT_ID*/ number;
	begin
		project_start_date_buffer := sysdate + dbms_random.value(low => -100, high => 100);
		project_end_date_buffer := project_start_date_buffer + 61;
		project_name_buffer := dbms_random.string('x', 8);
		new_project := tmnc_commons.GET_NEW_OBJECT_ID();

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_project, null, 1 /* project Obj_Type ID */, 'project_' || project_name_buffer || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_project, 1 /* pr_description Attr ID */, 'project_description_' || project_name_buffer, null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_project, 2 /* logo Attr ID */, '/proj/' || project_name_buffer || '_logo', null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_project, 4 /* phase Attr ID */, null, null, round(dbms_random.value(low => 1, high => 3)))

		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_project, 6 /* pr_start_date Attr ID */, null, project_start_date_buffer, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_project, 7 /* pr_end_date Attr ID */, null, project_end_date_buffer, null)
		select *
		from dual;
		return new_project;
	end;

	/*
	Insert new User Account in OBJECTS with params in ATTRIBUTES
	*/

	function generate_new_user_account(file_handler sys.utl_file.file_type)
		return number
	is
		user_name_buffer                   varchar(16);
		user_raw_password_buffer           varchar(32);
		user_password_buffer               varchar(128);
		new_user_account/*next OBJECT_ID*/ number;
	begin
		new_user_account         := tmnc_commons.get_new_object_id();
		user_name_buffer         := 'userobj_' || dbms_random.string('x', 8);
		user_raw_password_buffer := tmnc_commons.random_password(32);
		user_password_buffer     := tmnc_commons.pbkdf2(user_raw_password_buffer, 'justsalt', 4, 64);

		utl_file.putf(
		        file_handler,
		        'USER_ID: '      || new_user_account         || ', ' ||
			    'USER_NAME: '    || user_name_buffer         || ', ' ||
			    'RAW_PASSWORD: ' || user_raw_password_buffer || ', ' ||
			    'ENC_PASSWORD: ' || user_password_buffer     || ';\n'
		);

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_user_account, null, 2 /* User_account Obj_Type ID */, user_name_buffer);

		insert all
		    into attributes (object_id, attr_id, value, date_value, list_value_id)
				values (new_user_account, 8 /* last_name Attr ID */, 'last_name_of_user_' || user_name_buffer, null, null)
			into attributes (object_id, attr_id, value, date_value, list_value_id)
				values (new_user_account, 9 /* first_name Attr ID */, 'first_name_of_user_' || user_name_buffer, null, null)
			into attributes (object_id, attr_id, value, date_value, list_value_id)
				values (new_user_account, 10 /* password Attr ID */, user_password_buffer, null, null)
			into attributes (object_id, attr_id, value, date_value, list_value_id)
				values (new_user_account, 11 /* email Attr ID */, 'email_of_' || user_name_buffer || '@email.com', null, null)
			into attributes (object_id, attr_id, value, date_value, list_value_id)
				values (new_user_account, 12 /* avatar Attr ID */, '/usr/' || user_name_buffer || '/avatar.jpg', null, null)
			into attributes (object_id, attr_id, value, date_value, list_value_id)
				values (new_user_account, 13 /* system_role Attr ID */, null, null, 4)
		select * from dual;

		return new_user_account;
	end;

	/*
	Insert new Participant in OBJECTS with params in ATTRIBUTES
	*/
	function generate_new_participant
		return number
	is
		new_participant/*next OBJECT_ID*/ number;
	begin
		new_participant := tmnc_commons.GET_NEW_OBJECT_ID();

		insert into objects (object_id, parent_id, object_type_id, name)
		values (                                  new_participant, null, 3
			       /* Participant Obj_Type ID */, 'participant_' || dbms_random.string('x', 8) || '_obj');
		insert into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_participant, 15 /* role Attr ID */, null, null, round(dbms_random.value(low => 6, high => 7)));
		return new_participant;
	end;

	/*
	Insert new Sprint in OBJECTS with unique params in ATTRIBUTES
	*/
	function generate_new_sprint
		return number
	is
		sprint_name_buffer varchar(50);
		sprint_start_date_buffer timestamp;
		sprint_end_date_buffer timestamp;
		new_sprint /*next OBJECT_ID*/ number;
	begin
		new_sprint := tmnc_commons.get_new_object_id();

		sprint_start_date_buffer := sysdate + dbms_random.value(low => -100, high => 100);
		sprint_end_date_buffer := sprint_start_date_buffer + 61;
		sprint_name_buffer := dbms_random.string('x', 8);

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_sprint, null, 4 /* Sprint Obj_Type ID */, 'sprint_' || sprint_name_buffer || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint, 16 /* sp_description Attr ID */, 'sprint_description_' || sprint_name_buffer, null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint, 17 /* sp_start_date Attr ID */, null, sprint_start_date_buffer, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint, 18 /* sp_end_date Attr ID */, null, sprint_end_date_buffer, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_sprint, 19 /* sp_status Attr ID */, null, null, round(dbms_random.value(low => 8, high => 10)))
		select *
		from dual;
		return new_sprint;
	end;

	/*
	Insert new Task in OBJECTS with unique params in ATTRIBUTES
	*/
	function generate_new_task
		return number
	is
		task_deadline_buffer timestamp;
		task_spent_buffer timestamp;
		task_name_buffer varchar(50);
		new_task /*next OBJECT_ID*/ number;

	begin
		new_task := tmnc_commons.GET_NEW_OBJECT_ID();

		task_deadline_buffer := sysdate + dbms_random.value(low => -100, high => 100);
		task_name_buffer := dbms_random.string('x', 8);
		task_spent_buffer := sysdate + dbms_random.value(low => -50, high => 50);

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_task, null, 5 /* Task Obj_Type ID */, 'task_' || task_name_buffer || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_task, 21 /* type Attr ID */, null, null, round(dbms_random.value(low => 11, high => 14)))
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_task, 22 /* task_status Attr ID */, null, null, round(dbms_random.value(low => 15, high => 21)))
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_task, 24 /* priority Attr ID */, null, null, round(dbms_random.value(low => 22, high => 26)))
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_task, 25 /* deadline Attr ID */, null, task_deadline_buffer, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_task, 28 /* description Attr ID */, 'task_description' || task_name_buffer || '_obj', null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_task, 29 /* task_spent_time Attr ID */, null, task_spent_buffer, null)
		select *
		from dual;
		return new_task;
	end;

	/*
	Insert new Docimentation in OBJECTS with unique params in ATTRIBUTES
	*/
	function generate_new_documentation
		return number
	is
		doc_creation_time timestamp;
		doc_name_buffer varchar(50);
		new_doc /*next OBJECT_ID*/ number;

	begin
		new_doc := tmnc_commons.GET_NEW_OBJECT_ID();

		doc_creation_time := sysdate + dbms_random.value(low => -100, high => 100);
		doc_name_buffer := dbms_random.string('x', 8);

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_doc, null, 6 /* Document Obj_Type ID */, 'documentation_' || doc_name_buffer || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_doc, 30 /* file_path Attr ID */, '/file_path/' || doc_name_buffer, null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_doc, 31 /* creation_time Attr ID */, null, doc_creation_time, null)
		select *
		from dual;
		return new_doc;
	end;

	/*
	Insert new Label in OBJECTS with unique params in ATTRIBUTES
	*/
	function generate_new_label
		return number
	is

		label_name_buffer varchar(50);
		label_color varchar(50);
		new_label /*next OBJECT_ID*/ number;

	begin
		new_label := tmnc_commons.GET_NEW_OBJECT_ID();

		label_name_buffer := dbms_random.string('x', 8);
		label_color := TO_CHAR(TRUNC(dbms_random.VALUE(0, 256 * 256 * 256)), 'FM0XXXXX');

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_label, null, 7 /* Label Obj_Type ID */, 'label_' || label_name_buffer || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_label, 33 /* color Attr ID */, label_color, null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_label, 34 /* label_description Attr ID */, 'label_description' || label_name_buffer, null, null)
		select *
		from dual;
		return new_label;
	end;

	/*
	Insert new Comment in OBJECTS with unique params in ATTRIBUTES
	*/
	function generate_new_comment
		return number
	is
		com_changed_time timestamp;
		comm_content_buffer varchar(50);
		com_edited number;
		new_comment /*next OBJECT_ID*/ number;

	begin
		new_comment := tmnc_commons.GET_NEW_OBJECT_ID();

		com_changed_time := sysdate + dbms_random.value(low => -100, high => 100);
		comm_content_buffer := dbms_random.string('x', 45);
		com_edited := round(dbms_random.value(0, 1));

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_comment, null, 8 /* Comment Obj_Type ID */, 'comment__' || comm_content_buffer || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_comment, 35 /* time_changed Attr ID */, null, com_changed_time, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_comment, 37 /* content Attr ID */, 'comment_content_' || comm_content_buffer, null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_comment, 38 /* is_edited Attr ID */, com_edited, null, null)
		select *
		from dual;
		return new_comment;
	end;

	/*
	Insert new Log in OBJECTS with unique params in ATTRIBUTES
	*/
	function generate_new_log
		return number
	is
		log_changed_time timestamp;
		log_buffer varchar(50);
		new_log number;

	begin
		new_log := tmnc_commons.GET_NEW_OBJECT_ID();

		log_changed_time := sysdate + dbms_random.value(low => -100, high => 100);

		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_log, null, 9 /* Log Obj_Type ID */, 'log_' || log_buffer || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_log, 39 /* old_value Attr ID */, 'OLD_VALUE', null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_log, 40 /* new_value Attr ID */, 'NEW_VALUE', null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_log, 41 /* time Attr ID */, null, log_changed_time, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_log, 42 /* change_param Attr ID */, dbms_random.value(low => 0, high => 46), null, null)
		select *
		from dual;
		return new_log;
	end;


	/*
	Insert new Invite in OBJECTS with unique params in ATTRIBUTES
	*/

	function generate_new_invite
		return number
	is
		new_invite/*next OBJECT_ID*/ number;
	begin
		new_invite := tmnc_commons.GET_NEW_OBJECT_ID();
		insert into objects (object_id, parent_id, object_type_id, name)
		values (new_invite, null, 10 /* Invite Obj_Type ID */, 'invite_' || dbms_random.string('x', 8) || '_obj');
		insert all into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_invite, 44 /* URL Attr ID */, 'www.taskmanager.nc/' || dbms_random.string('x', 9), null, null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_invite, 45 /* RECIPIENT_EMAIL Attr ID */, 'mail_' || dbms_random.string('x', 9) || '@gmail.com',
		        null,
		        null)
		into attributes (object_id, attr_id, value, date_value, list_value_id)
		values (new_invite, 46 /* ROLE Attr ID */, null, null, round(dbms_random.value(low => 6, high => 7)))
		select *
		from dual;
		return new_invite;
	end;


	/*
	General procedure to inserting random data into the database for a specific number of projects
	*/
	procedure generate_data(projects_count number)
	as
		type project_participants_ids is table of number;
		type labels_in_task_ids is table of number;
		type labels_in_project_ids is table of number;
		type sprints_in_log_ids is table of number;
		type tasks_in_log_ids is table of number;

		project_participants project_participants_ids;
		labels_in_task labels_in_task_ids;
		labels_in_project labels_in_project_ids;
		sprints_in_log sprints_in_log_ids;
		tasks_in_log tasks_in_log_ids;

		generated_user_account_id number;
		generated_invite_id number;
		generated_user_entity_id number;
		generated_document_id number;
		generated_project_id number;
		generated_sprint_id number;
		generated_task_id number;
		generated_label_id number;
		generated_comment_id number;
		generated_log_id number;
		doc_creator_id number;
		task_creator_id number;
		task_assignee_id number;
		random_obj_param_id number;
		label_in_task_id number;

		location varchar2(512);
		file_handler utl_file.file_type;
	begin
	    sys.dbms_system.get_env('USERS_CREDENTIALS_OUTPUT', location);

	    if location = '' or location is null then
	        sys.dbms_system.get_env('PWD', location);

		    location := location || '/output';
	    end if;

		execute immediate 'create or replace directory USERS_CREDENTIALS_OUTPUT as '
		                      || ''''  || location || '''';

		file_handler := utl_file.fopen('USERS_CREDENTIALS_OUTPUT', 'users_credentials.txt', 'W');

		for project_index in 1..projects_count
		loop
			project_participants := project_participants_ids();
			/* Add project */
			generated_project_id := generate_new_project();
			for user_index in 1..20
			loop
				/* Add invites */
				generated_invite_id := generate_new_invite();
				update_parent(generated_invite_id, generated_project_id);
				/* Add users */
				generated_user_account_id := generate_new_user_account(file_handler);
				generated_user_entity_id := generate_new_participant();
				update_parent(generated_user_entity_id, generated_user_account_id);
				add_ref_to_object_by_attr(generated_project_id, generated_user_entity_id,
				                          14 /* project_id Attr ID */);
				if user_index = 1
				then
					/* First user - creator */
					add_ref_to_object_by_attr(generated_user_entity_id, generated_project_id,
					                          3 /* PR_CREATOR Attr ID */);
					doc_creator_id := generated_user_entity_id;
				end if;

				add_ref_to_object_by_attr(generated_user_entity_id, generated_project_id,
				                          5 /* PARTICIPANTS Attr ID */);
				project_participants.extend(1);
				/* Other users - participants */
				project_participants(project_participants.count) := generated_user_entity_id;
			end loop;

			labels_in_project := labels_in_project_ids();
			/* Add Labels */
			for label_index in 1..20
			loop
				generated_label_id := generate_new_label();
				/* Connect labels to project */
				update_parent(generated_label_id, generated_project_id);
				labels_in_project.extend(1);
				labels_in_project(labels_in_project.count) := generated_label_id;
			end loop;
			/* Add Documents to Project */
			for document_index in 1..5
			loop
				generated_document_id := generate_new_documentation();
				/* Connect documents to project */
                add_ref_to_object_by_attr(doc_creator_id, generated_document_id,
					                          32 /* DOC_CREATOR Attr ID */);
				update_parent(generated_document_id, generated_project_id);
			end loop;

			sprints_in_log := sprints_in_log_ids();
			tasks_in_log := tasks_in_log_ids();
			/* Add Sprints */
			for sprint_index in 1..5
			loop
				generated_sprint_id := generate_new_sprint();

				sprints_in_log.extend(1);
				sprints_in_log(sprints_in_log.count) := generated_sprint_id;
				/* Connect sprints to project */
				update_parent(generated_sprint_id, generated_project_id);
				/* Add Tasks */
				for task_index in 1..10
				loop
					generated_task_id := generate_new_task();

					tasks_in_log.extend(1);
					tasks_in_log(tasks_in_log.count) := generated_task_id;
					task_creator_id := project_participants(round(dbms_random.value(low => 1, high => 20)));
					task_assignee_id :=
							project_participants(round(dbms_random.value(low => 1, high => 20)));
					/* Connect sprints to project */
					update_parent(generated_task_id, generated_sprint_id);
					/* One User - Creator */
					add_ref_to_object_by_attr(task_creator_id, generated_task_id,
					                          23 /* TASK_CREATOR Attr ID */);
					/* One User - Assignee */
					add_ref_to_object_by_attr(task_assignee_id, generated_task_id,
					                          20 /* ASSIGNEE Attr ID */);
					add_ref_to_object_by_attr(task_creator_id, generated_task_id,
					                          27 /* WATCHER_LIST Attr ID */);

					if task_assignee_id <> task_creator_id
					then
						/* One User - Watcher */
						add_ref_to_object_by_attr(task_assignee_id, generated_task_id,
						                          27 /* WATCHER_LIST Attr ID */);
					end if;
					/* Add Documents to Task */
					for document_index in 1..2
					loop
						generated_document_id := generate_new_documentation();
						/* Connect document to task */
						update_parent(generated_document_id, generated_task_id);
					end loop;

					labels_in_task := labels_in_task_ids();
					/* Find Labels in project */
					for label_index in 1..round(dbms_random.value(low => 2, high => 3))
					loop
						label_in_task_id :=
								labels_in_project(round(dbms_random.value(low => 1, high => 20)));
						while label_in_task_id member of labels_in_task
						loop
							label_in_task_id :=
									labels_in_project(round(dbms_random.value(low => 1, high => 20)));
						end loop;
						/* Connect Labels to Task */
						add_ref_to_object_by_attr(label_in_task_id, generated_task_id,
						                          26 /* LABEL Attr ID */);
						labels_in_task.extend(1);
						labels_in_task(labels_in_task.count) := label_in_task_id;
					end loop;
					/* Add Comments to Task */
					for comment_index in 1..3
					loop
						generated_comment_id := generate_new_comment();
						add_ref_to_object_by_attr(project_participants(round(dbms_random.value(low => 1, high => 20))),
				                          generated_comment_id, 36 /* creator Attr ID */);
						/* Connect Comment to Task */
						update_parent(generated_comment_id, generated_task_id);
					end loop;
				end loop;
			end loop;
			/* Add Logs */
			for log_index in 1..10
			loop
				generated_log_id := generate_new_log();
				/* User, which do anything */
				add_ref_to_object_by_attr(project_participants(round(dbms_random.value(low => 1, high => 20))),
				                          generated_log_id, 43 /* USER Attr ID */);

				random_obj_param_id := round(dbms_random.value(low => 1, high => 3));
				if random_obj_param_id = 1
				then
					/* Connect log to project */
					update_parent(generated_log_id, generated_project_id);

				elsif random_obj_param_id = 2
				then
					/* Connect log to sprint */
					update_parent(generated_log_id,
					              sprints_in_log(round(dbms_random.value(low => 1, high => 5))));
				else
					/* Connect log to task */
					update_parent(generated_log_id,
					              tasks_in_log(round(dbms_random.value(low => 1, high => 10))));
				end if;
			end loop;
		end loop;

		utl_file.fclose(file_handler);
		execute immediate 'drop directory USERS_CREDENTIALS_OUTPUT';
	end;
end;
/