import {Component} from "@angular/core"
import {UserAuthService} from "./services/user/auth/user-auth.service"
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"]
})
export class AppComponent {

	constructor(private router: Router,
				private userAuthService: UserAuthService,
				private snackBar: MatSnackBar) {
	}

	ngOnInit() {

	}
}
