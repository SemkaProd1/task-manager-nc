import {NgModule} from "@angular/core"
import {
	Routes,
	RouterModule
} from "@angular/router"
import {LoginPageComponent} from "./components/user/login-page/login-page.component"
import {MainPageComponent} from "./components/core/main-page/main-page.component"
import {MainPageGuard} from "./guards/main-page/main-page.guard"
import {LoginFormComponent} from "./components/user/login-form/login-form.component"
import {RegistrationFormComponent} from "./components/user/registration-form/registration-form.component"
import {CreateProjectComponent} from "./components/project/create-project/create-project.component";
import {CreateTaskComponent} from "./components/task/create-task/create-task.component";
import {LoginPageGuard} from "./guards/login-page/login-page.guard"
import {UserInfoComponent} from "./components/user/user-info/user-info.component"
import {SprintBoardComponent} from "./components/sprint/sprint-board/sprint-board.component";
import {CreateSprintComponent} from "./components/sprint/create-sprint/create-sprint.component";
import {ProjectBoardComponent} from "./components/project/project-board/project-board.component";
import {UpdatePasswordComponent} from "./components/user/update-password/update-password.component";
import {UpdateEmailComponent} from "./components/user/update-email/update-email.component";
import {UpdateUserInfoComponent} from "./components/user/update-user-info/update-user-info.component";
import {DocumentationComponent} from "./components/document/documentation/documentation.component"
import {UpdateProjectComponent} from "./components/project/update-project/update-project.component";
import {UpdateTaskComponent} from "./components/task/update-task/update-task.component";
import {CreateLabelComponent} from "./components/label/create-label/create-label.component";
import {UserInviteComponent} from "./components/user/user-invite/user-invite.component";

const routes: Routes = [
	{
		path: "login",
		component: LoginPageComponent,
		canActivate: [LoginPageGuard],
		children: [
			{
				path: "",
				pathMatch: "full",
				component: LoginFormComponent
			},
			{
				path: "register",
				component: RegistrationFormComponent
			}
		]
	},
	{
		path: "app",
		component: MainPageComponent,
		canActivate: [MainPageGuard],
		children: [
			{
				path: "project",
				children: [
					{
						path: "new",
						component: CreateProjectComponent
					},
					{
						path: "board/:id",
						component: ProjectBoardComponent
					},
					{
						path: "update/:id",
						component: UpdateProjectComponent
					},
					{
						path: "label/new",
						component: CreateLabelComponent
					},
					{
						path: "invite",
						component: UserInviteComponent
					}
				],
			},
			{
				path: "user",
				children:[
					{
						path: "info",
						component: UserInfoComponent
					},
					{
						path: "edit/info",
						component: UpdateUserInfoComponent
					},
					{
						path: "edit/password",
						component: UpdatePasswordComponent
					},
					{
						path: "edit/email",
						component: UpdateEmailComponent
					},

				]
			},
			{
				path:"documentation",
				component: DocumentationComponent
			},
			{
				path:"sprint/board/:id",
				component: SprintBoardComponent
			},
			{
				path:"task",
				children:[
					{
						path: "new",
						component: CreateTaskComponent
					},
					{
						path: ":id/update",
						component: UpdateTaskComponent
					}
				]

			},

			{
				path: "sprint/new",
				component: CreateSprintComponent
			}
		]
	},
	{
		path: "**",
		redirectTo: "app"
	}
]


@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
