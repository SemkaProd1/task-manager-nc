import {Label} from "./label";

export enum TaskStatus {
    Open = "OPEN",
    InProgress = "IN_PROGRESS",
    Implemented = "IMPLEMENTED",
    ReadyForTesting = "READY_FOR_TESTING",
    Reopened = "REOPENED",
    OnHold = "ON_HOLD",
    Closed = "CLOSED"
}

export interface LiteTask {
    taskId: number;
    name: string;
    deadline: Date;
    labels: Label[];
    status: TaskStatus;
    assignee: number;
    assigneeUsername: string;
}