import {SimpleProjectInfo} from "./project";
import {SafeUrl} from "@angular/platform-browser"

export interface UserCredentials {
	username: string
	password: string
}

export type UserAuthCallback = (authenticatedUser: AuthenticatedUser) => void

export interface Principal {
	readonly objectId:  number
	readonly lastName:  string
	readonly firstName: string
	readonly email:     string
	readonly username:  string
}

export interface AuthenticatedUser {
	readonly principal: Principal
	readonly projects?: SimpleProjectInfo[]
}

export enum ParticipantRole {
	User = "USER",
	ProjectManager = "PROJECT_MANAGER"
}

export interface Participant {
	readonly objectId: number
	readonly projectId: number
	readonly role: ParticipantRole
	readonly userId: number
}

export interface ParticipantFullInfo {
	readonly avatar:    SafeUrl
	readonly username:  string
	readonly objectId:  number
	readonly projectId: number
	readonly role:      ParticipantRole
	readonly userId:    number
	readonly lastName:  string
	readonly firstName: string
	readonly email:     string
}

export interface UpdatePasswordUser {
	oldPassword: string,
	password: string,
	confirmPassword: string
}