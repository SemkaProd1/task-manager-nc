import {ParticipantRole} from "./user-credentials";
import {SafeUrl} from "@angular/platform-browser"


export interface Project {
    objectId: number
    creatorId: number
    projectName: string
    projectDescription: string
    startDate: Date
    endDate: Date
}

export interface CreatedProject {
    projectId: number
}


export class SimpleProjectInfo {
    objectId: number
    participantRole: ParticipantRole
    projectName: string
}

export interface ProjectInfoWithLogo extends SimpleProjectInfo {
    projectLogo: SafeUrl
}