export interface Documentation {
	objectId: number
	name: string
	creationDate: Date
	participantUrl: string
	creatorParticipantId: number
	creatorParticipantName: number
}
