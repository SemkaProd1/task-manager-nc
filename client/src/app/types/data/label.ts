export interface Label {
    objectId: number,
    color: string,
    name: string,
    description: string
}