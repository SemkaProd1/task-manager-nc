import {Label} from "./label";
import {TaskStatus} from "./liteTask"

export interface Task {
	creatorParticipantId: number,
	sprintId: number,
	name: string,
	description: string,
	spentTime: Date,
	deadline: Date,
	type: string,
	status: string,
	priority: string
}

export interface CreatedTask {
	taskId: number,
	name: string,
	description: string,
	spentTime: Date,
	deadline: Date,
	type: string,
	status: string,
	priority: string
}

export interface TaskPage {
	taskId: number,
	assigneeParticipantId: number,
	creatorParticipantId: number,
	labels: number[],
	name: string,
	description: string,
	deadline: Date,
	type: string,
	status: string,
	priority: string
}

export interface UpdateTaskStatus {
	objectId: number,
	status: TaskStatus
}
export enum TaskStatusImpl {
	OPEN = 'OPEN',
	IN_PROGRESS = 'IN_PROGRESS',
	IMPLEMENTED = 'IMPLEMENTED',
	READY_FOR_TESTING = 'READY_FOR_TESTING',
	REOPENED = 'REOPENED',
	ON_HOLD = 'ON_HOLD',
	CLOSED = 'CLOSED'
}

export enum TaskPriority {
	BLOCKER = 'BLOCKER',
	CRITICAL = 'CRITICAL',
	NORMAL = 'NORMAL',
	LOW = 'LOW',
	MAJOR = 'MAJOR'
}

export enum TaskType {
	TASK = 'TASK',
	BUG = 'BUG',
	STORY = 'STORY',
	EPIC = 'EPIC'
}