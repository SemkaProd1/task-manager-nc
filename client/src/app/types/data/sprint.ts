export interface Sprint {

	objectId: number,
	projectId: number,
	name: string,
	description: string,
	startDate: Date,
	endDate: Date,
	status: string
}