import {Injectable} from "@angular/core"
import {
	CanActivate,
	ActivatedRouteSnapshot,
	RouterStateSnapshot,
	Router
} from "@angular/router"
import {Observable, of} from "rxjs"
import {UserAuthService} from "../../services/user/auth/user-auth.service"
import {map, mergeMap, tap} from "rxjs/operators";

@Injectable({
	providedIn: "root"
})
export class LoginPageGuard implements CanActivate {
	constructor(private authService: UserAuthService,
	            private router: Router) {}

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
		Observable<boolean> | Promise<boolean> | boolean {

		return this.authService.isAuthenticated()
			.pipe(
				mergeMap(x => {
					if (x) {
						console.log("redirecting to app")
						this.router.navigate(["/app"])

						return of(false)
					}

					return of(true)
				})
			)
	}
}
