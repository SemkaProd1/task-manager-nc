import {Injectable} from "@angular/core"
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot
} from "@angular/router"

import {
	EMPTY,
	from,
	Observable,
	of
} from "rxjs"

import {
	UserAuthService
} from "../../services/user/auth/user-auth.service"
import {map, mergeMap, tap} from "rxjs/operators";


@Injectable({
	providedIn: "root"
})
export class MainPageGuard implements CanActivate {

	constructor(private authService: UserAuthService,
	            private router: Router) {}

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
		Observable<boolean> | Promise<boolean> | boolean {

		return this.authService.isAuthenticated()
			.pipe(
				mergeMap(authenticated => {
					if (!authenticated) {
						return from(this.router.navigate(["/login"]))
							.pipe(map(() => true))
					}

					return this.authService.updateUser()
						.pipe(map(() => true))
				})
			)
	}
}
