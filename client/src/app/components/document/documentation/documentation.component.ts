import {Component, OnInit} from "@angular/core"
import {UserAuthService} from "../../../services/user/auth/user-auth.service"
import {DateService} from "../../../utils/date/date.service"
import {ProjectService} from "../../../services/project/project.service"
import {DocumentationService} from "../../../services/documentation/documentation.service"
import {HttpEventType, HttpResponse} from "@angular/common/http"
import {Documentation} from "../../../types/data/documentation"
import {catchError} from "rxjs/operators"
import {of} from "rxjs"
import {MatSnackBar} from "@angular/material"

@Component({
	selector: "app-documentation",
	templateUrl: "./documentation.component.html",
	styleUrls: ["./documentation.component.scss"]
})
export class DocumentationComponent implements OnInit {
	isDragEnter: boolean = false
	isNeedToShowProgressBar: boolean = false
	progress: number = 0
	documents: Documentation[] = []

	constructor(private authService: UserAuthService,
	            private dateService: DateService,
	            private projectService: ProjectService,
	            private documentationService: DocumentationService,
	            private snackBar: MatSnackBar) {
	}

	ngOnInit() {
		this.documentationService.loadProjectDocuments()
			.subscribe(value => this.documents = value)
	}

	addNewDocument(event: DragEvent) {
		event.preventDefault()
		event.stopPropagation()

		this.isDragEnter = !this.isDragEnter
		this.isNeedToShowProgressBar = true

		const file: File = event.dataTransfer.files[0]

		this.documentationService.create(file)
			.subscribe(event => {
				if (event.type === HttpEventType.UploadProgress) {
					this.progress = Math.round((100 * event.loaded) / event.total);
				} else if (event instanceof HttpResponse) {
					this.isNeedToShowProgressBar = false
					this.progress = 0
					if (event.body) {
						this.documentationService.getDocument(event.body)
							.subscribe(value => this.documents.push(value))
					}
				}
			}, error1 => {
				this.snackBar.open("Something went wrong. " + error1.error.message, "Got it")
			})
	}

	preventDefault(event: Event) {
		event.preventDefault()
		event.stopPropagation()
	}

	switchStyle(event: Event) {
		event.preventDefault()
		event.stopPropagation()

		this.isDragEnter = !this.isDragEnter
	}

	downloadFile(id: number, name: string) {
		this.documentationService.downloadDocument(id).subscribe(value => {
			const url = window.URL.createObjectURL(value);
			const anchorForDownload = document.createElement("a")

			document.body.appendChild(anchorForDownload)

			anchorForDownload.href = url
			anchorForDownload.download = name

			anchorForDownload.click()

			window.URL.revokeObjectURL(url)
			anchorForDownload.remove()
		})
	}

	deleteFile(id: Documentation) {
		this.documentationService.deleteDocument(id.objectId)
			.subscribe(value => {
				let number = this.documents.findIndex(
					document => document.objectId === id.objectId)
				this.documents.splice(number, 1)
			})
	}
}
