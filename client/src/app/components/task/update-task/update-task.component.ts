import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {UserAuthService} from "../../../services/user/auth/user-auth.service";
import {FormValidator} from "../../../utils/validators/form-validator";
import {TaskService} from "../../../services/task/task.service";
import {TaskPriority, TaskStatusImpl, TaskType} from "../../../types/data/task";
import {EnumHandlerService} from "../../../utils/enum-handler/enum-handler.service";
import {mergeMap} from "rxjs/operators";
import {ProjectService} from "../../../services/project/project.service";
import {Participant, ParticipantFullInfo} from "../../../types/data/user-credentials";
import {LabelService} from "../../../services/label/label.service";



@Component({
	selector: 'app-update-task',
	templateUrl: './update-task.component.html',
	styleUrls: ['./update-task.component.scss']
})
export class UpdateTaskComponent implements OnInit {

	task = this.taskService.currentTask;
	participant = this.projectService.currentParticipant;
	participantAccount = this.projectService.participantAccounts;

	types = this.handler.toArray(TaskType);
	statuses = this.handler.toArray(TaskStatusImpl);
	priorities = this.handler.toArray(TaskPriority);
	assignees: Array<ParticipantFullInfo>;
	participants: Array<Participant>;
	projectLabels = this.projectService.projectLabels;
	currentLabelsIds;

	constructor(private formBuilder: FormBuilder, private taskService: TaskService,
	            private activateRoute: ActivatedRoute, private auth: UserAuthService,
	            private formValidator: FormValidator,
	            private handler: EnumHandlerService,
	            private activatedRoute: ActivatedRoute,
	            private projectService: ProjectService,
	            private labelService: LabelService) {
	}

	updateTaskFormGroup: FormGroup;

	ngOnInit() {
		this.updateTaskFormGroup = this.formBuilder.group({
			name: this.formBuilder.control("", [Validators.required]),
			description: this.formBuilder.control("", [Validators.required]),
			type: this.formBuilder.control("", [Validators.required]),
			status: this.formBuilder.control("", [Validators.required]),
			assignee: this.formBuilder.control("", [Validators.required]),
			priority: this.formBuilder.control("", [Validators.required]),
			labels:      this.formBuilder.control([]),
			deadline: this.formBuilder.control("", [Validators.required])
		})

		this.participantAccount.subscribe(value => {
			this.participants = value.map(value1 => value1);
		})

		this.projectService.participantAccounts.subscribe(value => {
			this.assignees = value.map(value1 => value1)
		})

		this.activatedRoute.params.pipe(mergeMap(value => {
				console.log(value.id);
				return this.taskService.loadCurrentTask(value.id);
			}
		)).subscribe(value => {

			this.labelService.getLabels(value.labels).subscribe(value1 => value1);

			console.log(this.labelService.labels);


			this.updateTaskFormGroup = this.formBuilder.group({
				name:        this.formBuilder.control(value.name, [Validators.required]),
				description: this.formBuilder.control(value.description, [Validators.required]),
				type:        this.formBuilder.control(value.type, [Validators.required]),
				assignee:    this.formBuilder.control(value.assigneeParticipantId, [Validators.required]),
				status:      this.formBuilder.control(value.status, [Validators.required]),
				priority:    this.formBuilder.control(value.priority, [Validators.required]),
				creatorId:   this.formBuilder.control(value.creatorParticipantId),
				labels:      this.formBuilder.control(this.labelService.labels),
				deadline:    this.formBuilder.control(new Date(value.deadline), [Validators.required])
			})
		});
	}

	update() {
		if (this.updateTaskFormGroup.valid) {
			this.taskService.update(this.updateTaskFormGroup);
		} else {
			this.formValidator.validateAllFormFields(this.updateTaskFormGroup);
		}
	}
}
