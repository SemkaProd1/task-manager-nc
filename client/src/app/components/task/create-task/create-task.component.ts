import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {TaskService} from "../../../services/task/task.service";
import {FormValidator} from "../../../utils/validators/form-validator";
import {TaskPriority, TaskStatusImpl, TaskType} from "../../../types/data/task";
import {EnumHandlerService} from "../../../utils/enum-handler/enum-handler.service";
import {ProjectService} from "../../../services/project/project.service"

@Component({
    selector: 'app-create-task',
    templateUrl: './create-task.component.html',
    styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {

    types = this.handler.toArray(TaskType);
    statuses = this.handler.toArray(TaskStatusImpl);
    priorities = this.handler.toArray(TaskPriority);
    participants = this.projectService.participantAccounts
    projectLabels = this.projectService.projectLabels

    constructor(private formBuilder: FormBuilder, private http: HttpClient,
                private taskService: TaskService,
                private formValidator: FormValidator,
                private handler: EnumHandlerService,
                private projectService: ProjectService) {}

    public createTaskFormModel: FormGroup;

    ngOnInit() {
        this.createTaskFormModel = this.formBuilder.group({
            name:        this.formBuilder.control("", [Validators.required]),
            description: this.formBuilder.control(""),
            deadline:    this.formBuilder.control(new Date().getDate(), [Validators.required]),
            type:        this.formBuilder.control("", [Validators.required]),
            status:      this.formBuilder.control("", [Validators.required]),
            priority:    this.formBuilder.control("", [Validators.required]),
            assignee:    this.formBuilder.control(null, [Validators.required]),
            labels:      this.formBuilder.control([]),
            documents:   this.formBuilder.control([])
        })
    }

    create() {
        this.taskService.create(this.createTaskFormModel);
    }
}


