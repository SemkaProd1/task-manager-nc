import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticatedUser} from "../../../types/data/user-credentials";
import {UserAuthService} from "../../../services/user/auth/user-auth.service";
import {Observable} from "rxjs";
import {UserService} from "../../../services/user/user/user.service";
import {FileDownloadService} from "../../../services/file-download/file-download.service";
import {SafeUrl} from "@angular/platform-browser";
import {DialogMenuComponent} from "../../core/dialog-menu/dialog-menu.component";
import {MatDialog} from "@angular/material";
import {FormValidator} from "../../../utils/validators/form-validator";
import {UsernameValidator} from "../../../utils/validators/username-validator";


@Component({
    selector: 'app-update-user-info',
    templateUrl: './update-user-info.component.html',
    styleUrls: ['./update-user-info.component.scss']
})
export class UpdateUserInfoComponent implements OnInit {

    user: Observable<AuthenticatedUser> = this.userAuthService.authenticatedUserObservable;

    avatar: File = null;
    avatarPreview: SafeUrl = "../../../../assets/images/person.svg";

    constructor(private formBuilder: FormBuilder,
                private userAuthService: UserAuthService,
                private userService: UserService,
                private fileDownload: FileDownloadService,
                private dialog: MatDialog,
                private formValidator: FormValidator,
                private userValidator: UsernameValidator) {}


    public updateUserInfoFormGroup: FormGroup;

    ngOnInit() {
        this.user.subscribe(value => {
            this.fileDownload.loadUserAvatar(value.principal.objectId).subscribe(value1 => {
                this.avatarPreview = value1;
            });

            this.updateUserInfoFormGroup = this.formBuilder.group({
                username: this.formBuilder.control(value.principal.username, [Validators.required,
                    Validators.pattern("^(?=.{5,32}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$")],
            this.userValidator.checkAuthorizedName.bind(this.userValidator)),
                firstName: this.formBuilder.control(value.principal.firstName, [Validators.required]),
                lastName: this.formBuilder.control(value.principal.lastName, [Validators.required])
            })
        })
    }

    onFileChange(event){
        console.log(event);
        this.avatar = event.target.files[0];
    }

    handleFileInput(file: FileList){
        this.avatar = file.item(0);

        let reader = new FileReader();
        reader.onload = (event: any) =>{
            this.avatarPreview = event.target.result;
        }
        reader.readAsDataURL(this.avatar);
    }

    delete(){
        const dialogRef=this.dialog.open(DialogMenuComponent);
        dialogRef.afterClosed().subscribe(result=>{
            if(result){
                this.userService.deleteAccount();
            } else{
                console.log('false');
            }
        })
    }

    update(){
        this.userService.updateUserInfo(this.updateUserInfoFormGroup, this.avatar);
    }
}
