import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user/user/user.service";
import {EmailValidator} from "../../../utils/validators/email-validator";
import {FormValidator} from "../../../utils/validators/form-validator";

@Component({
  selector: 'app-update-email',
  templateUrl: './update-email.component.html',
  styleUrls: ['./update-email.component.scss']
})
export class UpdateEmailComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private userService: UserService,
              private emailValidator: EmailValidator,
              private formValidator: FormValidator) { }

  public updateEmailFormGroup: FormGroup;

  ngOnInit() {
    this.updateEmailFormGroup = this.formBuilder.group({

	        email: this.formBuilder.control("", [Validators.required, Validators.email],
		        this.emailValidator.checkAuthorizedEmail.bind(this.emailValidator))
	    })
  }

  update(){
  	if(this.updateEmailFormGroup.valid){
	    this.userService.updateEmail(this.updateEmailFormGroup);
    } else {
  	    this.formValidator.validateAllFormFields(this.updateEmailFormGroup);
    }
  }
}
