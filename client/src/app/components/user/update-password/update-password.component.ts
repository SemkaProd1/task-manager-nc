import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserAuthService} from "../../../services/user/auth/user-auth.service";
import {AuthenticatedUser, UpdatePasswordUser} from "../../../types/data/user-credentials";
import {PasswordValidator} from "../../../utils/validators/password-validator";
import {ParentErrorStateMatcher} from "../../../utils/error-matchers/parent-error-state-matcher";
import {Observable} from "rxjs";
import {UserService} from "../../../services/user/user/user.service";
import {FormValidator} from "../../../utils/validators/form-validator";

@Component({
    selector: 'app-update-password',
    templateUrl: './update-password.component.html',
    styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {

    public user: Observable<AuthenticatedUser> = this.userAuthService.authenticatedUserObservable;

    model: UpdatePasswordUser = {
        oldPassword: '',
        password: '',
        confirmPassword: ''
    };

    constructor(private formBuilder: FormBuilder,
                private userAuthService: UserAuthService,
                private userService: UserService,
                private formValidation: FormValidator) {
    }

    public updateUserPasswordFormGroup: FormGroup;
    public createPasswordFormGroup: FormGroup;

    parentErrorStateMatcher = new ParentErrorStateMatcher();

    ngOnInit() {
        this.createPasswordForm();

        this.updateUserPasswordFormGroup = this.formBuilder.group({
            oldPassword: this.formBuilder.control("", [Validators.required]),
            matchingPassword: this.createPasswordFormGroup
        });
    }

    createPasswordForm(){
        this.createPasswordFormGroup = new FormGroup({
            newPassword: this.formBuilder.control("",
                [Validators.required, Validators.minLength(8), Validators.maxLength(32)]),
            confirmPassword: this.formBuilder.control("", [Validators.required])
        }, (formGroup: FormGroup) => {
            return PasswordValidator.areEqual(formGroup);
        });
    }

    update() {
        if(this.updateUserPasswordFormGroup.valid){
            this.userService.updatePassword(this.model);
        } else {
            this.formValidation.validateAllFormFields(this.updateUserPasswordFormGroup);
        }
    }
}

