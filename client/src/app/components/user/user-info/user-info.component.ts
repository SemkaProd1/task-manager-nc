import {
	Component,
	OnInit
} from "@angular/core"
import {AuthenticatedUser} from "../../../types/data/user-credentials"
import {UserAuthService} from "../../../services/user/auth/user-auth.service"
import {FileDownloadService} from "../../../services/file-download/file-download.service"
import {UserService} from "../../../services/user/user/user.service";
import {ProjectInfoWithLogo, SimpleProjectInfo} from "../../../types/data/project"
import {Observable, of, zip} from "rxjs"
import {mergeMap, tap} from "rxjs/operators"
import {DomSanitizer, SafeUrl} from "@angular/platform-browser"
import {Router} from "@angular/router";
import {ProjectService} from "../../../services/project/project.service";

@Component({
	selector: "app-user-info",
	templateUrl: "./user-info.component.html",
	styleUrls: ["./user-info.component.scss"]
})
export class UserInfoComponent implements OnInit {

	user: AuthenticatedUser
	avatar: SafeUrl
	projects: Observable<ProjectInfoWithLogo[]>

	constructor(public fileDownload: FileDownloadService,
				private userAuthService: UserAuthService,
	            private userService: UserService,
	            private router: Router,
	            private projectService: ProjectService,
	            private sanitizer: DomSanitizer) {
	}

	ngOnInit() {
        this.userAuthService.authenticatedUserObservable
            .pipe(
            	tap(user => {
            		this.user = user;
		            this.projects = this.receiveLogoForProjects(this.user.projects)
            	}),
                mergeMap(value => {
                    return this.fileDownload.loadUserAvatar(value.principal.objectId)
                })
            )
			.subscribe(value => {
				this.avatar = value
				this.avatar
            })

	}

	receiveLogoForProjects(projectInfos: SimpleProjectInfo[]): Observable<ProjectInfoWithLogo[]> {
		let avatarUrls: Observable<ProjectInfoWithLogo>[] = []

		for (const projectInfo of projectInfos) {
			let observable: Observable<ProjectInfoWithLogo> = this.fileDownload.loadProjectLogo(projectInfo.objectId)
				.pipe(
					mergeMap(
						value => {
							const projectLogo = value === null || value === undefined
								? this.sanitizer.bypassSecurityTrustUrl(
									"../../../../assets/images/ballot.svg")
								: this.fileDownload.toDataURL(value)

							return of({...projectInfo, projectLogo})
						}
					)
				)
			avatarUrls.push(observable)
		}

		return zip(...avatarUrls)
	}

	edit(){
		this.router.navigateByUrl("/app/user/edit/info");
	}

	getProjectId(projectId: number) {
		this.projectService.projectId = projectId;
	}
}
