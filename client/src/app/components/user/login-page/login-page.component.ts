import {Component, OnInit} from '@angular/core'
import {Router} from "@angular/router"

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

    readonly routes: {link: string, icon: string, label: string}[] = [
        {
            link: "/login",
            icon: "perm_identity",
            label: "Login"
        },
        {
            link: "/login/register",
            icon: "person_add",
            label: "Registration"
        }
    ]

    constructor(private router: Router) {

    }

    ngOnInit() {
    }

}
