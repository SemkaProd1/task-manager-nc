import {Component, OnInit} from "@angular/core"
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms"
import {UserService} from "../../../services/user/user/user.service";
import {PasswordValidator} from "../../../utils/validators/password-validator";
import {ParentErrorStateMatcher} from "../../../utils/error-matchers/parent-error-state-matcher";
import {UsernameValidator} from "../../../utils/validators/username-validator";
import {EmailValidator} from "../../../utils/validators/email-validator";
import {FormValidator} from "../../../utils/validators/form-validator";


@Component({
	selector: "app-registration-form",
	templateUrl: "./registration-form.component.html",
	styleUrls: ["./registration-form.component.scss"]
})
export class RegistrationFormComponent implements OnInit {

	model: CreateNewUserRequestPayload = {
		firstName:'',
		lastName:'',
		email:'',
		username:'',
		password:'',
		confirmPassword:''
	};

	registrationFormModel: FormGroup;

	passwordGroup: FormGroup;

	parentErrorStateMatcher = new ParentErrorStateMatcher();
	constructor(private formBuilder: FormBuilder, private userService: UserService,
	            private userValidator: UsernameValidator, private emailValidator: EmailValidator,
	            private formValidator: FormValidator) {

	}

	ngOnInit() {
		this.createForm();
		this.registrationFormModel = this.formBuilder.group({
			login: 			  this.formBuilder.control("", [Validators.required,
							  Validators.pattern("^(?=.{6,32}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$")],
			                  this.userValidator.checkUserName.bind(this.userValidator)),
			email: 			  this.formBuilder.control("", [Validators.required, Validators.email],
							  this.emailValidator.checkEmail.bind(this.emailValidator)),
			firstName: 		  this.formBuilder.control("", [Validators.required]),
			lastName: 		  this.formBuilder.control("", [Validators.required]),
			matchingPassword: this.passwordGroup,
		});

	}

	createForm() {
		this.passwordGroup = new FormGroup({
			password: 		 this.formBuilder.control("", [Validators.required,
				Validators.minLength(8), Validators.maxLength(32)]),
			confirmPassword: this.formBuilder.control("", Validators.required)
		}, (formGroup: FormGroup) =>{
			return PasswordValidator.areEqual(formGroup);
		});
	}

	register(): void {
		if(this.registrationFormModel.valid){
			this.userService.register(this.model);
		} else {
			this.formValidator.validateAllFormFields(this.registrationFormModel);
		}
	}

}


export interface CreateNewUserRequestPayload {
	firstName:string;
	lastName:string;
	email:string;
	username:string;
	password:string;
	confirmPassword:string;
}

