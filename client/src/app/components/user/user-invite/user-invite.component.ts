import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EmailValidator} from "../../../utils/validators/email-validator";
import {FormValidator} from "../../../utils/validators/form-validator";
import {ProjectService} from "../../../services/project/project.service";

@Component({
  selector: 'app-user-invite',
  templateUrl: './user-invite.component.html',
  styleUrls: ['./user-invite.component.scss']
})
export class UserInviteComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private projectService: ProjectService,
              private emailValidator: EmailValidator,
              private formValidator: FormValidator) { }

  sendInviteFormGroup: FormGroup;

  ngOnInit() {
    this.sendInviteFormGroup = this.formBuilder.group({
      email: this.formBuilder.control("", [Validators.required, Validators.email],
          this.emailValidator.checkAuthorizedEmail.bind(this.emailValidator))
    })
  }

  send(){
    if(this.sendInviteFormGroup.valid){
      this.projectService.sendInvite(this.sendInviteFormGroup);
    } else {
      this.formValidator.validateAllFormFields(this.sendInviteFormGroup);
    }
  }

}
