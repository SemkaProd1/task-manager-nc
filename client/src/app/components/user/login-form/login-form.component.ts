import {Component, OnInit} from "@angular/core"
import {FormBuilder, FormGroup, Validators} from "@angular/forms"
import {UserAuthService} from "../../../services/user/auth/user-auth.service"
import {Router} from "@angular/router"
import {MatSnackBar} from "@angular/material";

@Component({
	selector: "app-login-form",
	templateUrl: "./login-form.component.html",
	styleUrls: ["./login-form.component.scss"]
})
export class LoginFormComponent implements OnInit {

	loginFormModel: FormGroup

	constructor(private readonly userAuthService: UserAuthService,
				private readonly formBuilder: FormBuilder,
	            private readonly router: Router,
	            private snackbar: MatSnackBar) {
	}

	ngOnInit() {
		this.loginFormModel = this.formBuilder.group({
			username: this.formBuilder.control("", [Validators.required]),
			password: this.formBuilder.control("", [Validators.required])
		})
	}

	login() {
		this.userAuthService.authenticate(this.loginFormModel.value).subscribe(value => {
			this.router.navigate(["/app", "user", "info"])
		}, error => {
			this.snackbar.open("Login or password are invalid. Please try again", "Got it");
			console.log("Cannot authenticate")
		});
	}
}


