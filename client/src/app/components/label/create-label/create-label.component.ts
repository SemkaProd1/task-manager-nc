import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FormValidator} from "../../../utils/validators/form-validator";
import {LabelService} from "../../../services/label/label.service";
import {ProjectService} from "../../../services/project/project.service";

@Component({
  selector: 'app-create-label',
  templateUrl: './create-label.component.html',
  styleUrls: ['./create-label.component.scss']
})

export class CreateLabelComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private formValidator: FormValidator,
              private projectService: ProjectService) { }

  createLabelFormModel: FormGroup;

  ngOnInit() {

    this.createLabelFormModel = this.formBuilder.group({
      name: this.formBuilder.control("", [Validators.required]),
      description: this.formBuilder.control(""),
      color: this.formBuilder.control("")
    })

  }

  create(){
    if(this.createLabelFormModel.valid){
      this.projectService.createLabel(this.createLabelFormModel);
    } else {
      this.formValidator.validateAllFormFields(this.createLabelFormModel);
    }
  }
}
