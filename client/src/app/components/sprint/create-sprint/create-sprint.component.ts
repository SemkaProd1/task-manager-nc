import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SprintService} from "../../../services/sprint/sprint.service";
import {FormValidator} from "../../../utils/validators/form-validator";

@Component({
	selector: 'app-create-sprint',
	templateUrl: './create-sprint.component.html',
	styleUrls: ['./create-sprint.component.scss']
})
export class CreateSprintComponent implements OnInit {

	constructor(private formBuilder: FormBuilder,
	            private sprintService: SprintService,
	            private formValidator: FormValidator) {
	}

	statuses = CreateSprintComponent.toArray(SprintStatus);

	public static toArray(enum1: any): string[] {
		const array: string[] = [];

		let index: number = 0;
		for (const value in enum1) {
			array[index] = value.valueOf();
			index++;
		}
		return array;
	}

	public createSprintFormModel: FormGroup;

	ngOnInit(){
		this.createSprintFormModel = this.formBuilder.group({
			name:        this.formBuilder.control("", [Validators.required]),
			description: this.formBuilder.control("", [Validators.required]),
			startDate:   this.formBuilder.control(null, [Validators.required]),
			endDate:     this.formBuilder.control(null, [Validators.required]),
			status:      this.formBuilder.control("", [Validators.required])
		})
	}

	create() {
		if(this.createSprintFormModel.valid){
			this.sprintService.create(this.createSprintFormModel);
		} else {
			this.formValidator.validateAllFormFields(this.createSprintFormModel);
		}
	}

}

export enum SprintStatus {
	ACTIVE = 'ACTIVE',
	READY = 'READY',
	COMPLETED = 'COMPLETED'
}