import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop"
import {ActivatedRoute, Router} from "@angular/router"
import {TaskService} from "../../../services/task/task.service"
import {Component, OnInit} from "@angular/core"
import {LiteTask, TaskStatus} from "../../../types/data/liteTask"
import {DateService} from "../../../utils/date/date.service"
import {SprintService} from "../../../services/sprint/sprint.service"
import {UserService} from "../../../services/user/user/user.service"
import {ProjectService} from "../../../services/project/project.service"
import {catchError, map} from "rxjs/operators"
import {container} from "@angular/core/src/render3";
import {of} from "rxjs"

@Component({
	selector: "app-sprint-board",
	templateUrl: "./sprint-board.component.html",
	styleUrls: ["./sprint-board.component.scss"],
	providers: [DateService]
})

export class SprintBoardComponent implements OnInit {

	openTasks: LiteTask[] = [];
	inProgressTasks: LiteTask[] = [];
	implementedTasks: LiteTask[] = [];
	readyForTestingTasks: LiteTask[] = [];
	reopenedTasks: LiteTask[] = [];
	onHoldTasks: LiteTask[] = [];
	closedTasks: LiteTask[] = [];

	constructor(private router: Router,
	            private taskService: TaskService,
	            public dateService: DateService,
	            private activatedRoute: ActivatedRoute,
	            private sprintService: SprintService,
	            public userService: UserService,
	            private projectService: ProjectService) {}

	ngOnInit() {
		this.activatedRoute.params.subscribe(value => {
			this.sprintService.loadCurrentSprint(value.id)
			this.taskService.getTasks(value.id).subscribe(
				res => {
					this.byStatus(res)
				})
		})
	}

	drop(event: CdkDragDrop<LiteTask[]>, board: string) {
		if (event.previousContainer === event.container) {

			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex)

		} else {
			this.taskService.updateTaskByStatus({
				objectId: event.item.data.taskId,
				status: board as TaskStatus
			})

			transferArrayItem(
				event.previousContainer.data,
				event.container.data,
				event.previousIndex,
				event.currentIndex)
		}
	}

	createTaskRouting() {
		this.router.navigateByUrl("app/task/new")
	}

	byStatus(tasks: LiteTask[]) {
		this.openTasks = Array.from(this.taskService.getTasksByStatus("OPEN", tasks))
		this.inProgressTasks = Array.from(this.taskService.getTasksByStatus("IN_PROGRESS", tasks))
		this.implementedTasks = Array.from(this.taskService.getTasksByStatus("IMPLEMENTED", tasks))
		this.readyForTestingTasks = Array.from(this.taskService.getTasksByStatus("READY_FOR_TESTING", tasks))
		this.reopenedTasks = Array.from(this.taskService.getTasksByStatus("REOPENED", tasks))
		this.onHoldTasks = Array.from(this.taskService.getTasksByStatus("ON_HOLD", tasks))
		this.closedTasks = Array.from(this.taskService.getTasksByStatus("CLOSED", tasks))
	}

	getUserAccountId(participantId: number) {
		return this.projectService.participantAccounts
			.pipe(
				map(value => {
					return value.filter(value1 => value1.objectId === participantId)[0].userId
				}),
				catchError(err => of(undefined))
			)
	}

	update(taskId: number) {
		console.log(taskId);
	}

	// getDate(date: Date) {
	//   return this.dateService.getDate(date);
	// }

	getAllCount() {
		return this.openTasks.length
		+ this.inProgressTasks.length
		+ this.implementedTasks.length
		+ this.readyForTestingTasks.length
		+ this.onHoldTasks.length
		+ this.reopenedTasks.length
		+ this.closedTasks.length
	}

	getCompletedCount() {
		return this.closedTasks.length
	}

	getPercentage() {
		return Math.ceil(this.getCompletedCount() / (this.getAllCount() + 0.00000000001) * 100)
	}
}
