import {Component, OnInit} from "@angular/core"
import {FormBuilder, FormGroup, Validators} from "@angular/forms"
import {ProjectService} from "../../../services/project/project.service";


@Component({
	selector: "app-create-project",
	templateUrl: "./create-project.component.html",
	styleUrls: ["./create-project.component.scss"]
})
export class CreateProjectComponent implements OnInit {

	public createProjectFormModel: FormGroup;

	constructor(private formBuilder: FormBuilder,
	            private projectService: ProjectService) {}

	ngOnInit() {

		this.createProjectFormModel = this.formBuilder.group({
				projectName:          this.formBuilder.control("", [Validators.required]),
				projectDescription:   this.formBuilder.control("", [Validators.required]),
				startDate:            this.formBuilder.control(new Date(), [Validators.required]),
				endDate:              this.formBuilder.control(new Date(), [Validators.required])
		})
	}

	create() {
		this.projectService.create(this.createProjectFormModel);
	}
}
