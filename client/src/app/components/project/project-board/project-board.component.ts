import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router"
import {ProjectService} from "../../../services/project/project.service";
import {SprintService} from "../../../services/sprint/sprint.service";
import {Sprint} from "../../../types/data/sprint";
import {DateService} from "../../../utils/date/date.service";
import {DialogMenuComponent} from "../../core/dialog-menu/dialog-menu.component";
import {MatDialog} from "@angular/material";

@Component({
    selector: 'app-project-board',
    templateUrl: './project-board.component.html',
    styleUrls: ['./project-board.component.scss']
})
export class ProjectBoardComponent implements OnInit {

    sprints: Sprint[];
    participants = this.projectService.participantAccounts
	projectLabels = this.projectService.projectLabels

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private projectService: ProjectService,
                private sprintService: SprintService,
                public dateService: DateService,
                private dialog: MatDialog) {}

    ngOnInit() {
        this.activatedRoute.params.subscribe(value => {
            this.projectService.loadCurrentProject(value.id);
            this.sprintService.getSprints(value.id)
                .subscribe(res => {
                    this.sprints = Array.from(res)
                });
        })
    }

    createSprintRouting() {
        this.router.navigateByUrl("app/sprint/new");
    }

    addParticipant() {
        this.router.navigate(["/app/project/invite"])
    }

    addLabel() {
        this.router.navigate(["/app/project/label/new"])
    }

    deleteLabel(id: number) {
        this.dialog.open(DialogMenuComponent).afterClosed().subscribe(result => {
            if (result) {
                this.projectService.deleteLabel(id);
            } else {
                console.log('false');
            }
        })
    }
}
