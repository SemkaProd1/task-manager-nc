import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProjectService} from "../../../services/project/project.service";
import {ActivatedRoute} from "@angular/router";
import {UserAuthService} from "../../../services/user/auth/user-auth.service";
import {FormValidator} from "../../../utils/validators/form-validator";
import {EnumHandlerService} from "../../../utils/enum-handler/enum-handler.service";
import {mergeMap, take} from "rxjs/operators";
import {SafeUrl} from "@angular/platform-browser";
import {FileDownloadService} from "../../../services/file-download/file-download.service";


@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.scss']
})
export class UpdateProjectComponent implements OnInit {

  project = this.projectService.currentProject;

  logo: File = null;

  logoPreview: SafeUrl = "../../../../assets/images/ballot.svg";

  constructor(private formBuilder: FormBuilder, private projectService: ProjectService,
              private activateRoute: ActivatedRoute, private auth: UserAuthService,
              private formValidator: FormValidator,
              private handler: EnumHandlerService,
              private fileDownload: FileDownloadService) { }

  updateProjectFormGroup: FormGroup;

  phases = this.handler.toArray(Phase);


  ngOnInit() {

    this.updateProjectFormGroup = this.formBuilder.group({
      name:        this.formBuilder.control("", [Validators.required]),
      description: this.formBuilder.control("", [Validators.required]),
      phase:       this.formBuilder.control("",[Validators.required]),
      startDate:   this.formBuilder.control("", [Validators.required]),
      endDate:     this.formBuilder.control("", [Validators.required])
    })

      this.activateRoute.params.pipe(mergeMap( value =>{
          this.fileDownload.loadProjectLogoForUpdate(value.id).subscribe(value1 => {
            this.logoPreview = value1;
          });

          return this.projectService.loadCurrent(value.id)
    })).subscribe( value => {
            this.updateProjectFormGroup = this.formBuilder.group({
              name:        this.formBuilder.control(value['name'], [Validators.required]),
              description: this.formBuilder.control(value['description'], [Validators.required]),
              phase:       this.formBuilder.control(value['phase'],[Validators.required]),
              startDate:   this.formBuilder.control(new Date(value['startDate']), [Validators.required]),
              endDate:     this.formBuilder.control(new Date(value['endDate']), [Validators.required])
            })
      })

  }

  onFileChange(event) {

    console.log(event);
    this.logo = event.target.files[0];

  }

  handleFileInput(file: FileList){
    this.logo = file.item(0);

    let reader = new FileReader();
    reader.onload = (event: any) =>{
      this.logoPreview = event.target.result;
    }
    reader.readAsDataURL(this.logo);
  }

  update(){

    console.log(this.logo);
    if(this.updateProjectFormGroup.valid){
      this.projectService.update(this.updateProjectFormGroup, this.logo);
    } else {
      this.formValidator.validateAllFormFields(this.updateProjectFormGroup);
    }
  }
}

export enum Phase {
  DEVELOPMENT = "DEVELOPMENT",
  TESTING = "TESTING",
  BUGFIXING = "BUG_FIXING"
}
