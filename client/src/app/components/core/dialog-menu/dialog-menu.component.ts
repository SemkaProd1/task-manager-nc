import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {Router} from "@angular/router";

@Component({
    selector: 'app-dialog-menu',
    templateUrl: './dialog-menu.component.html',
    styleUrls: ['./dialog-menu.component.scss']
})
export class DialogMenuComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data: any, router: Router) {

    }

    ngOnInit() {
    }
}
