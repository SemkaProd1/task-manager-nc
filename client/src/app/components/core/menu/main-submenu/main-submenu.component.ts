import {Component, HostListener, Input, OnInit} from "@angular/core"

@Component({
	selector: "app-main-submenu",
	templateUrl: "./main-submenu.component.html",
	styleUrls: ["./main-submenu.component.scss"]
})
export class MainSubmenuComponent implements OnInit {
	@Input()
	icon: string

	@Input()
	placeholder: string

	@Input()
	pad: number = 15

	show: boolean = false

	constructor() {
	}

	ngOnInit() {
	}

}
