import {Component, OnInit} from "@angular/core"
import {UserAuthService} from "../../../../services/user/auth/user-auth.service"
import {Router} from "@angular/router";

@Component({
	selector: "app-main-menu",
	templateUrl: "./main-menu.component.html",
	styleUrls: ["./main-menu.component.scss"]
})
export class MainMenuComponent implements OnInit {

	readonly projectRoutes: {link: string, icon: string, label: string}[] = [
		{
			link: "/app/project/new",
			icon: "all_inbox",
			label: "New project"
		},
	];

	readonly userRoutes: {link: string, icon: string, label: string}[] = [
		{
			link: "/app/user/info",
			icon: "person_outline",
			label: "User info"
		}
	];

	readonly updateInfoRoutes: {link: string, icon: string, label: string}[] = [
		{
			link:"/app/user/edit/info",
			icon: "how_to_reg",
			label: "User Profile"
		}
	];

	readonly updatePasswordRoutes:{link: string, icon: string, label: string}[] = [
		{
			link:"/app/user/edit/password",
			icon: "security",
			label: "Password"
		}
	];

	readonly updateEmailRoutes:{link: string, icon: string, label: string}[] = [
		{
			link:"/app/user/edit/email",
			icon:"alternate_email",
			label:"Email"
		}
	];

	readonly taskRoutes:{link: string, icon: string, label: string}[] = [
		{
			link: "/app/task/new",
			icon: "all_inbox",
			label: "New Task"
		}
	];

	readonly sprintBoardRoutes:{link: string, icon: string, label: string}[] = [
		{
			link: "/app/sprint/board",
			icon: "all_inbox",
			label: "Sprint Board"
		}
	];

	readonly documentationRoutes:{link: string, icon: string, label: string}[] = [
		{
			link: "/app/documentation",
			icon: "all_inbox",
			label: "Documentation"
		}
	];

	readonly labelsRoutes:{link: string, icon: string, label: string}[] = [
		{
			link: "/app/labels",
			icon: "all_inbox",
			label: "Labels"
		}
	];

	readonly projectBoardRoutes:{link: string, icon: string, label: string}[] = [{
		link: "/app/project/board/:id",
		icon: "all_inbox",
		label: "Project"
	}];

	constructor(public userAuthService: UserAuthService,
				private router: Router) {
	}

	ngOnInit() {
	}


	logout() {
		this.userAuthService.logout()
			.subscribe(() => this.router.navigate(["/login"]))
	}
}
