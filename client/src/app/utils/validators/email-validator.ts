import {Injectable} from "@angular/core";
import {UserService} from "../../services/user/user/user.service";
import {FormControl} from "@angular/forms";


@Injectable()
export class EmailValidator {

	timeout: any;

	constructor(public userService: UserService){}

	checkEmail(control: FormControl): any {
		clearTimeout(this.timeout);

		return new Promise(resolve => {
			this.timeout = setTimeout(() =>{
				this.userService.isEmailExist(control.value).subscribe((res) =>{
					resolve(null);
				}, (error) => {
					resolve({ 'emailExists' : true });
				})
			}, 1000);
		})
	}

	checkAuthorizedEmail(control: FormControl): any {
		clearTimeout(this.timeout);

		return new Promise(resolve => {
			this.timeout = setTimeout(() =>{
				this.userService.isAuthorizedEmailExist(control.value).subscribe((res) =>{
					resolve(null);
				}, (error) => {
					console.log(error);
					resolve({ 'emailExists' : true });
				})
			}, 1000);
		})
	}
}