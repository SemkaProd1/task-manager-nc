import {Injectable} from "@angular/core";
import {UserService} from "../../services/user/user/user.service";
import {FormControl} from "@angular/forms";
import {UserAuthService} from "../../services/user/auth/user-auth.service";


@Injectable()
export class UsernameValidator {

	timeout: any;
	currentUser;
	constructor(public userService: UserService,
	            public userAuthService: UserAuthService) {
	}

	checkUserName(control: FormControl): any {

		clearTimeout(this.timeout);

		return new Promise(resolve => {
			this.timeout = setTimeout(() => {
				this.userService.isUserExist(control.value).subscribe((res) => {
					resolve(null);
				}, (err) => {
					resolve({'usernameExists': true});
				});
			}, 1000);
		})
	}

	checkAuthorizedName(control: FormControl): any {

		clearTimeout(this.timeout);
		this.userAuthService.authenticatedUserObservable.subscribe(
			value => this.currentUser = value.principal.username);


			return new Promise(resolve => {

					this.timeout = setTimeout(() => {
					console.log("Here");
						if(this.currentUser !== control.value) {
							this.userService.isAuthorizedUserExist(control.value).subscribe((res) => {
								resolve(null);
							}, (err) => {
								resolve({'usernameExists': true});
							});
						} else {
							return false;
						}
					}, 1000);

			})

	}
}