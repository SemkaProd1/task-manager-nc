import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnumHandlerService {

  public toArray(enum1: any): string[] {
    const array: string[] = [];

    let index: number = 0;
    for (const value in enum1) {
      array[index] = value.valueOf();
      index++;
    }
    return array;
  }
  constructor() { }
}
