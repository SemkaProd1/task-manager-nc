import { TestBed } from '@angular/core/testing';

import { EnumHandlerService } from './enum-handler.service';

describe('EnumHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnumHandlerService = TestBed.get(EnumHandlerService);
    expect(service).toBeTruthy();
  });
});
