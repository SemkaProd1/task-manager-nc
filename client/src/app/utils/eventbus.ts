import {BehaviorSubject, Observable, of} from "rxjs";

export class EventBus {
	private static INSTANCE: EventBus = new EventBus()

	private emitter: BehaviorSubject<void>
	private observable: Observable<void>

	constructor() {
		this.emitter = new BehaviorSubject<void>(null)
		this.observable = this.emitter.asObservable()
	}

	public send() {
		this.emitter.next(null)
	}

	public listen(callback: (value) => void) {
		this.observable.subscribe(callback)
	}

	public static get() {
		return this.INSTANCE
	}
}