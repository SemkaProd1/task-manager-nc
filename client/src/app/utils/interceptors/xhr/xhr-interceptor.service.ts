import {Injectable} from "@angular/core"
import {HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http"
import { catchError } from 'rxjs/operators'
import {BehaviorSubject, Observable, of, throwError} from "rxjs"
import {Router} from "@angular/router";
import {UserAuthService} from "../../../services/user/auth/user-auth.service";

@Injectable({
	providedIn: "root"
})
export class XhrInterceptor implements HttpInterceptor {

	constructor(private router: Router,
				private userAuthService: UserAuthService) {
	}

	intercept(request: HttpRequest<any>, next: HttpHandler) {
		return next.handle(request).pipe(
			catchError(err => {
				console.log(this.userAuthService
					? "UserAuthService was initialized"
					: "UserAuthService not initialized");
				if (err.status === 403) {
					this.userAuthService.doErasePrincipal()
					this.router.navigate(["/login"])
				}

				return throwError(err)
			})
		);
	}
}
