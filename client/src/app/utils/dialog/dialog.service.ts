import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from "@angular/material";

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(public dialogRef: MatDialogRef<DialogService>,
              public dialog: MatDialog) {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(
        DialogService, {
          width: '250px'
        }
    );

    dialogRef.afterClosed().subscribe(result =>{
      console.log('The dialog was closed');
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }

}
