import {ErrorStateMatcher} from "@angular/material"
import {FormControl, FormGroupDirective, NgForm} from "@angular/forms"

export class LoginFormErrorMatcher extends ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
		return false
	}
}
