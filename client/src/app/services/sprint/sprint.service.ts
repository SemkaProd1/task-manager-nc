import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ProjectService} from "../project/project.service";
import {FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {DateService} from "../../utils/date/date.service";
import {Sprint} from "../../types/data/sprint";
import {BehaviorSubject, Observable, of, throwError} from "rxjs"
import {catchError, map, mergeMap, take} from "rxjs/operators";
import {TaskStatus} from "../../types/data/liteTask"

@Injectable({
	providedIn: 'root'
})
export class SprintService {

	public sprintId;
	public sprints: Sprint[];

	private currentSprintOriginal: BehaviorSubject<Sprint> = new BehaviorSubject<Sprint>(null);
	public currentSprint = this.currentSprintOriginal.asObservable()

	constructor(private http: HttpClient, private projectService: ProjectService,
	            private router: Router, private dateService: DateService) {
	}

	create(createSprintFormModel: FormGroup) {

		this.projectService.currentProject.pipe(take(1), mergeMap(
			value => {
				const form = createSprintFormModel.value;
				const result = {
					projectId: value.objectId,
					name: form.name,
					description: form.description,
					startDate: this.dateService.normalizeDate(form.startDate),
					endDate: this.dateService.normalizeDate(form.endDate),
					status: form.status
				}

				console.log(value.objectId);

				return this.http.post("/api/sprint/new", result)
			}
		)).subscribe(
			res => {
				this.sprintId = res['objectId'];
				this.router.navigateByUrl("/app/sprint/board/" + this.sprintId);
			},
			err => {
				console.log(err);
			}
		)
	}

	getSprints(projectId: number): Observable<Sprint[]> {
		this.sprints = [];
		return this.http.get("/api/sprint/" + projectId + "/allInProject")
			.pipe(
				map(res => {
					const arr: any[] = res as [];
					for (const i of arr) {
						const sprint: Sprint = {
							objectId: i.objectId,
							projectId: i.projectId,
							name: i.name,
							description: i.description,
							startDate: new Date(this.dateService.normalizeDate(this.dateService.toDate(i.startDate))),
							endDate: new Date(this.dateService.normalizeDate(this.dateService.toDate(i.endDate))),
							status: i.status
						};
						this.sprints.push(sprint);
					}
					return this.sprints;

				}),
				catchError(err => {
					return throwError(err)
				})
			);
	}

	getStatistics(sprintId: number) {
		return this.http.get("/taskCountsByStatus", {
			params: {
				sprintId: JSON.stringify(sprintId)
			}
		})
		.pipe(
			mergeMap(value => {
				const all = value[TaskStatus.Open]
					+ value[TaskStatus.InProgress]
					+ value[TaskStatus.Implemented]
					+ value[TaskStatus.OnHold]
					+ value[TaskStatus.ReadyForTesting]
					+ value[TaskStatus.Reopened]
					+ value[TaskStatus.Closed]
				const completed = value[TaskStatus.Closed]

				return of({...value, all, completed})
			})
		)
	}

	loadCurrentSprint(sprintId: number) {
		this.http.get("/api/sprint/" + sprintId + "/").subscribe(
			res => {
				this.sprintId = res['objectId'];
				this.currentSprintOriginal.next(res as Sprint)
			}
		)
	}
}