import {Injectable} from "@angular/core"
import {HttpClient} from "@angular/common/http"
import {Observable, of} from "rxjs"
import {catchError, delay, map, mergeMap} from "rxjs/operators"
import {DomSanitizer, SafeUrl} from "@angular/platform-browser"

@Injectable({
	providedIn: "root"
})
export class FileDownloadService {

	constructor(private http: HttpClient,
				private sanitizer: DomSanitizer) {
	}

	public loadUserAvatar(id: number): Observable<SafeUrl | null> {
		return this.http.get("/api/usr/" + id + "/avatar", {
			responseType: "blob"
		}).pipe(
			map(value => this.toDataURL(value)),
			catchError(err => {
				return of(this.sanitizer.bypassSecurityTrustUrl('../../../../assets/images/person.svg'))
			})
		)
	}

	public loadProjectLogo(id: number): Observable<Blob | null> {
		return this.http.get("/api/project/" + id + "/getLogo", {
			responseType: "blob"
		}).pipe(
			catchError(err => of(null))
		)
	}

	public loadProjectLogoForUpdate(id: number): Observable<SafeUrl | null> {
		return this.http.get("/api/project/" + id + "/getLogo", {
			responseType: "blob"
		}).pipe(
			map(value => this.toDataURL(value)),
			catchError(err => {
				return of(this.sanitizer.bypassSecurityTrustUrl('../../../../assets/images/ballot.svg'))
			})
		)
	}

	public loadDocument(id: number): Blob | null {
		return null
	}

	public toDataURL(blob: Blob): SafeUrl {
		return this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(blob))
	}
}
