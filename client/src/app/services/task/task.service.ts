import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {SprintService} from "../sprint/sprint.service";
import {UserAuthService} from "../user/auth/user-auth.service";
import {DateService} from "../../utils/date/date.service";
import {LiteTask} from "../../types/data/liteTask";
import {BehaviorSubject, Observable, of, throwError} from "rxjs";
import {catchError, map, mergeMap, take} from "rxjs/operators"
import { TaskPage, UpdateTaskStatus} from "../../types/data/task";
import {LabelService} from "../label/label.service";

import {ProjectService} from "../project/project.service"
import {MatSnackBar} from "@angular/material";

@Injectable({
    providedIn: 'root'
})
export class TaskService {

    public currentTaskId;
    public tasks: LiteTask[];
    public tasksByCurrentStatus: LiteTask[];
    private url;

    currentTaskOriginal: BehaviorSubject<TaskPage> = new BehaviorSubject<TaskPage>(null);
    currentTask = this.currentTaskOriginal.asObservable();

    constructor(private http: HttpClient,
                private router: Router,
                private sprintService: SprintService,
                private authService: UserAuthService,
                private dateService: DateService,
                private labelService: LabelService,
                private projectService: ProjectService,
                private snackBar: MatSnackBar) {}


    updateTaskByStatus(updateStatus: UpdateTaskStatus) {
        this.http.post("api/project/task/setStatus", updateStatus).subscribe(
            value => {
                console.log(updateStatus.status)
            }
        )
    }
    update(updateTaskFormModel: FormGroup){
       this.authService.authenticatedUserObservable.pipe(take(1),
            mergeMap(value => {
                const form = updateTaskFormModel.value;
                console.log(form.assignee);

                console.log(form.labels);
                const result = {
                    name:                  form.name,
                    description:           form.description,
                    deadline:              this.dateService.normalizeDate(form.deadline),
                    type:                  form.type,
                    status:                form.status,
                    priority:              form.priority,
                    creatorParticipantId:  form.creatorId,
                    assigneeParticipantId: form.assignee,
                    labels:                form.labels.map(value=> value.objectId),
                    sprintId:              this.sprintService.sprintId,
                    objectId:              this.currentTaskId
                };

                this.url = "/app/sprint/board/" + this.sprintService.sprintId;
                return this.http.post("api/project/task/update", result);
            })
        ).subscribe(res =>{
            this.snackBar.open("Task Successfully updated", "Got it");

            this.router.navigate([this.url]);

        },
            err =>{
            this.snackBar.open("Something went wrong, please try again", "Got it");

            })
    }

    create(createTaskFormModel: FormGroup) {
        this.authService.authenticatedUserObservable
            .pipe(
                take(1),
                mergeMap(user => this.projectService.participantAccounts.pipe(
                    take(1),
                    map(participants => participants.filter(
                    participant => participant.userId === user.principal.objectId)[0].objectId))
                ),
                mergeMap(participantId => this.sprintService.currentSprint.pipe(
                    take(1),
                    mergeMap(sprint => {
                        const form = createTaskFormModel.value;

                        const formData = new FormData()
                        formData.append("name", form.name)
                        formData.append("description", form.description)
                        formData.append("deadline", this.dateService.normalizeDate(form.deadline).toISOString())
                        formData.append("type", form.type)
                        formData.append("status", form.status)
                        formData.append("priority", form.priority)
                        formData.append("creatorParticipantId", JSON.stringify(participantId))
                        formData.append("assigneeParticipantId", JSON.stringify(form.assignee))
                        formData.append("sprintId", JSON.stringify(sprint.objectId))
                        for (const element of form.labels) {
                            formData.append("labels", element.objectId)
                        }

                        const url = "/app/sprint/board/" + sprint.objectId
                        return this.http.post("/api/project/task/new", formData)
                            .pipe(
                                mergeMap(value => this.router.navigate([url]))
                            )
                    })
                ))
            )
            .subscribe(value => {
                console.log(value)
            })
    }


    getTasksByStatus(status: string, tasks: LiteTask[]) {
        this.tasksByCurrentStatus = [];
        this.tasks = [];
        for (let task of tasks) {
            if (task.status === status) {
                this.tasksByCurrentStatus.push(task);
            }
        }
        return this.tasksByCurrentStatus;
    }

    // getTask(taskId: number){
    //
    //     return this.http.get("/api/project/task/get/" + taskId).pipe(
    //         map(res => {
    //             const taskPage: TaskPage = {
    //                 taskId: res["objectId"],
    //                 name: res["name"],
    //                 deadline: new Date(this.dateService.normalizeDate(this.dateService.toDate(res["deadline"]))),
    //                 labels: this.labelService.getLabels(res["labels"]),
    //                 status: res["status"],
    //                 assigneeParticipantId: res["assigneeParticipantId"],
    //                 creatorParticipantId: res["creatorParticipantId"],
    //                 description: res["description"],
    //                 type: res["type"],
    //                 priority: res["priority"]
    //             };
    //             console.log(taskPage);
    //             return taskPage;
    //         })
    //     );
    // }

    loadCurrentTask(taskId: number){
        return this.http.get<TaskPage>("/api/project/task/get/" + taskId).pipe(mergeMap( res => {
                console.log(res);
                this.currentTaskId = res['objectId'];
                this.currentTaskOriginal.next(res as TaskPage);
                return of(res as TaskPage);
            }))
    }

    getTasks(sprintId: number): Observable<LiteTask[]> {
        this.tasks = this.tasks || [];
        return this.http.get<LiteTask[]>("/api/sprint/" + sprintId + "/tasks/")
            .pipe(
                map(res => {
                    return res.map(value => {
                        value.deadline = new Date(
                            this.dateService.normalizeDate(this.dateService.toDate(value.deadline)))
                        return value
                    });
                }),
                catchError(err => {
                    alert(JSON.stringify(err));
                    return throwError(err);
                })
            );
    }
}
