import {Injectable} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {UserAuthService} from "../user/auth/user-auth.service";
import {DateService} from "../../utils/date/date.service";
import {Project} from "../../types/data/project";
import {BehaviorSubject, forkJoin, Observable, of, pipe, zip} from "rxjs"
import {concatMap, mergeMap, take, tap} from "rxjs/operators"
import {Participant, ParticipantFullInfo, Principal} from "../../types/data/user-credentials"
import {MatSnackBar} from "@angular/material";
import {FileDownloadService} from "../file-download/file-download.service"
import {Label} from "../../types/data/label"
import {LabelService} from "../label/label.service"

@Injectable({
	providedIn: 'root'
})
export class ProjectService {
	public projectId: number;

	private currentProjectOriginal: BehaviorSubject<Project> = new BehaviorSubject<Project>(null);
	currentProject = this.currentProjectOriginal.asObservable()

	private currentParticipantsOriginal: BehaviorSubject<Principal[]>
		= new BehaviorSubject<Principal[]>(null);
	currentParticipant = this.currentParticipantsOriginal.asObservable();

	private participantAccountsOriginal: BehaviorSubject<ParticipantFullInfo[]>
		= new BehaviorSubject<ParticipantFullInfo[]>([])
	participantAccounts = this.participantAccountsOriginal.asObservable()

	private projectLabelsOriginal: BehaviorSubject<Label[]>
		= new BehaviorSubject<Label[]>([])
	projectLabels = this.projectLabelsOriginal.asObservable()

	currentId;

	constructor(private http: HttpClient,
	            private router: Router,
	            private authService: UserAuthService,
	            private dateService: DateService,
	            private snackBar: MatSnackBar,
	            private download: FileDownloadService,
	            private label: LabelService) {}

	public create(createProjectFormModel: FormGroup) {
		this.authService.authenticatedUserObservable.pipe(take(1),
			concatMap(authenticatedUser => {
				const form = createProjectFormModel.value;
				const result = {
					projectName:        form.projectName,
					projectDescription: form.projectDescription,
					startDate:          this.dateService.normalizeDate(form.startDate),
					endDate:            this.dateService.normalizeDate(form.endDate),
					creatorId:          authenticatedUser.principal.objectId
				}

				return this.http.put<{newParticipant: Participant}>("/api/project/new", result)
			}),
			concatMap(response => this.router.navigate(
				["/app/project/board/", response.newParticipant.projectId])),
			concatMap(() => this.authService.updateUser())
		)
		.subscribe(value => {
			console.log("Successfully!!")
		})
	}

	public update(updateProjectFormGroup: FormGroup, logo: File){
		this.currentProject.pipe(take(1), mergeMap(value => {
			const form = updateProjectFormGroup.value;
			const result : FormData = new FormData();

			result.append("objectId", JSON.stringify(value['objectId']));
			result.append("name", form.name);
			result.append("description", form.description);
			result.append("phase", form.phase);
			result.append("startDate", this.dateService.normalizeDate(form.startDate).toISOString());
			result.append("endDate", this.dateService.normalizeDate(form.endDate).toISOString());
			if(logo != null){
				result.append("logo", logo, logo.name);
			}
			return this.http.post("/api/project/update", result);
		})).subscribe(res =>{
			console.log(res);
			this.snackBar.open("Project successfully updated", "Got It");
			this.router.navigate(["/app/user/info"]);
		}, err => {
			console.log(err);
		})
	}

	createLabel(createLabelFormModel: FormGroup){
		this.currentProject.pipe(take(1),mergeMap( res =>{
			const form= createLabelFormModel.value;
			const result = {
				projectId: res.objectId,
				name: form.name,
				description: form.description,
				color: form.color.substring(1)

			}
			return this.http.post<{newParticipant: Project}>("/api/labels/new", result);
		})).subscribe(
				value => {

					this.currentProject.subscribe(value1 => {
						this.currentId = value1.objectId})
					this.router.navigate(["app/project/board/", this.currentId])
				},
				error1 => {
					console.log(error1);
				}
			)
	}

	sendInvite(inviteFormGroup: FormGroup){
		this.currentProject.pipe(take(1), mergeMap(res => {
			const form = inviteFormGroup.value;
			const result = {
				projectId: res.objectId,
				role: 'USER',
				url: null,
				email: form.email
			}
			return this.http.post("/api/invite/send", result)
		})).subscribe(value => {
			this.snackBar.open("Invite sent", "Got It")
			this.currentProject.subscribe(value1 => {
				this.currentId = value1.objectId})
			this.router.navigate(["app/project/board/", this.currentId])
		}, error1 => {
			console.log(error1);
		})
	}
	loadCurrentProject(projectId : number) {
		this.http.get<Project>("/api/project/get/" + projectId + "/").pipe(
			tap(res => {
				this.currentProjectOriginal.next(res)
			}),
			concatMap(project => zip(
				this.http.get<Participant[]>("/api/project/get/" + project.objectId + "/participantAccounts"),
				this.http.get<Principal[]>("/api/project/participants/" + project.objectId + "/"),
				this.label.getLabelsFromProject(project.objectId)
			)),
			concatMap(([participants, principals, labels]) => {
				this.projectLabelsOriginal.next(labels);

				const participantFullInfos: Observable<ParticipantFullInfo>[] = []

				for (const participant of participants) {
					const filterElement = principals.filter(principal => principal.objectId === participant.userId)[0]

					const observable = this.download.loadUserAvatar(participant.userId)
						.pipe(concatMap(value => of({
							avatar:    value,
							username:  filterElement.username,
							objectId:  participant.objectId,
							projectId: participant.projectId,
							role:      participant.role,
							userId:    filterElement.objectId,
							lastName:  filterElement.lastName,
							firstName: filterElement.firstName,
							email:     filterElement.email,
						} as ParticipantFullInfo)));

					participantFullInfos.push(observable)
				}

				return forkJoin<ParticipantFullInfo>(...participantFullInfos)
			}),
			tap(participants => {
				this.participantAccountsOriginal.next(participants)
			})
		).subscribe(value => {
			console.log(value)
		})
	}

	loadCurrent(projectId: number){
		return this.http.get("/api/project/get/" + projectId + "/").pipe(mergeMap(
			res =>{
				this.currentProjectOriginal.next(res as Project);
				return of(res as Project);
			}
		))
	}

	deleteLabel(id: number){
		this.http.delete("/api/labels/delete/" + id + "/").subscribe(
			value => {
				this.snackBar.open("Label successfully deleted", "Got it");
				this.currentProject.subscribe(value1 => {
					this.currentId = value1.objectId});
				this.loadCurrentProject(this.currentId);
			},
			error1 => {
				console.log(error1);
			}
		)
	}
}
