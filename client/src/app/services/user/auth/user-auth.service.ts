import {HttpClient} from "@angular/common/http"
import {
    AuthenticatedUser,
    UserCredentials
} from "../../../types/data/user-credentials"
import {Injectable} from "@angular/core"
import {
    BehaviorSubject,
    Observable, of,
    throwError
} from "rxjs";
import {
    map,
    mergeMap,
    tap
} from "rxjs/operators";

@Injectable({
	providedIn: "root"
})
export class UserAuthService {
    private readonly authenticatedUserSource: BehaviorSubject<AuthenticatedUser> =
            new BehaviorSubject(null)

    public readonly authenticatedUserObservable: Observable<AuthenticatedUser> =
        this.authenticatedUserSource.asObservable()


    constructor(private httpClient: HttpClient) {
    }

    public authenticate(userCredentials: UserCredentials): Observable<AuthenticatedUser> {
        return this.httpClient.post("/login", userCredentials, {observe: "response"})
            .pipe(
                mergeMap(value => {
                    console.log(value.body)
                    if (!value.body['message']) {
                        return this.updateUser()
                    } else {
                        return of(value.body as AuthenticatedUser)
                    }
                })
            )
    }

    public updateUser(): Observable<AuthenticatedUser> {
        return this.httpClient.get("/login/user")
            .pipe(this.postLogin())
    }

    public isAuthenticated(): Observable<boolean> {
        return this.httpClient.get<boolean>("/login/status")
    }

    public logout() {
        return this.httpClient.get("/login/logout")
            .pipe(map(() => this.erasePrincipal()))
    }

    public doErasePrincipal() {
        window.localStorage.removeItem("principal")
        this.authenticatedUserSource.next(null)
    }

    private postLogin() {
        return tap<AuthenticatedUser>(newValue => {
            window.localStorage.setItem("principal", JSON.stringify(newValue.principal))
            this.authenticatedUserSource.next(newValue)
        })
    }

    private erasePrincipal() {
        this.doErasePrincipal()

        return throwError("Not authenticated")
    }
}
