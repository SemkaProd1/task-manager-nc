import {Injectable} from "@angular/core"
import {HttpClient, HttpParams} from "@angular/common/http"
import {UserAuthService} from "../auth/user-auth.service"
import {
	CreateNewUserRequestPayload
} from "../../../components/user/registration-form/registration-form.component"
import {Router} from "@angular/router"
import {MatSnackBar} from "@angular/material"
import {FormGroup} from "@angular/forms"
import {map, mergeMap, take} from "rxjs/operators"
import {UpdatePasswordUser} from "../../../types/data/user-credentials";

@Injectable({
	providedIn: "root"
})
export class UserService {

	currentUsername;

	constructor(private httpClient: HttpClient,
	            private userAuthService: UserAuthService,
	            private router: Router,
	            private snackBar: MatSnackBar) {
	}

	public register(model: CreateNewUserRequestPayload): void {
		this.httpClient.post("/register/pass", model, {observe: "response"}).subscribe(
			res => {
				this.snackBar.open("Thank you for the registration. Please Log in.", "Got it");
				this.router.navigateByUrl("/login")
			},
			err => {
				this.snackBar.open("Something went wrong, please, try it again", "Got it")
			})
	}

	isEmailExist(email: string) {
		let params = new HttpParams().set("email", email)
		return this.httpClient.get("/register/isMailExists", {params: params}).pipe(
			map((resp: number) => {
				if (resp == 1) {
					throw new Error("Email already exists")
				}
			}))
	}

	isAuthorizedEmailExist(email: string) {
		let params = new HttpParams().set("email", email)
		return this.httpClient.get("api/usr/isMailExists", {params: params}).pipe(
			map((resp: number) => {
				if (resp == 1) {
					throw new Error("Email already exists")
				}
			}))
	}

	isAuthorizedUserExist(username) {

		let params = new HttpParams().set("username", username)
		return this.httpClient.get("api/usr/isUsernameExists", {params: params}).pipe(
			map((resp: number) => {
				if (resp == 1) {
					throw new Error("Email already exists")
				}
			}))

	}


	isUserExist(username) {
		let params = new HttpParams().set("username", username)
		return this.httpClient.get("/register/isUsernameExists", {params: params})
			.pipe(map((res: number) => {
				if (res == 1) {
					throw new Error("Username already exists")
				}
			}))
	}

	updateEmail(updateUserFormModel: FormGroup) {
		this.userAuthService.authenticatedUserObservable.pipe(
			take(1),
			mergeMap(value => {
				const form = updateUserFormModel.value

				const result = {
					objectId: value.principal.objectId,
					email: form.email
				}
				return this.httpClient.post("/api/usr/update/email", result)
			}),
			mergeMap(value => this.userAuthService.updateUser())
		).subscribe(
			res => {
				this.snackBar.open("Email successfully changed", "Got it");
				this.router.navigateByUrl("app/user/info")
			},
			err => {
				this.snackBar.open("Something went wrong, please try again", "Got it")
			}
		)
	}


	updatePassword(updatePassword: UpdatePasswordUser){
		this.userAuthService.authenticatedUserObservable.pipe(take(1),
			mergeMap( value =>{

				const result = {
					objectId: value.principal.objectId,
					oldPassword: updatePassword.oldPassword,
					password: updatePassword.password,
					confirmPassword: updatePassword.confirmPassword
				}

				console.log(result);
				return this.httpClient.post("/api/usr/update/password", result)
			})
		).subscribe(res => {
			this.snackBar.open("Password successfully changed", "Got it");
			this.router.navigateByUrl("app/user/info")
		}, err =>{
			this.snackBar.open("Something went wrong. " +
				"Please check match of old password field with old password", "Got it");
		})
	}

    deleteAccount() {
        return this.userAuthService.authenticatedUserObservable.pipe(
            mergeMap(value =>
                this.httpClient.delete("/api/usr/remove/" + value.principal.objectId)))
            .subscribe(res => {
                this.userAuthService.logout()
            .subscribe(() => this.router.navigate(["/login"]));

            });

    }

    updateUserInfo(updateUserInfo: FormGroup, avatar: File) {
        return this.userAuthService.authenticatedUserObservable.pipe(
        	take(1),
            mergeMap(value => {
                const formData: FormData = new FormData();
                formData.append("objectId", JSON.stringify(value.principal.objectId));
                formData.append("username", updateUserInfo.value.username);
                formData.append("firstName", updateUserInfo.value.firstName);
                formData.append("lastName", updateUserInfo.value.lastName);
                if(avatar != null){
	                formData.append("avatar", avatar, avatar.name);
                }
				return this.httpClient.post("/api/usr/update", formData);
			}),
	        mergeMap(value => this.userAuthService.updateUser())
		).subscribe(res =>{
			console.log(res);
			this.snackBar.open("Profile successfully updated", "Got It");
			this.router.navigate(["/app/user/info"])
		}, error1 => {
			console.log(error1);
		})
	}

	getUserAccount(participantId: number) {
		return this.httpClient.get<number>("/api/usr/getUserIdByParticipant", {
			params: {
				participantId: JSON.stringify(participantId)
			}
		})
	}
}
