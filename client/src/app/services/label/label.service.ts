import {Injectable} from '@angular/core';
import {Observable, throwError} from "rxjs"
import {catchError, map} from "rxjs/operators";
import {Label} from "../../types/data/label";
import {HttpClient} from "@angular/common/http";


@Injectable({
    providedIn: 'root'
})
export class LabelService {
    labels: Label[];

    constructor(private http: HttpClient) {
    }

    getLabels(labels: number[]) {
        this.labels = [];
        return this.http.get("/api/labels/" + labels)
            .pipe(map(res => {
                const arr: any[] = res as [];
            for(const i of arr){
                const label: Label = {
                    objectId: i.objectId,
                    name: i.name,
                    description: i.description,
                    color: i.color
                };
                this.labels.push(label)
            }
               return this.labels;
    }), catchError(err => {
        return throwError(err)
            })
            );
    }

    getLabelsFromProject(projectId: number): Observable<Label[]> {
        return this.http.get<Label[]>("/api/labels/project/" + projectId)
    }
}
