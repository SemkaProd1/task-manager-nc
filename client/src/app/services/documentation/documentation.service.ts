import {Injectable} from '@angular/core';
import {
    HttpClient,
    HttpRequest, HttpResponse
} from "@angular/common/http"
import {Router} from "@angular/router";
import {UserAuthService} from "../user/auth/user-auth.service";
import {BehaviorSubject, Observable, of} from "rxjs"
import {
    concatMap,
    map,
    mergeMap, take, tap
} from "rxjs/operators"
import {DateService} from "../../utils/date/date.service"
import {ProjectService} from "../project/project.service"
import {Documentation} from "../../types/data/documentation"
import {AuthenticatedUser} from "../../types/data/user-credentials"
import {Project} from "../../types/data/project"

@Injectable({
    providedIn: 'root'
})
export class DocumentationService {

    constructor(private http: HttpClient,
                private router: Router,
                private authService: UserAuthService,
                private dateService: DateService,
                private projectService: ProjectService) {
    }

    public create(file: File) {
        return this.authService.authenticatedUserObservable.pipe(
            take(1),
            mergeMap(user => this.projectService.currentProject.pipe(
                take(1),
                mergeMap(project => of({user, project}))
            )),
            mergeMap((value: {user: AuthenticatedUser, project: Project}) => this.projectService.participantAccounts.pipe(
                take(1),
                mergeMap(participants => of({
                    participant: participants.filter(
                        participant => participant.userId === value.user.principal.objectId)[0],
                    projectId: value.project.objectId
                }))
            )),
            mergeMap(value => {
                const formData: FormData = new FormData();
                formData.append("creatorParticipantId", JSON.stringify(value.participant.objectId));
                formData.append("creationTime", this.dateService.normalizeDate(new Date()).toISOString());
                formData.append("parentId", JSON.stringify(value.projectId));
                formData.append("filePath", file, file.name);

                const request = new HttpRequest("POST", "api/documentation/add", formData, {
                    reportProgress: true
                })

                return this.http.request(request)
            }),
            map((resp: any) => resp),
        );
    }

    public loadProjectDocuments(): Observable<Documentation[]> {
        return this.projectService.currentProject.pipe(
            take(1),
            concatMap(value =>
                this.http.get<Documentation[]>("/api/documentation/project/" + value.objectId))
        )
    }

    public downloadDocument(id: number) {
        return this.http.get("/api/documentation/download/" + id, {
            responseType: "blob"
        })
    }

    public deleteDocument(id: number) {
        return this.http.post("/api/documentation/delete", {objectId: id})
    }

    public getDocument(id: number) {
        return this.http.get<Documentation>("/api/documentation/project/document/" + id)
    }

    public extractFilename(disposition: string) {
        if (disposition && disposition.indexOf('attachment') !== -1) {
            const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            const matches = filenameRegex.exec(disposition);
            if (matches != null && matches[1]) {
                return matches[1].replace(/['"]/g, '');
            }
        }

        return null
    }
}
