import {BrowserModule} from "@angular/platform-browser"
import {NgModule} from "@angular/core"
import {AppRoutingModule} from "./app-routing.module"
import {AppComponent} from "./app.component"
import {
    HTTP_INTERCEPTORS,
    HttpClientModule
} from "@angular/common/http"
import {
    FormsModule,
    ReactiveFormsModule
} from "@angular/forms"
import {BrowserAnimationsModule} from "@angular/platform-browser/animations"
import {LoginPageComponent} from "./components/user/login-page/login-page.component"
import {
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatGridListModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatOptionModule,
    MatSnackBarModule, MatDialogModule
} from "@angular/material"
import {FlexLayoutModule} from "@angular/flex-layout"
import {LoginFormComponent} from "./components/user/login-form/login-form.component"
import {RegistrationFormComponent} from "./components/user/registration-form/registration-form.component"
import {XhrInterceptor} from "./utils/interceptors/xhr/xhr-interceptor.service"
import {MainPageComponent} from "./components/core/main-page/main-page.component"
import {CreateProjectComponent} from "./components/project/create-project/create-project.component"
import {MatSidenavModule} from "@angular/material/sidenav"
import {MatToolbarModule} from "@angular/material/toolbar"
import {MainMenuComponent} from "./components/core/menu/main-menu/main-menu.component"
import {MatRippleModule} from "@angular/material/core"
import {MatListModule} from "@angular/material/list"
import {MainSubmenuComponent} from "./components/core/menu/main-submenu/main-submenu.component"
import {MatDatepickerModule} from '@angular/material/datepicker'
import {MatNativeDateModule} from "@angular/material"
import {CreateTaskComponent} from './components/task/create-task/create-task.component'
import {SprintBoardComponent} from './components/sprint/sprint-board/sprint-board.component'
import {DragDropModule} from '@angular/cdk/drag-drop'
import {ScrollingModule} from '@angular/cdk/scrolling'
import {CdkTableModule} from '@angular/cdk/table'
import {CdkTreeModule} from '@angular/cdk/tree'
import {ProjectBoardComponent} from './components/project/project-board/project-board.component'
import {CreateSprintComponent} from './components/sprint/create-sprint/create-sprint.component'
import {CommonModule, DatePipe} from "@angular/common"
import {UserInfoComponent} from './components/user/user-info/user-info.component'
import {UpdatePasswordComponent} from './components/user/update-password/update-password.component'
import {UpdateEmailComponent} from './components/user/update-email/update-email.component'
import {UpdateUserInfoComponent} from './components/user/update-user-info/update-user-info.component'
import {PasswordValidator} from "./utils/validators/password-validator"
import {UsernameValidator} from "./utils/validators/username-validator"
import {EmailValidator} from "./utils/validators/email-validator"
import {CreateDocumentComponent} from './components/document/create-document/create-document.component'
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { DocumentationComponent } from './components/document/documentation/documentation.component'
import {UpdateProjectComponent} from "./components/project/update-project/update-project.component";
import {FormValidator} from "./utils/validators/form-validator";
import {MatMenuModule} from '@angular/material/menu';
import { UpdateTaskComponent } from './components/task/update-task/update-task.component';
import {EnumHandlerService} from "./utils/enum-handler/enum-handler.service";
import {DialogMenuComponent} from "./components/core/dialog-menu/dialog-menu.component";
import { CreateLabelComponent } from './components/label/create-label/create-label.component';
import { UserInviteComponent } from './components/user/user-invite/user-invite.component';



@NgModule({
    declarations: [
        AppComponent,
        LoginPageComponent,
        LoginFormComponent,
        RegistrationFormComponent,
        MainPageComponent,
        CreateProjectComponent,
        MainMenuComponent,
        MainSubmenuComponent,
        CreateTaskComponent,
        UserInfoComponent,
        CreateTaskComponent,
        SprintBoardComponent,
        ProjectBoardComponent,
        CreateSprintComponent,
        UpdatePasswordComponent,
        UpdateEmailComponent,
        UpdateUserInfoComponent,
        CreateDocumentComponent,
        DocumentationComponent,
        CreateDocumentComponent,
        UpdateProjectComponent,
        UpdateTaskComponent,
        DialogMenuComponent,
        CreateLabelComponent,
        UserInviteComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule,
        ...[
            MatButtonModule,
            MatCheckboxModule,
            MatCardModule,
            MatGridListModule,
            MatTabsModule,
            MatFormFieldModule,
            MatInputModule,
            MatIconModule,
            MatFormFieldModule,
            MatSelectModule,
            MatOptionModule,
            MatSidenavModule,
            MatToolbarModule,
            MatRippleModule,
            MatListModule,
            MatDatepickerModule,
            MatNativeDateModule,
            CdkTableModule,
            CdkTreeModule,
            DragDropModule,
            ScrollingModule,
            MatSnackBarModule,
            MatProgressBarModule,
            MatDialogModule,
            MatMenuModule
        ],
        FlexLayoutModule.withConfig({useColumnBasisZero: false})
    ],
    providers: [
        DatePipe,
        UsernameValidator,
        PasswordValidator,
        EmailValidator,
	    FormValidator,
        EnumHandlerService,
        {provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true}
    ],
    bootstrap: [AppComponent],
    entryComponents:[DialogMenuComponent]
})

export class AppModule {}
