console.log("Configuring proxy server")

module.exports = [
    {
        context: [
            "/login",
            "/api",
            "/register"
        ],
        target: "http://localhost:10240",
        secure: false,
        logLevel: "debug"
    }
]